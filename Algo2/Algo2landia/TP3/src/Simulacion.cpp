#include "Simulacion.h"

Simulacion::Simulacion(Mapa m, Coordenada c, const map <Color, Coordenada> &objetos) : _mapa(std::move(m)),
                                                                                       _agente(std::move(c)),
                                                                                       _cantMov(0),
                                                                                       _cantObjetivosRealizados(0),
                                                                                       _objetivosDisponibles(
                                                                                               list<Objetivo>()),
                                                                                       _objetos(objetos),
                                                                                       _diccObjetivos(
                                                                                               Trie<Trie<list<Objetivo>::iterator>>()) {
    _posObj = vector < vector < Color >> (m.ancho());

    for (int i = 0; i < m.ancho(); i++)
        _posObj[i] = vector<Color>(m.alto(), "");

    for (pair <Color, Coordenada> p: objetos)
        _posObj[p.second.first][p.second.second] = p.first;

    _objPos = Trie<Coordenada>();

    for (pair <Color, Coordenada> o: objetos)
        _objPos.insert(o);
}

bool Simulacion::mover(Direccion d) {

    Coordenada ps = _siguientePosicion(_agente, d);
    Coordenada pss = _siguientePosicion(ps, d);
    bool res = _mapa.enRango(ps);

    if (res) {

        _cantMov++;
        bool chocaPared = _mapa.esPiso(_agente) && _mapa.esElevacion(ps);
        bool hayObj = !_posObj[ps.first][ps.second].empty();
        bool seVaObj = hayObj && !_mapa.enRango(pss);
        bool chocaOtroObj = hayObj && _mapa.enRango(pss) && !_posObj[pss.first][pss.second].empty();
        bool chocaParedObj = hayObj && _mapa.enRango(pss) && _mapa.esPiso(ps) && _mapa.esElevacion(pss);

        if (!chocaPared && !seVaObj && !chocaOtroObj && !chocaParedObj) {
            _agente = ps;
            if (hayObj) {
                Color colorOb = _posObj[ps.first][ps.second];
                Color colorCa = _mapa.coloresCasilla(pss);
                _posObj[pss.first][pss.second] = colorOb;
                _posObj[ps.first][ps.second] = "";
                _objPos.insert(make_pair(colorOb, pss));

                // Necesario para la traducción:
                _objetos[colorOb] = pss;

                if (_diccObjetivos.count(colorOb) == 1 && (_diccObjetivos.at(colorOb)).count(colorCa) == 1) {
                    _cantObjetivosRealizados++;

                    auto it = (_diccObjetivos.at(colorOb)).at(colorCa);
                    _objetivosDisponibles.erase(it);
                    (_diccObjetivos.at(colorOb)).erase(colorCa);

                    if ((_diccObjetivos.at(colorOb)).empty()) {
                        _diccObjetivos.erase(colorOb);
                    }
                }
            }
        }

    }
    return res;
}

void Simulacion::agObjetivo(const Objetivo &o) {
    if (_diccObjetivos.count(o.objeto()) == 0 || (_diccObjetivos.at(o.objeto())).count(o.receptaculo()) == 0) {
        if (!seCumpleObjetivo(o)) {
            _objetivosDisponibles.push_front(o);
            auto it = _objetivosDisponibles.begin();

            if (_diccObjetivos.count(o.objeto()) == 0) {
                Trie<list<Objetivo>::iterator> nuevoDic;
                _diccObjetivos.insert(make_pair(o.objeto(), nuevoDic));
            }
            (_diccObjetivos.at(o.objeto())).insert(make_pair(o.receptaculo(), it));
        } else {
            _cantObjetivosRealizados++;
        }
    }
}

bool Simulacion::seCumpleObjetivo(const Objetivo &o) {
    Coordenada c = _objPos.at(o.objeto());
    return _mapa.coloresCasilla(c) == o.receptaculo();
}

Coordenada Simulacion::posJugador() const {
    return _agente;
}

Nat Simulacion::cantMovimientos() const {
    return _cantMov;
}

Nat Simulacion::cantObjetivosRealizados() const {
    return _cantObjetivosRealizados;
}

Coordenada Simulacion::posObjeto(const Color &c) {
    return _objPos.at(c);
}

const list <Objetivo> &Simulacion::objetivosDisponibles() const {
    return _objetivosDisponibles;
}

Coordenada Simulacion::_siguientePosicion(Coordenada c, Direccion d) {
    Coordenada res = c;

    switch (d) {
        case ARRIBA:
            res.second++;
            break;
        case ABAJO:
            res.second--;
            break;
        case DERECHA:
            res.first++;
            break;
        case IZQUIERDA:
            res.first--;
            break;
    }
    return res;
}


// Funciones para la traducción
Mapa Simulacion::mapa() const {
    return _mapa;
}

const map <Color, Coordenada> &Simulacion::objetos() const {
    return _objetos;
}
