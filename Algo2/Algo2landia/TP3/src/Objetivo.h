#ifndef TP3_ALGO2LANDIA_OBJETIVO_H
#define TP3_ALGO2LANDIA_OBJETIVO_H

#include "Tipos.h"

class Objetivo {
public:

    Objetivo();

    // Nuestro nuevoObjetivo
    Objetivo(Color objeto, Color receptaculo);

    // nuestro colorObjeto
    const Color &objeto() const;

    // Nuestro colorDestino
    const Color &receptaculo() const;

private:
    Color _receptaculo;
    Color _objeto;
};

#endif //TP3_ALGO2LANDIA_OBJETIVO_H
