#ifndef TP3_ALGO2LANDIA_MAPA_H
#define TP3_ALGO2LANDIA_MAPA_H

#include "Tipos.h"

using namespace std;

class Mapa {

public:

    Mapa();

    // Constructor
    Mapa(Nat ancho, Nat alto, const map<Color, Coordenada> &receptaculos);

    // Agregar rampa
    void agregarRampa(Coordenada c);

    // Agregar elevación
    void agregarElevacion(Coordenada c);

    // Calcular el ancho del mapa
    Nat ancho() const;

    // Calcular el alto del mapa
    Nat alto() const;

    // Decir si una casilla es elevación
    bool esElevacion(Coordenada c) const;

    // Decir si una casilla es rampa
    bool esRampa(Coordenada c) const;

    // Decir si una casilla es piso
    bool esPiso(Coordenada c);

    // Decidir si una coordenada se encuentra en rango
    bool enRango(Coordenada c) const;

    Color coloresCasilla(Coordenada c);



    // Funciones para la traducción:
    const map<Color, Coordenada> &receptaculos() const;

    Coordenada receptaculo(const Color &c);

private:
    Nat _ancho;
    Nat _alto;
    vector<vector<pair<TipoCasillero, Color>>> _casillas;

    // Variables para la traducción:
    map<Color, Coordenada> _receptaculos;
};


#endif //TP3_ALGO2LANDIA_MAPA_H
