
#include "string_map.h"

template <typename T>
string_map<T>::string_map(){
    raiz = nullptr;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if (&d != this) {
        _destruir(raiz);
        _crear(d.raiz, "");
    }
}

template<typename T>
void string_map<T>::_crear(Nodo* nodo, string clave) {
    if (nodo != nullptr) {
        if (nodo->definicion != nullptr)
            insert(make_pair(clave, *nodo->definicion));

        for (int i = 0; i < nodo->siguientes.size(); i++)
            if (nodo->siguientes[i] != nullptr)
                _crear(nodo->siguientes[i], clave + (char) i);
    }
}

template <typename T>
string_map<T>::~string_map(){
    _destruir(raiz);
}

template<typename T>
void string_map<T>::_destruir(Nodo* nodo) {
    if (nodo != nullptr) {
        for (Nodo *n: nodo->siguientes)
            _destruir(n);

        if (nodo->definicion != nullptr)
            delete nodo->definicion;

        delete nodo;
    }
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& clave) {
    if (raiz == nullptr)
        raiz = new Nodo;

    Nodo* temp = raiz;

    for (int i = 0; i < clave.first.length(); i++) {
        int pos = int(clave.first[i]);

        if (temp->siguientes[pos] == nullptr) {
            if (i == clave.first.length() - 1) {
                T *def = new T(clave.second);
                temp->siguientes[pos] = new Nodo(def);
            } else {
                temp->siguientes[pos] = new Nodo;
            }
        } else {
            if (i == clave.first.length() - 1) {
                *temp->siguientes[pos]->definicion = clave.second;
            }
        }

        temp = temp->siguientes[pos];
    }

    _size++;
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 0;

    if (raiz != nullptr) {
        Nodo* temp = raiz;
        int i = 0;

        while (temp != nullptr && i < clave.length())
            temp = temp->siguientes[int(unsigned(clave[i++]))];

        if (temp != nullptr && temp->definicion != nullptr)
            res = 1;
    }

    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* temp = raiz;

    for (unsigned char i : clave)
        temp = temp->siguientes[int(i)];

    return *temp->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* temp = raiz;

    for (unsigned char c : clave)
        temp = temp->siguientes[int(c)];

    return *temp->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    bool seBorroRaiz = _erase(raiz, clave, 0);

    if (seBorroRaiz)
        raiz = nullptr;
}

template<typename T>
bool string_map<T>::_erase(Nodo* nodo, const string& clave, int i) {
    bool res = false;

    if (i == clave.length()) {
        delete nodo->definicion;

        if (_noTieneHijos(nodo)) {
            delete nodo;
            res = true;
        } else {
            nodo->definicion = nullptr;
        }

        _size--;
    } else {
        bool seBorro = _erase(nodo->siguientes[int(unsigned(clave[i]))], clave, i + 1);

        if (seBorro)
            nodo->siguientes[int(unsigned(clave[i]))] = nullptr;

        if (_noTieneHijos(nodo) && nodo->definicion == nullptr) {
            delete nodo;
            res = true;
        }
    }

    return res;
}

template<typename T>
bool string_map<T>::_noTieneHijos(Nodo* nodo) {
    bool noTieneHijos = true;
    int i = 0;

    while (noTieneHijos && i < nodo->siguientes.size())
        noTieneHijos = nodo->siguientes[i++] == nullptr;

    return noTieneHijos;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}
