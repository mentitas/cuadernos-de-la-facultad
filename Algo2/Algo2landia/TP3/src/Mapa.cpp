#include "Mapa.h"

Mapa::Mapa() {}

Mapa::Mapa(Nat ancho, Nat alto, const map<Color, Coordenada> &receptaculos) : _ancho(ancho), _alto(alto),
                                                                              _receptaculos(receptaculos) {
    _casillas = vector<vector<pair<TipoCasillero, Color>>>(ancho);

    for (int i = 0; i < ancho; i++)
        _casillas[i] = vector<pair<TipoCasillero, Color>>(alto, make_pair(PISO, ""));

    for (pair<Color, Coordenada> p: receptaculos) {
        Coordenada c = p.second;
        _casillas[c.first][c.second].second = p.first;
    }
}

void Mapa::agregarRampa(Coordenada c) {
    _casillas[c.first][c.second].first = RAMPA;
}

void Mapa::agregarElevacion(Coordenada c) {
    _casillas[c.first][c.second].first = ELEVACION;
}

Nat Mapa::ancho() const {
    return _ancho;
}

Nat Mapa::alto() const {
    return _alto;
}

bool Mapa::esElevacion(Coordenada c) const {
    return _casillas[c.first][c.second].first == ELEVACION;
}

bool Mapa::esRampa(Coordenada c) const {
    return _casillas[c.first][c.second].first == RAMPA;
}

bool Mapa::esPiso(Coordenada c) {
    return _casillas[c.first][c.second].first == PISO;
}

bool Mapa::enRango(Coordenada c) const {
    return (0 <= c.first && c.first < _ancho) && (0 <= c.second && c.second < _alto);
}

Color Mapa::coloresCasilla(Coordenada c) {
    return _casillas[c.first][c.second].second;
}



// Funciones para la traducción:
const map<Color, Coordenada> &Mapa::receptaculos() const {
    return _receptaculos;
}

Coordenada Mapa::receptaculo(const Color &c) {
    return _receptaculos.at(c);
}