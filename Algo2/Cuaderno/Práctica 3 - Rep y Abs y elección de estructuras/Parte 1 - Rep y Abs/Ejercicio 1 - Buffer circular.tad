Ejercicio 1
El siguiente TAD modela una cola que puede contener a lo sumo n elementos.

--------------------------------------
TAD ColaAcotada

    observadores básicos:
        verCola   : cacotada → cola(nat)
        capacidad : cacotada → nat

    generadores:
    	vacia   : nat → cacotada
    	encolar : nat × cacotada c → cacotada             {tamaño(verCola(c)) < capacidad(c)}
    
    axiomas
    	verCola(vacia(c))       ≡ vacia
    	verCola(encolar(a,c))   ≡ encolar(a, verCola(c))
    	capacidad(vacia(n))     ≡ n
    	capacidad(encolar(a,c)) ≡ capacidad(c)

Fin TAD
--------------------------------------

Como estructura de representación se propone utilizar un buffer circular. Esta estructura almacena los
k elementos de la cola en posiciones contiguas de un arreglo, aunque no necesariamente en las primeras k
posiciones. Para ello, se utilizan dos ı́ndices que indican en qué posición empieza y en qué posición termina la
cola. Los nuevos elementos se encolan “a continuación” de los actuales tomando módulo n, es decir, si el último
elemento de la cola se encuentra en la última posición del arreglo, el próximo elemento a encolar se ubicará en
la primera posición del arreglo. Notar que para desencolar el primer elemento de la cola, simplemente se avanza
el ı́ndice que indica dónde empieza la cola (eventualmente volviendo éste a la primera posición del arreglo). En
nuestra implementación, además de esto, se pone en cero la posición que se acaba de liberar. Se propone la
siguiente estructura de representación.

--------------------------------------
cacotada se representa con estr, donde
estr es tupla
⟨inicio: nat,
fin: nat,
elem: array[0 . . . n] de nat ⟩
--------------------------------------

Se pide:
a) Definir el invariante de representación y la función de abstracción.
b) Escribir la interface completa y todos los algoritmos.

***

a. Definir el invariante de representación y la función de abstracción.

*Describo el invariante de representación en castellano:

1. inicio y fin son naturales mayores que 0 y menores que la longitud del arreglo
2. entre la posición inicio y fin del arreglo, todas las posiciones del arreglo tienen elemento


*Describo el invariante de representación formalmente:

Rep : estr → booleano

∀ (e : estr)
Rep(e) ≡ 1 ∧L 2

Donde
	1 ≡ 0 ≤ inicio < tam(e.elem) ∧ 0 ≤ e.inicio < tam(e.elem)
	2 ≡ (∀ i : nat)(enRango(tam(e.elem), e.inicio, e.fin, i)
		⟹ (definido?(e.elem, i) ∧ (definido?(e.elem, i) ⟹L e.elem[i] ≠ 0)))

	enRango : nat × nat × nat × nat → bool
	enRango(tam, inicio, fin, i) ≡ if (inicio < fin)
	                                   then inicio ≤ i ≤ fin
	                                   else 0 ≤ i ≤ fin ∨ inicio ≤ i < tam
	                                fi





*Describo la función de abstracción formalmente:

abs : estr e → cola acotada {Rep(e)}

∀ (e : estr)
Abs(e) =obs a |

	capacidad(a) = tam(elem)
	∧ noPertenece(verCola(a), 0)
	∧ mismosElementos(elem, verCola(a), inicio, fin)





// Funciones auxiliares

	noPertenece : cola × nat → bool
	noPertenece(cola, n) ≡  if tamaño(cola) = 0
	                           then true
	                           else proximo(cola) ≠ n ∧ noPertenece(desencolar(cola), n)
	                        fi

	//voy a recorrer la cola en orden
	//voy a recorrer el array de forma desfazada (de inicio a fin)
	mismosElementos : array × cola × nat → bool
	mismosElementos(elem, cola, inicio, fin) ≡ if tamaño(cola) = 0
	                                              then true
	                                              else elem[posiciónActual(elem, cola, inicio)] = proximo(cola)
	                                                   ∧ mismosElementos(elem, desencolar(cola), inicio, fin)
	                                           fi

	//Elemento actual = tam(elem) - tamaño(cola) + inicio
	posiciónActual : array e × cola c × nat i → nat         {0 ≤ i < tam(e) ∧ tam(e) ≤ tamaño(c)}
	posiciónActual(elem, cola, inicio) ≡ moduloN(tamaño(cola), tam(elem) - tamaño(cola) + inicio)

	moduloN : nat n × nat i → nat        {0 ≤ i ∧ 0 ≤ n}
	moduloN(n, i) ≡ if i>n then modulo(n, i-n) else i fi
	




b. Escribir la interface completa y todos los algoritmos.

> Módulo: cola
> Se explica con: TAD ColaAcotada
> Géneros: cola
> Operaciones: Crear, verCola, encolar.



Interfaz

    parámetros formales: //¿Qué es ésto?
    
    géneros: cola
    
    se explica con: TAD colaAcotada

    operaciones:

        Crear(in n : nat) → res : cola
        Pre ≡ { true }
        Post ≡ { res^ =obs vacía(n) }
        Descripción: Crea una cola acotada vacía de longitud n.
        Complejidad: Θ(1) //Porque estoy representando la cola como un array
        Aliasing: No presenta aspectos de aliasing.

        Encolar(inout c : cola, in n : nat) → res : bool
        Pre ≡ { c = c₀}
        Post ≡ { (res = tamaño(verCola(c₀^)) < capacidad(c₀^)) ∧ (res ⇒L c^ = verCola(encolar(n, c₀^)))}
        Descripción: Si hay capacidad en la cola, agrega un elemento al final
        Complejidad: Θ(1) //Porque estoy representando la cola como un array
        Aliasing: No presenta aspectos de aliasing.

        verElem(in c : cola, in n : nat) → res : nat
        Pre ≡ { 0 ≤ n < tamaño(verCola(c^)) }
        Post ≡ { res = verCola(c^)[n] }
        Descripción: Devuelve el n-ésimo elemento de la cola
        Complejidad: Θ(1) // Porque estoy accediendo el elemento m de un array
        Aliasing: No presenta aspectos de aliasing.

    algoritmos:

    	function Crear(in n : nat) → res : cola
    		res.elem ← array[n]
    		res.inicio ← 0
    		res.fin ← 0

    	function Encolar(inout c : cola, in n : nat) → res : bool
    		if tamaño(verCola(c^)) < capacidad(c^)) then
               res ← true
               array ← cola.elem
               array[cola.fin] ← n
            else
                res ← false
            end if

        function verElem(in c : cola, in n : nat) → res : nat
        	res ← cola.elem[moduloN(capacidad(c^), elem.inicio + n)]

Fin Interfaz