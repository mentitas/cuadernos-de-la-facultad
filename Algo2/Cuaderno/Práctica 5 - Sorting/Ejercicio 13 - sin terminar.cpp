// Ejercicio 13 ★
//
// Se tiene un arreglo A[1...n] de T , donde T son tuplas ⟨c₁ : nat × c₂ : string[l]⟩ y
// los string[l] son strings de longitud máxima l. Si bien la comparación de dos nat toma O(1),
// la comparación de dos string[l] toma O(l). Se desea ordenar A en base a la segunda componente
// y luego la primera.
// 
// 1. Escriba un algoritmo que tenga complejidad temporal O(n*l + n log(n)) en el peor caso.
// Justifique la complejidad de su algoritmo.

// uso MergeSort porque es estable y re fachero
void TuplaSort(vector<pair<int,string>> &v){
	MergeSort_segundoComponente(v); // O(n*l*log(n))
	MergeSort_primerComponente(v);  // O(n*log(n))
}



// O(n*l*log(n))
vector<int> MergeSort(vector<T> &v, int desde, int hasta){ // desde y hasta incluyen
	int n = v.size();
	if (n<2){
		// ya está ordenado                         // O(1)
	} else {
		int medio = (desde+hasta) div 2;
		vector<T> A = MergeSort(v, desde,   medio); // T(n/2) 
		vector<T> B = MergeSort(v, medio+1, hasta); // T(n/2)
		v = Merge(A, B);                            // O(n*l)
	}
	return v;
}

// θ(n*l)
template<class T>
vector<T> Merge(vector<T> A, vector<T> B){

	vector<T> res(A.size()+B.size()); // θ(|A| + |B|)

	int a=0;   // θ(1)
	int b=0;   // θ(1)
	while(a<A.size() && b<B.size()){  // O(|A| + |B|)
		if(A[a] < B[b]){              // O(l)
			res.push_back(A[a]);      // θ(1)
			a++;                      // θ(1)
		} else {
			res.push_back(B[b]);      // θ(1)
			b++;
		}
	}

	while(a<A.size()){         // O(|A|)
		res.push_back(A[a]);   // θ(1)
		a++;                   // θ(1)
	}

	while(b<B.size()){         // O(|B|)
		res.push_back(B[b]);   // θ(1)
		b++;                   // θ(1)
	}

	return res;                // θ(|A|+|B|)
}


// Calculo la nueva complejidad con el teorema maestro:

T(n) = 2T(n/2) + n*l   si n>1
T(n) = 1               si n=1

Caso 2: f(n) = θ(n^(log_c(a)))
             = θ(n^(log_2(2)))
             = θ(n^1)
             = θ(n)
             = θ(n*l)

             ⟹ T(n) = (n*l*log(n))




// 2. Suponiendo que los naturales de la primer componente están acotados, adapte su algoritmo
// para que tenga complejidad temporal O(nl) en el peor caso.
// Justifique la complejidad de su algoritmo.


// Idea 1: 
// Hago counting con la parte entera: O(n)
// Ordeno la parte string: O(n*l*log(n))


// Idea 2:
// 


// ???????????????
             
void TuplaSort(vector<pair<int,string>> &v){

}