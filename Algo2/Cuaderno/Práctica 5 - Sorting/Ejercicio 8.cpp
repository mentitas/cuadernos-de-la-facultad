// Ejercicio 8
//
// Se tienen dos arreglos de números naturales, A[1..n] y B[1..m]. Nada en especial se sabe de B,
// pero A tiene n' secuencias de números repetidos continuos 
// (por ejemplo A = [3333311118888877771145555], n' = 7). Se sabe además que n' es mucho más
// chico que n. Se desea obtener un arreglo C de tamaño n + m que contenga los elementos de A y B,
// ordenados.
//
// 1. Escriba un algoritmo para obtener C que tenga complejidad temporal O(n + (n'+ m) log(n'+ m))
// en el peor caso. Justifique la complejidad de su algoritmo.

// Obs: no puedo usar counting porque los elementos dentro de A y B no están acotados

// n' = ñ


void superMergeLoco (vector<int> A, vector<int> B){
	
	int n = A.size();
	int m = B.size();

	vector<int> A_comprimido = comprimir(A); // O(n)
	vector<int> A''          = sinRepes(A);  // O(n)

	// Uso '' en vez de ' porque sino se me rompen los comentarios

	// Por ejemplo:
    // A=[1,5,5,6]
    // A_comprimido=[(1,1),(5,2),(6,1)]
    // A''=[1,5,6]

	C = concatenar(A'',B);                         // O(ñ+ m)
	MergeSort(C);                                  // O((ñ+m)*log(ñ+m))
	MergeSort(A_comprimido);                       // O(ñ*log(ñ))
	ReconstruirRepeticiones(C, A_comprimido, n+m); // O(n + m)

	return C; // O(n + m)
}

void ReconstruirRepeticiones(vector<int> C, vector<pair<int,int>> zip, int nm){

	vector<int> res = vector<int>(nm,0); // O(nm) = O(n+m)

	int i=0; // Recorre res
	int j=0; // Recorre C
	int k=0; // Recorre zip
	
	while(i < nm){                    // O(nm) = O(n+m)
		if (C[j] = zip[k].first){     // O(1)               
			// descomprimo                   
			while(zip[k].second > 0){   // O(n)       
				res[i] = C[j];          // O(1)     
				zip[k].second--;        // O(1)     
				i++;                    // O(1)  
			}
			k++; // O(1)                  
			j++; // O(1)
		}else{
			res[i] = C[j]; // O(1) 
			i++;           // O(1)
			j++;           // O(1)
		}

}


// Complejidad:

// O(n) + O(ñ+m) + O(n+m) + O(ñ*log(ñ)) + O((ñ+m)*log(ñ+m))

// Obs: 
//      O(n)              ⊆ O(n+m)
//    ∧ O(ñ+m)            ⊆ O(n+m)
//    ∧ O(ñ*log(ñ))       ⊆ O((ñ+m)*log(ñ+m))
//    ∧ O((ñ+m)*log(ñ+m)) ⊆ O((n+m)*log(n+m))

// Entonces:

// O(n) + O(ñ+m) + O(n+m) + O(ñ*log(ñ)) + O((ñ+m)*log(ñ+m))
// = O(n + m + ñ + ñ*log(ñ) + (ñ+m)*log(ñ+m))
// = O(n + m + ñ + (ñ+m)*log(ñ+m))
// = O(n + (ñ+m)*log(ñ+m)










// 2. Suponiendo que todos los elementos de B se encuentran en A, escriba un algoritmo para
// obtener C que tenga complejidad temporal O(n + n' (log(n' ) + m)) en el peor caso y que utilice
// solamente arreglos como estructuras auxiliares. Justifique la complejidad de su algoritmo.

// Pre: B ⊆ A

void superMergeLoco (vector<int> A, vector<int> B){
	
	int n = A.size();
	int m = B.size();

	vector<pair<int,int>> comprimido = comprimir(A); // O(n)

	int ñ = comprimido.size(); 

	// Por ejemplo:
    // A=[1,5,5,6]
    // A_comprimido=[(1,1),(5,2),(6,1)]

	MergeSort(comprimido);                         // O(ñ*log(ñ))
	agregarAComprimido(comprimido, B);             // O(m)
	vector<int> C = Descomprimir(comprimido)       // O(n + m)

	return C; // O(n + m)
}

// Pre: zip ⊇ B
void agregarAComprimido(vector<pair<int,int>> zip, vector<int> B){
	int ñ = zip.size();
	int m = B.size();

	int i=0; // Recorre zip
	int j=0; // Recorre B

	while(j<m){                    // O(m)
		if(B[j] == zip[i].first){  // O(1)
			zip[i].second++;       // O(1)   
			j++;                   // O(1)     
		} else {
			j++;                   // O(1)
			i++;                   // O(1)
		}
	}
}

// Complejidad:

//   O(n) + O(m) + O(ñ*log(ñ)) + O(n+m)
// = O(n + m+ ñ*log(ñ))