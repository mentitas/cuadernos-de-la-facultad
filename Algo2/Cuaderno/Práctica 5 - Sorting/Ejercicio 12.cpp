// Ejercicio 12

// Se desea ordenar los datos generados por un sensor industrial que monitorea la presencia
// de una sustancia en un proceso quı́mico. Cada una de estas mediciones es un número entero
// positivo. Dada la naturaleza del proceso se sabe que, dada una secuencia de n mediciones,
// a lo sumo √n  valores están fuera del rango [20, 40].
// Proponer un algoritmo O(n) que permita ordenar ascendentemente una secuencia de mediciones
// y justificar la complejidad del algoritmo propuesto.


void sustanciaSort(vector<int> &v){
	int n = v.size();             // O(1)
	int sqrt = cuadradoEntero(n); // O(1)

	vector<int> enRango(n-sqrt,0); // O(n-sqrt) = O(n)        
	vector<int> noRango(sqrt,  0); // O(sqrt)   = O(n)  

	int i=0; // Recorre enRango
	int j=0; // Recorre noRango

	for(int x : v){              // O(n)
		if (x >= 20 && x<=40){   // O(1)
			enRango[i] = x;      // O(1)   
			i++;
		} else {
			noRango[j] = x;      // O(1)    
			j++;
		}
	}

	countingSort(enRango);  // O(n), hecho en el ejercicio 11.
	selectionSort(noRango); // O(|noRango|²), pero |noRango| = √n, entonces es O(n)
}

// Complejidad total del algoritmo: O(n)