// Ejercicio 7
// Suponga que su objetivo es ordenar arreglos de naturales (sobre los que no se conoce nada en
// especial), y que cuenta con una implementación de árboles AVL. ¿Se le ocurre alguna forma de
// aprovecharla? Conciba un algoritmo que llamaremos AVL Sort. ¿Cuál es el mejor orden de
// complejidad que puede lograr?
// Ayuda: deberı́a hallar un algoritmo que requiera tiempo O(n log d) en peor caso, donde n es la
// cantidad de elementos a ordenar y d es la cantidad de elementos distintos. Para inspirarse,
// piense en Heap Sort (no en los detalles puntuales, sino en la idea general de este último).
// Justifique por qué su algoritmo cumple con lo pedido.

-----------------------------------------------------------------------------------------

Inspirándome en Heap Sort, si primero paso todos los elementos a un AVL (O(n * log(n)))
y luego voy reconstruyendo el array retirando de a uno el mínimo del AVL (O(n * log(n))),
entonces la complejidad total del algoritmo sería O(n*log(n))

Si primero comprimo el array en tuplas (elemento, cdadDeRepeticiones), entonces me queda un
array de d elementos. Comprimirlo es O(n).
Si ahora los paso al AVL, entonces insertarlos es O(d*log(d)) y retirarlos es O(d*log(d)).
Luego descomprimirlos es O(n).

Luego:

O(n) + O(d*log(d)) = O(n + d*log(d)) ⊆ O(n + n*log(d)) = O(n*log(d))

El algoritmo sería:

void AVLSort(vector<int> v){

	vector<pair<int,int> comprimido = comprimir(v); // O(n)

	int n = v.size();          // O(1)
	int d = comprimido.size(); // O(1)

	AVL avl = {}; // O(1)

	for(int i=0; i<d; i++){        // O(d*log(d))
		avl.insert(comprimido[i]); // O(lod(d)) 
	}

	int minimo=0; // O(1)
    
    for(int i=0; i<d; i++){          // O(d*log(d))
		minimo = buscarMinimo(avl);  // O(log(d))
		avl.erase(minimo);           // O(log(d))
		comprimido[i] = minimo;      // O(1)
	}

	v = descomprimir(comprimido); // O(n)
}