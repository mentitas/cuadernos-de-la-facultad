// Ejercicio 18 ★

// 1. Dar un algoritmo que ordene un arreglo de n enteros positivos menores que n en tiempo O(n).

// Pre: A tiene n elementos y todos son naturales menores que n
void CountingSort(vector<int> &A){
	int n = A.size();             // θ(1)
	vector<int> apariciones(n,0); // θ(n)

	for(int e : A) apariciones[e]++; // θ(n)

	int i=0; // Recorre apariciones
	int j=0; // Recorre A
	while(i<n){ // θ(n)
		if (apariciones[i]>0){ // θ(1) 
			A[j] = i;          // θ(1)
			j++;               // θ(1)
			apariciones[i]--;  // θ(1)
		} else {
			i++;               // θ(1)
		}
	}
}


// 2. Dar un algoritmo que ordene un arreglo de n enteros positivos menores que n² en tiempo O(n).
// Pista: Usar varias llamadas al ı́tem anterior.


// Voy a usar radix

// Todos los elementos son menores que n², por lo que tienen como máximo log_b(n²) dígitos
// (siendo b la base que estoy usando).

// Si escribo los elementos del arreglo en base n, van a tener a lo sumo 2 dígitos.
// Cada dígito es un entero entre 0 y n, y ordenar cada dígito es O(n).
// La complejidad total es O(2*n) = O(n)


pair<int,int> pasarABaseN(int e, int n){
	int a = log_n(e);
	int b = log_n(e - n^a);
	return make_pair(a,b);
}

int pasarDeBaseN(pair<int,int> p, int n){
	return p.first * n^2 + p.second * n;
}



// Pre: A tiene n elementos y todos son naturales menores que n²
void CountingSort_2(vector<int> &A){
	int n = A.size(); // θ(1)
	vector<pair<int,int>> enBaseN(n, make_pair(0,0)); // θ(n)

	for(int i=0; i<n; i++){               // θ(n)
		enBaseN[i] = pasarABaseN(A[i],n); // θ(1)
	}

	CountingSort_2doElem(enBaseN); // θ(n)
	CountingSort_1erElem(enBaseN); // θ(n)

	for(int i=0; i<n; i++){
		A[i] = pasarDeBaseN(enBaseN[i],n); // θ(1)
	}
}


// 3. Dar un algoritmo que ordene un arreglo de n enteros positivos menores que nᵏ para una constante
// arbitraria pero fija k.

// 4. ¿Qué complejidad se obtiene si se generaliza la idea para arreglos de enteros no acotados?