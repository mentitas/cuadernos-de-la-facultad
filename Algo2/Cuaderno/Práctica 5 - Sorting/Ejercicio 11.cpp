// Ejercicio 11 ★

// Sea A[1 . . . n] un arreglo de números naturales en rango (cada elemento está en el rango de
// 1 a k, siendo k alguna constante). Diseñe un algoritmo que ordene esta clase de arreglos en
// tiempo O(n). Demuestre que la cota temporal es correcta.

void CountingSort(vector<int> &v){
	int n = v.size();  // O(1)
	int k = máximo(v); // O(n)

	vector<int> arregloAux(k, 0); // O(k)

	for(int elem : v){            // O(n)
		arregloAux[elem]++;
	}

	int i=0; // Recorre arregloAux
	int j=0; // Recorre v
	while(j<n){                    // O(n)
		while(arregloAux[i]>0){    
			arregloAux[i]--;       // O(1)
			v[j] = i+1;            // O(1)
			j++;                   // O(1)
		}
		i++;                       // O(1)
	}
}

// La complejidad es O(k+n), pero dado que k es un número constante, la complejidad total
// es O(n).