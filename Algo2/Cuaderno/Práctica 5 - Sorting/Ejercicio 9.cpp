// Ejercicio 9 ★

//Considere la siguiente estructura para guardar las notas de un alumno de un curso:

-----------------------------------------------------------------------------------------
alumno es tupla ⟨nombre: string,
                 sexo: FM,
                 puntaje: nota⟩

donde FM es enum{masc,fem} y nota es un nat no mayor que 10
-----------------------------------------------------------------------------------------

// Se necesita ordenar un arreglo(alumno) de forma tal que todas las mujeres aparezcan al inicio
// de la tabla según un orden creciente de notas y todos los varones aparezcan al final de la
// tabla también ordenados de manera creciente respecto de su puntaje, como muestra en el
// siguiente ejemplo:

Entrada:              Salida:
Ana    F  10          Rita   F   6   
Juan   M   6          Paula  F   7
Rita   F   6          Ana    F  10
Paula  F   7          Juan   M   6
Jose   M   7          Jose   M   7
Pedro  M   8          Pedro  M   8

// 1. Proponer un algoritmo de ordenamiento ordenaPlanilla(inout p: planilla) para resolver el
// problema descripto anteriormente y cuya complejidad temporal sea O(n) en el peor caso, donde
// n es la cantidad de elementos del arreglo. Justificar.


// Idea:
// Armo 2 arreglos con 10 elementos cada uno (uno M y el otro F)
// En cada posición del arreglo armo una lista con los nombres de las personas que tengan
// esa nota.
// Luego rearmo la planilla
// Es O(n)


using FM {masc, fem};
using nota = unsigned int;

class alumno{
public:
    string nombre();
    FM sexo();
    Nota nota()
private:
    string _nombre();
    FM _sexo();
    nota _nota();
}

using planilla = vector<alumno>;


void ordenaPlanilla(planilla & p){
    vector<list<alumno>> notasF(10, list<alumno>);  // O(1)
    vector<list<alumno>> notasM(10, list<alumno>);  // O(1)

    for(alumno a : p){                                // O(n)
        if (a.sexo() == fem){                         // O(1)
            notasF[a.nota()-1].push_back(a.nombre()); // O(1)
        } else {
            notasM[a.nota()-1].push_back(a.nombre()); // O(1)
        }
    }
    
    int j=0; // recorre p


    for(list<alumno> l : notasF){ // A
        if (!l.empty()){          // O(1)              
            for(alumno a : l){               
                p[j] = a;         // O(1)
                j++;              // O(1)
            }
        } 
    }

    for(list<alumno> l : notasM){ // B
        if (!l.empty()){          // O(1)              
            for(alumno a : l){    
                p[j] = a;         // O(1)
                j++;              // O(1)
            }
        } 
    }

    // A+B = O(n)
}

// La complejidad total del algoritmo es O(n)





// 2. Supóngase ahora que la planilla original ya se encuentra ordenada alfabéticamente por nombre.
// ¿Puede asegurar que si existen dos o más mujeres con igual nota, entonces las mismas aparecerán
// en orden alfabético en la planilla ordenada? Justifique su respuesta.

Si, van a seguir en orden alfabético porque en mi algoritmo:

> Veo la nota del alumnx
> Lx meto al final de la lista correspondiente según la nota          (hago push_back)
> Luego rearmo la planilla completa respetando el orden de las listas (la recorro de adelante
                                                                       para atrás)
Es decir que mi algoritmo es estable.


// 3. ¿La cota O(n) contradice el “lower bound” sobre la complejidad temporal en el peor caso de
// los algoritmos de ordenamiento? (El Teorema de “lower bound” establece que todo algoritmo
// general de ordenamiento tiene complejidad temporal Ω(n log n).) Explique su respuesta.

No lo contradice porque el teorema establece esa cota para arreglos de los cuales no se nada al
respecto.
En éste ejercicio, yo tengo que ordenar los alumnxs según un valor que es acotado (la nota, 
menor que 10), entonces por eso tengo mejor complejidad que el lowerbound.