// Ejercicio 10
// Suponer que se tiene un hipotético algoritmo de “casi” ordenamiento casiSort que dado un
// arreglo A de n elementos, ordena n/2 elementos arbitrarios colocándolos en la mitad izquierda
// del arreglo A[1 . . . n/2]. Los elementos no ordenados de A se colocan en la mitad derecha 
// A[n/2 + 1 . . . n].

// 1. Describir un algoritmo de ordenamiento para arreglos de n elementos (con n potencia de 2)
// utilizando el algoritmo casiSort.

// Idea:

// A = [5,3,0,6,1,2,4,7]
// CasiSort(A,0,|A|)

// A = [0,3,5,6,1,2,4,7]
        - - - - 

// CasiSort(A, |A|/2, |A|)

// A = [0,3,5,6,1,2,4,7]
        - - - - - -

// CasiSort(A, 3|A|/4, |A|)

// A = [0,3,5,6,1,2,4,7]
        - - - - - - - 

// Merge(todo lol)

// A = [0,1,2,3,4,5,6,7]




vector<int> CompletoSort(vector<int> v){ 
	int n = v.size();
	if (n>1){ // T(n)

		vector<int> izq(n div 2, 0); // O(n div 2) = O(n)
		vector<int> der(n div 2, 0); // O(n div 2) = O(n)

		CasiSort(v);

		for(int i=0; i<n, i++){        // O(n)
			if (i <= n div 2){     // O(1)
				izq[i] = v[i]; // O(1)
			} else {
				der[i] = v[i]; // O(1)
			}
		}

		der = CompletoSort(der);   // T(n/2)

		return = Merge(izq, der);  // O(n)

	} else { 
		return v;                  // O(n)
	}


}

// desde incluye, hasta no incluye
vector<int> CompletoSort(vector<int> v, int desde, int hasta){
	int n = v.size();
	if (n>1){
		CasiSort(v);                                  // θ(n)
		CompletoSort(v, (desde+hasta) div 2, hasta);  // T(n/2)
		MergeDosMitades(v, desde, hasta)              // θ(n)
	} else { 
		// Ya está ordenado                  // O(1)
	}
}

// desde incluye, hasta no incluye
void MergeDosMitades(vector<int> v, int desde, int hasta){

	int medio = (desde+hasta) div 2 // O(1)
	vector<int> der(medio, 0);      // O(medio) = θ(n)
	vector<int> izq(medio, 0);      // O(medio) = θ(n)

	for(int i=desde; i<medio; i++){  // θ(n)/2 
		izq[i] = v[i];               // O(1)
	}

	for(int i=medio; i<hasta; i++){  // θ(n)/2
		der[i-medio] = v[i];         // O(1)
	}

	v = Merge(der, izq); // θ(n)
}


// 2. Obtener la complejidad temporal en el peor caso del algoritmo dado en el punto anterior,
// suponiendo que casiSort tiene complejidad temporal Θ(n).

Es divide and conquer, calculo la complejidad con el teorema maestro:

T(n) = a*T(n/c) + f(n)   si n>1
T(n) = 1                 si n=1

Donde:
a = 2
c = 2
f(n) = 2n

entonces:

T(n) = 2*T(n/2) + 2n



Evaluo los casos:

Caso 1: f(n) = O(n ^(log_c  a - ε))
             = O(n ^(log_2  2 - ε))
             = O(n ^(1 - ε))          → ε=0, no es caso 1

Caso 2: f(n) = θ(n ^(log_c  a))
             = θ(n ^(log_2  2))
             = θ(n ^(1))
             = θ(n)                   → ¡es caso 2!

             Entonces T(n) = θ(f(n)*log(n))
                           = θ(n*log(n))


// 3. Cree que es posible diseñar un algoritmo para casiSort que realice O(n) comparaciones en el
// peor caso? Justifique su respuesta.

No es posible porque el teorema del lowerbound establece que ordenar es Ω(n*log(n))