// Ejercicio 5 ★
// Se tiene un arreglo de n números naturales que se quiere ordenar por frecuencia, y en caso de
// igual frecuencia, por su valor. Por ejemplo, a partir del arreglo [1,3,1,7,2,7,1,7,3] se quiere
// obtener [1,1,1,7,7,7,3,3,2]. Describa un algoritmo que realice el ordenamiento descrito,
// utilizando las estructuras de datos intermedias que considere necesarias. Calcule el orden de
// complejidad temporal del algoritmo propuesto.

// Idea 1: 
// Armo un arreglo donde marco la cdad de repeticiones
// Pro: Tengo en O(1) la cdad de repeticiones
// Contra: el número más alto no es acotado

// Idea 2:
// Armo un arreglo donde en la posición n se encuentren los elementos con n apariciones
// El arreglo es de longitud n (en el peor caso)
// Una vez que tengo cada elemento en el arreglo, tengo que ordenar las listas de cada posición
// del arreglo
// Luego reconstruir el vector original es O(n)
// Contra: saber la cantidad de apariciones de un elemento es O(n)
//         calcularlo para todos los elementos es O(n²)


// Idea 3:
// Comprimo el vector para representar a cada número con su cantidad de apariciones.
// Por ejemplo: v=[1,5,5], v'=[(1,1),(5,2)]
// Comprimir es O(n + |v'|) ⊆ O(n²)

// Idea 4:
// Primero ordeno el arreglo: O(n log(n))
// Comprimir es O(n)
// Ordenar segun cantidad de apariciones es O(n log(n))

void ordenarSegúnAparición(vector<int> &v){
	MergeSort(v);                                            // O(n log(n))
	vector<pair<int,int>> v_comprimido = Comprimir(v);       // O(n)
	MergeSort(v_comprimido);                                 // O(n log(n))
	v = Descomprimir(v_comprimido);                          // O(n)
}

/*
void HeapSort(vector<int> &v){
	heap h = array2heap(v); // O(n)
	int max = 0;
	for(int i=0; i<v.size(); i++){ // O(n log(n))
		max = proximo(h); // O(1)
		desencolar(h);    // O(log(n))
		v[i] = max;       // O(1)
	}
}
*/

// O(n log(n))
vector<int> MergeSort(vector<int> &v, int desde, int hasta){ // desde y hasta incluyen
	int n = v.size();
	if (n<2){
		// ya está ordenado
	} else {
		int medio = (desde+hasta) div 2;
		vector<int> A = MergeSort(v, desde,   medio);
		vector<int> B = MergeSort(v, medio+1, hasta);
		v = Merge(A, B);
	}
	return v;
}

template<class T>
vector<T> Merge(vector<T> A, vector<T> B){

	vector<T> res(A.size()+B.size()); // O(|A| + |B|)

	int a=0;   // O(1)
	int b=0;   // O(1)
	while(a<A.size() && b<B.size()){  // O(|A| + |B|)
		if(A[a] < B[b]){              // O(1)
			res.push_back(A[a]);      // O(1)
			a++;                      // O(1)
		} else {
			res.push_back(B[b]);      // O(1)
			b++;
		}
	}

	while(a<A.size()){         // O(|A|)
		res.push_back(A[a]);   // O(1)
		a++;                   // O(1)
	}

	while(b<B.size()){         // O(|B|)
		res.push_back(B[b]);   // O(1)
		b++;                   // O(1)
	}

	return res;                // O(1)
}

// Pre: v está ordenado
vector<pair<int,int>> Comprimir(vector<int> v){
	int r = 0;                       // O(1)
	int n = v.size();                // O(1)
	vector<pair<int, int>> res = {}; // O(1)
	for(int i=0; i<n; i++){                    // O(n)
		if(r == 0 || v[i] != res[r-1].first){  // O(1)
			res.push_back(make_pair(v[i], 1)); // O(1)
			r++;                               // O(1)
		} else { 
			res[r-1].second++;                 //O(1)
		}
	}
	return res; // O(|res|) ⊆ O(n)
}

vector<int> Descomprimir(vector<pair<int,int>> v){
	int n = v.size();      // O(1)
	vector<int> res = {};  // O(1)
	for(pair<int,int> par : v){              // O(|v| * par.second) = O(n)
		for(int i=0; i<par.second+1; i++){   // O(par.second)
			res.push_back(par.first);        // O(1)
		}
	}
	return res; // O(n)
}



// Faltaría sobreescribir el operador < para que funcione con tuplas
