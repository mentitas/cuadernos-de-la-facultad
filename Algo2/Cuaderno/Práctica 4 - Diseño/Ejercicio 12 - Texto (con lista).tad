Ejercicio 12
Considere el siguiente TAD que describe la estructura de un texto:

TAD Texto

	observadores básicos
		cantPalabras : txt → nat
		enPos        : txt t × nat n → palabra

	generadores
		enBlanco  : → txt
		agPalabra : txt × palabra → txt

	otras operaciones
		cambiarPalabra : txt × palabra × palabra → txt
		posiciones     : txt × palabra → conj(nat)
		subtexto       : txt t × nat ini × nat f in → txt
		masRepetidas   : txt → conj(palabra)

	otras operaciones (no exportadas)
		maxRepeticiones : txt → nat

	axiomas

		cantPalabras(enBlanco)        ≡ 0
		cantPalabras(agPalabra(t, p)) ≡ 1 + cantPalabras(t)

		enPos(agPalabra(t, p),n)         ≡ if cantPalabras(t) = n − 1 then p else enPos(t, n) fi
		
		cambiarPalabra(enBlanco, p, p')          ≡ enBlanco
		cambiarPalabra(agPalabra(t, p''), p, p') ≡ agPalabra(cambiarPalabra(t, p, p'),
			                                                 if p = p'' then p' else p'' fi)

		posiciones(enBlanco, p')       ≡ ∅
		posiciones(agPalabra(t, p),p') ≡ if p = p'
		                                    then Ag(cantPalabras(t)+1, posiciones(t, p')))
											else posiciones(t, p')
			                             fi

		subtexto(t, i, f) ≡ if i = f 
		                       then agPalabra(enPos(t, i), enBlanco)
                               else agPalabra(enPos(t, fS), subtexto(t, i, f − 1))
                            fi

        masRepetidas(agPalabra(t,p)) ≡ if cantPalabras(t) = 0 ∨ #posiciones(t, p) = maxRepeticiones(t)
                                          then Ag(p, ∅)
                                          else
                                               if #posiciones(t, p) + 1 = maxRepeticiones(t)
                                                  then Ag(p, masRepetidas(t))
                                                  else masRepetidas(t)
                                               fi
                                       fi

		maxRepeticiones(t) ≡ if cantPalabras(t) = 0
		                        then 0
		                        else #posiciones(t, dameUno(masRepetidas(t)))
		                     fi
		Fin TAD


// Se desea diseñar el módulo correspondiente al TAD texto. En particular, asumimos que trabajaremos sólo
// con textos en español, y por lo tanto podemos dar una cota para la longitud de la x que puede
// aparecer en el texto.
// 
// Se requiere que las operaciones que se listan a continuación cumplan con la complejidad temporal indicada:
// 
//  > subtexto(in inicio:nat, in fin:nat, in t:txt) → txt
// 	Devuelve el texto correspondiente al fragmento de t que comienza en la posición inicio y finaliza en la
// 	posición fin.
// 	
// 	*O(fin − inicio) en el peor caso
// 
// 
//  > cambiarPalabra(in anterior:palabra, in nueva:palabra, inout t:text)
// 	Cambia todas las ocurrencias en el texto de la palabra anterior por la nueva.
// 	
// 	*O(k) en el peor caso, donde k es la cantidad de veces que se repite la palabra a cambiar.
// 
// 
//  > palabrasMasRepetidas (in t:txt) → conj(palabras)
//    Devuelve el conjunto de palabras que más se repiten en el texto.
// 
//    *O(1) en el peor caso. Puede generar aliasing.
// 
// 
// a) Describir la estructura a utilizar, documentando claramente cómo la misma resuelve el problema y cómo
//    cumple con los requerimientos de eficiencia. El diseño debe incluir sólo la estructura de nivel superior.
//    Para justificar los órdenes de complejidad, describa las estructuras soporte. Importante: si alguna de las
//    estructuras utilizadas requiere que sus elementos posean una función especial (por ejemplo, comparación)
//    deberá describirla.
// 
// b) Escribir una versión en lenguaje imperativo del algoritmo cambiarPalabra. Justifique la complejidad sobre 
//    el código.


// Primer idea:

// Un vector de strings ( que representa el texto total), un diccTrie(palabra, conj(nat)) que marca todas
// las apariciones de una palabra en el texto total y un conjunto con las palabras más repetidas (junto con la
// cantidad de repeticiones)


// Segunda idea:

// texto: vector de strings
// apariciones: diccTrie(palabra, lista(nat))
// másRepes: conj(palabra)


// Tercer idea:
// texto: lista enlazada de strings
// apariciones: diccTrie(palabra, tupla⟨lista(nat), itLista(palabra)⟩)

TAD palabra es string

representación:
	documento se representa con estr, donde
		estr es tupla⟨ texto: lista(palabra),
		               apariciones: diccTrie(palabra, Lista(nat)),
		               aparicionesIt: diccTrie(palabra, Lista(itLista(palabra))),
		               itMásRepes: diccTrie(palabra, itLista(palabra))
		               másRepes: lista(palabra),
		             ⟩

invariante de representación:
	Rep: estr → bool
	(∀e: estr) Rep(e) ≡ true ⟺ 

	1 ≡ claves(e.apariciones) =obs claves(e.itMásRepes) ∧ claves(e.apariciones) =obs claves(e.aparicionesIt)
	2 ≡ (∀ p : palabra)(def?(p, e.apariciones) ⇒L long(obtener(p, e.apariciones)) ⟺ #apariciones(p, e.texto))
	3 ≡ (∀ p : palabra)(∀ i : nat)(def?(p, e.apariciones) ∧L 0 ≤ i < long(obtener(p, e.apariciones)) ⇒L e.texto[i] = p)
	4 ≡ (∀ p : palabra)(∀ i : it)(def?(p, e.aparicionesIt) ∧L está?(i, obtener(p, e.aparicionesIt)) ⇒L Siguiente(it) = p) 
	5 ≡ (∀ p : palabra)(def?(p, e.itMásRepes) ⇒L Siguiente(obtener(p, itMásRepes)) = p)
	6 ≡ (∀ p,q : palabra)(está?(p, másRepes) ∧ ¬está?(q, másRepes) ⇒L 
						 long(obtener(p, e.apariciones)) > long(obtener(q, e.apariciones)))

	Funciones auxiliares:
		#apariciones : string × secu(string) → nat
		#apariciones(p, secu) ≡  if vacía?(secu) then 0 else β(p = prim(secu)) + #apariciones(p, fin(secu)) fi


función de abstracción:
	Abs: estr e → documento { Rep(e) }
	(∀e: estr) Abs(e) =obs d |

		cantPalabras(d) =obs long(e.texto)
		∧ (∀ i : nat)(0 ≤ i < cantPalabras(d) ⇒L enPos(d, i) = e.texto[i-1])


Interfaz

    se explica con: Texto

    géneros: documento

    operaciones:

    	//enBlanco
        enBlanco() → res: documento
        Pre ≡ { true }
        Post ≡ { res =obs enBlanco }
        Descripción: Genera un documento vacío.
        Complejidad: θ(1)
        Aliasing: No presenta aspectos de aliasing

        //agPalabra
        agPalabra(in/out d: documento, in p: palabra)
        Pre ≡ { d = d₀ }
        Post ≡ { d = agPalabra(d₀, p) }
        Descripción: Agrega una palabra al final del documento
        Complejidad: θ(1) //OJO!!!
        Aliasing: El documento es pasado como referencia modificable.

        //cantPalabras
        cantPalabras(in d: documento) → res: nat
        Pre ≡ { true }
        Post ≡ { res =obs cantPalabras(d)}
        Descripción: Devuelve la cantidad de palabras del documento.
        Complejidad: θ(1)
        Aliasing: El documento es pasado como referencia no modificable.

        //enPos
        enPos(in d: documento, in n : nat) → res: palabra
        Pre ≡ { 1 ≤ n ≤ cantPalabras(t) }
        Post ≡ { res =obs enPos(d, n)}
        Descripción: Devuelve la palabra en la posición n del documento
        Complejidad: θ(n)
        Aliasing: El documento es pasado como referencia no modificable.

        //cambiarPalabra
        cambiarPalabra(in/out d: documento, in p: palabra, in q: palabra)
        Pre ≡ { d = d₀ }
        Post ≡ { d = cambiarPalabra(d₀, p, q) }
        Descripción: Cambia todas las ocurrencias en el texto de la palabra anterior por la nueva.
        			 Si la palabra no aparece en el documento, el documento no se modifica.
        Complejidad: θ(k)
        Aliasing: El documento es pasado como referencia modificable.

        //posiciones
        posiciones(in d: documento, in p: palabra) → res: Lista(nat)
        Pre ≡ { true }
        Post ≡ { (∀ i: nat)((1 ≤ i ≤ cantPalabras(d) ∧L enPos(d, i) = p) ⇒L está?(i, res)) }
        Descripción: Devuelve una lista con las posiciones de la palabra p en el documento.
                     Si la palabra no aparece en el texto, devuelve una lista vacía.
        Complejidad: O(1)
        Aliasing: El documento es pasado como referencia no modificable. La lista res es devuelta como
        		  referencia no modificable. 

        //subtexto
        subtexto(in d: documento, in ini: nat, in fin: nat) → res:documento
        Pre ≡ { 1 ≤ ini ≤ f in ≤ cantPalabras(t) }
        Post ≡ { res =obs subtexto(d, ini, fin) }
        Descripción: Devuelve el texto correspondiente al fragmento de t que comienza en la posición inicio 
                     y finaliza en la posición fin. 
        Complejidad: O(fin - ini)
        Aliasing: El documento es pasado como referencia no modificable.

        //másRepetidas
        másRepetidas(in d: documento) → res: lista(palabra)
        Pre ≡ { true }
        Post ≡ { res =obs masRepetidas(d) }
        Descripción: Devuelve la lista de palabras que más se repiten en el texto.
        Complejidad: θ(1)
        Aliasing: El documento es pasado como referencia no modificable. La lista res es devuelta como
        		  referencia no modificable. 

        //máxRepeticiones (hacer???)
        función(in arg1: α, in/out arg2: algo(α)) → res: bool
        Pre ≡ { true }
        Post ≡ { ... }
        Descripción: ...
        Complejidad: Θ(...)
        Aliasing: ...

    algoritmos:


    	// (x es la palabra más larga del documento. Está acotada)

        ienBlanco() → res: documento
        	res.texto         ← Lista::Vacía()  // θ(1)
        	res.apariciones   ← Dicc::Vacío()   // θ(1)
        	res.aparicionesIt ← Lista::Vacía()  // θ(1)
        	res.másRepes      ← Lista::Vacía()  // θ(1)
        	res.itMásRepes    ← Dicc::Vacío()   // θ(1)


        iagPalabra(in/out d: documento, in p: palabra)

           	it ← Lista::AgregarAtrás(d.texto, palabra) // θ(1)
        	pos ← Lista::Longitud(d.texto)             // θ(1)

        	if Dicc::Definido?(e.apariciones, p) then              //O(|x|) = O(1)
        		listaDePos ← Dicc::Significado(e.apariciones, p)   //O(|x|) = O(1)
        		listaDeIts ← Dicc::Significado(e.aparicionesIt, p) //O(|x|) = O(1)
        	else
        		listaDePos ← Lista::Vacía() // θ(1)
        		listaDeIts ← Lista::Vacía() // θ(1)                   
        	fi

        	Lista::AgregarAtrás(listaDePos, pos) // θ(1)
        	Lista::AgregarAtrás(listaDeIts, it)  // θ(1)

        	Dicc::Definir(e.apariciones,   p, listaDePos)    // O(|x|) = O(1)
        	Dicc::Definir(e.aparicionesIt, p, listaDeIts)    // O(|x|) = O(1)

        	if Lista::EsVacía?(e.másRepes) then    // θ(1)
        		Lista::AgregarAtrás(e.másRepes, p) // θ(1)
        	else
        		unaPalabraMáxRepe ← Lista::e.másRepes[0] // θ(1)
        		listaDePosDeMáxRepe ← Dicc::Significado(e.apariciones, unaPalabraMáxRepe) //O(|x|) = O(1)

        		if Lista::Longitud(listaDePos) > Lista::Longitud(listaDePosDeMáxRepe) then // θ(1)
        			e.másRepes ← Lista::Vacía()   // θ(1)
        			e.itMásRepes ← Dicc:: Vacío() // θ(1)
        		fi 

        		if Lista::Longitud(listaDePos) ≥ Lista::Longitud(listaDePosDeMáxRepe)
        			it ← Lista::AgregarAtrás(e.másRepes, p) // θ(1)
        			Dicc::Definir(e.itMásRepes, p, it)      // O(|x|) = O(1)
        		fi
        	fi

        	// Complejidad total del algoritmo: θ(1)


        icantPalabras(in d: documento) → res: nat
        	res ← Lista::Longitud(e.texto) // θ(1)


        ienPos(in d: documento, in n : nat) → res: palabra
        	res ← ::e.texto[n-1] // θ(n)

        
        icambiarPalabra(in/out d: documento, in p: palabra, in q: palabra)

        	if Dicc::Definido?(e.apariciones, p) ∧ ¬Dicc::Definido?(e.apariciones, q) then //O(|x|) = O(1)
				
        		// Cambiar en el texto

				listaDePos ← Dicc::Significado(e.apariciones, p) //O(|x|) = O(1)

				it ← Lista::CrearIt(listaDePos)  // θ(1)

				while Lista::HaySiguiente(it) do       // θ(k)
					e.apariciones[Siguiente(it)-1] ← q // θ(1)
				endwhile


	        	// Definir en apariciones

	        	Dicc::Borrar(e.apariciones, p)              // O(|x|) = O(1)
	        	Dicc::Definir(e.apariciones, q, listaDePos) // O(|x|) = O(1)


	        	if Dicc::Definido?(e.itMásRepes, p) then // O(|x|) = O(1)

	        		// Cambiar en másRepes
	        		it ← Dicc::Significado(e.itMásRepes, p) // O(|x|) = O(1)
	        		
	        		Lista::AgregarComoSiguiente(it, q) // θ(1) - agrego q a la lista, ahora it apunta a q
	        		Lista::EliminarAnterior(it)        // θ(1) - elimino p de la lista

	        		Dicc::Borrar(e.itMásRepes, p)      // O(|x|) = O(1)
	        		Dicc::Definir(e.itMásRepes, q, it) // O(|x|) = O(1)
	        	fi

			else if if Dicc::Definido?(e.apariciones, p) ∧ Dicc::Definido?(e.apariciones, q) then //O(|x|) = O(1)
 				
 				// Actualizo e.texto

				listaDePosP ← Dicc::Significado(e.apariciones, p) //O(|x|) = O(1)

				it ← Lista::CrearIt(listaDePosP) // θ(1)

				while Lista::HaySiguiente(it) do       // θ(1)
					e.apariciones[Siguiente(it)-1] ← q // θ(1)
				endwhile

 				// Concateno la lista de posiciones de q con las listas de posiciones de p

 				listaDePosQ ← Dicc::Significado(e.apariciones, q) //O(|x|) = O(1)
 				listaConcatenada ← Lista::Concatenar(listaDePosQ, listaDePosP) // Ésta función no existe, pero es θ(1)

 				// Borro p de e.apariciones

 			    Dicc::Borrar(e.apariciones, p)                    //O(|x|) = O(1)
	        	Dicc::Definir(e.apariciones, q, listaConcatenada) //O(|x|) = O(1)

 				// Actualizo palabras más repetidas

        		unaPalabraMáxRepe ← Lista::e.másRepes[0] // θ(1)
        		listaDePosDeMáxRepe ← Dicc::Significado(e.apariciones, unaPalabraMáxRepe) //O(|x|) = O(1)

        		if Lista::Longitud(listaConcatenada) > Lista::Longitud(listaDePosDeMáxRepe) then // θ(1)
        			e.másRepes ← Lista::Vacía()   // θ(1)
        			e.itMásRepes ← Dicc:: Vacío() // θ(1)
        		fi 

        		if if Lista::Longitud(listaConcatenada) ≥ Lista::Longitud(listaDePosDeMáxRepe) // θ(1)
        			it ← Lista::AgregarAtrás(e.másRepes, q) // θ(1)
        			Dicc::Definir(e.itMásRepes, q, it)      // O(|x|) = O(1)
        		fi
			fi

			// La complejidad del algoritmo es θ(k)


		iposiciones(in d: documento, in p: palabra) → res: Lista(nat)
			res ← Significado(e.apariciones, p) // O(|x|) = O(1)


		isubtexto(in d: documento, in ini: nat, in fin: nat) → res:documento
			//No cumple la complejidad requerida porque agregar atrás de todo en un 
			//vector v es O(longitud(v))

			//Entonces copiar un vector elemento x elemento es:

			res ← enBlanco()

			while (ini ≤ fin) do
				agPalabra(res, enPos(d, ini)) // θ(|d|)
				ini ← ini + 1
			endwhile


Fin Interfaz
