// Ejercicio 5
// 
// Una matriz finita posee las siguientes operaciones:
// 
// > Crear, con la cantidad de filas y columnas que albergará la matriz.
// > Definir, que permite definir el valor para una posición válida.
// > #Filas, que retorna la cantidad de filas de la matriz.
// > #Columnas, que retorna la cantidad de columnas de la matriz.
// > Obtener, que devuelve el valor de una posición válida de la matriz (si nunca se definió la matriz en la
//   posición solicitada devuelve cero).
// > SumarMatrices, que permite sumar dos matrices de iguales dimensiones.
//  
// Diseñe un módulo para el TAD Matriz finita de modo tal que dadas dos matrices finitas A y B,
// ∗ Definir y Obtener aplicadas a A se realicen cada una en Θ(n) en peor caso, y
// ∗ SumarMatrices aplicada a A y B se realice en Θ(n + m) en peor caso,
// donde n y m son la cantidad de elementos no nulos de A y B, respectivamente.



// Ideas para la representación:

a. una lista enlazada desordenada de tuplas <elemento, alto, ancho>

// obtener es O(n)
// definir es O(n) (porque tengo que chequear si ya está definida)
// sumar es O(n*m)


b. es un arreglo de arreglos de naturales

// obtener es θ(1)
// definir es θ(1)
// sumar es θ(ancho*alto)

c. es un diccTrie(posición, elemento)

// Obtener es θ(|posición mas larga|)
// Definir es θ(|posición mas larga|)

d. es un diccABB(elemento, posición)

e. es una lista enlazada ordenada (según la posición) de tupla(posición, elemento)

// Obtener es O(log(n)) (uso busqueda binaria)
// Definir es O(log(n)) (uso busqueda binaria)
// Sumar es O(n + m)??? (hago un merge)

representación:
	matriz se representa con estr, donde
		estr es tupla⟨ filas: nat,
                       columnas: nat, 
                       listaElementos: lista_enlazada( tupla⟨pos, nat⟩ )
		             ⟩

Interfaz

    se explica con: Matriz finita

    géneros: matriz

    operaciones:

      	Crear(in alto : nat, in ancho : nat) → res: matriz
        Pre ≡ { ancho ≠ 0 ∧ alto ≠ 0 }
        Post ≡ { res =obs crear(alto, ancho) }
        Descripción: Crea una matriz con la cantidad de filas y columnas que indica la entrada.
        Complejidad: θ(...)
        Aliasing: No presenta aspectos de aliasing.

        Definir(in/out m : matriz, in alto : nat, in ancho : nat, in e : nat)
        Pre ≡ { m =obs m₀ ∧ alto < m.alto ∧ ancho < m.ancho}
        Post ≡ { m =obs definir(m₀, alto, ancho) }
        Descripción: Permite definir el valor para una posición válida.
        Complejidad: O(n) //n es cantidad de elementos no nulos de la matriz
        Aliasing: La matriz es pasada como un valor de referencia modificable

        #Filas(in m : matriz) → res : nat
        Pre ≡ { true } 
        Post ≡ { res =obs filas(m) }
        Descripción: Retorna la cantidad de filas de la matriz.
        Complejidad: θ(1)
        Aliasing: La matriz es pasada como un valor de referencia no modificable.

        #Columnas(in m : matriz) → res : nat
        Pre ≡ { true } 
        Post ≡ { res =obs columnas(m) }
        Descripción: Retorna la cantidad de columnas de la matriz.
        Complejidad: θ(1)
        Aliasing: La matriz es pasada como un valor de referencia no modificable.

        Obtener(in m : matriz, in alto : nat, in ancho : nat ) → res : nat
        Pre ≡ { true }
        Post ≡ { res =obs obtener(m, alto, ancho) }
        Descripción: que devuelve el valor de una posición válida de la matriz (si nunca se definió la matriz en la
					 posición solicitada devuelve cero).
        Complejidad: O(n) //n es cantidad de elementos no nulos de la matriz
        Aliasing: La matriz es pasada como un valor de referencia no modificable.

        sumarMatrices(in A : matriz, in B : matriz) → res : matriz
        Pre ≡ { A.ancho =obs B.ancho  ∧  A.alto =obs B.alto }
        Post ≡ { res =obs sumarMatrices(A, B) }
        Descripción: Permite sumar dos matrices de iguales dimensiones.
        Complejidad: O(n + m) //n y m son la cantidad de elementos no nulos de A y B, respectivamente.
        Aliasing: Las matrices son pasadas como valores de referencia no modificables.

    algoritmos:


    	iesMenorEstricto(in p : pos, in q : pos) → res: bool
	    	if p.first == q.first then
	    		if p.second == q.second then
	    			res ← false
	    		else
	    			res ← p.second < q.second
	    		fi
	    	else
	    		res ← p.first < q.first
	    	fi

	    	***


        iDefinir(in/out m : matriz, in alto : nat, in ancho : nat, in e : nat)

            pos ← ⟨ancho, alto⟩ // θ(1)
            terminé? ← false    // θ(1)
        
            it ← Lista::CrearIt(m.listaElementos) // θ(1)

            while (Lista::HaySiguiente(it) && ¬terminé?) do // θ(n) en el peor caso, siendo n la cantidad de elementos de la lista

                if Lista::Siguiente(t).pos = pos  then     // θ(1)
                    terminé? ← true                        // θ(1)
                    Lista::Siguiente(t).valor ← e          // θ(1)
                else if (esMayorEstricto(pos, Lista::Siguiente(t).pos)c || ¬HaySiguiente(Avanzar(it))) // θ(1)
                    terminé? ← true                            // θ(1)
                    Lista::AgregarComoSiguiente(it, ⟨pos, e⟩)  // θ(1)
                fi

                Avanzar(it)  // θ(1)
            
            endwhile

        // Complejidad del algoritmo en el peor caso: θ(n)


        ***

        iObtener(in m : matriz, in alto : nat, in ancho : nat ) → res : nat

            pos ← ⟨ancho, alto⟩ // θ(1)
            it ← Lista::CrearIt(m.listaElementos) // θ(1)

            res ← 0

            while (Lista::HaySiguiente(it) && res == 0) do // θ(n) en el peor caso, siendo n la cantidad de elementos de la lista
                if Siguiente(it).pos == pos then           // θ(1)
                    res ← Siguiente(it).valor              // θ(1)
                fi
                Avanzar(it)
            endwhile



        ***

        iSumarMatrices(in A : matriz, in B : matriz) → res : matriz

        res.alto ← A.alto
        res.ancho ← A.ancho
        res.listaElementos ← Lista::Vacía()

        itA ← Lista::CrearIt(A.listaElementos)
        itB ← Lista::CrearIt(B.listaElementos)

        
        // En el peor caso, tengo que realizar una operación por cada elemento de cada lista
        // es decir, O(n + m)


        while HaySiguiente(itA) && HaySiguiente(itB) do

            if Siguiente(itA).pos = Siguiente(itB).pos then
                
                tuplaSumada.pos   ← Lista::Siguiente(itA).pos                                 // θ(1)
                tuplaSumada.valor ← Lista::Siguiente(itA).valor + Lista::Siguiente(itA).valor // θ(1)
                Lista::Avanzar(itA) // θ(1)
                Lista::Avanzar(itB) // θ(1)

            else if esMayorEstricta(Siguiente(itA).pos, Lista::Siguiente(itB).pos) then // θ(1)
            
                tuplaSumada ← Lista::Siguiente(itB) // θ(1)
                Lista::Avanzar(itB)                 // θ(1)
            
            else 

                tuplaSumada ← Lista::Siguiente(itA) // θ(1)
                Lista::Avanzar(itA)                 // θ(1)

            fi

            Lista::AgregarAtrás(res.listaElementos, tuplaSumada⟩ // θ(1)

        endwhile

        // En el peor caso, en éste while recorro todo A
        while HaySiguiente(itA) do // O(n)
            Lista::AgregarAtrás(res.listaElementos, Lista::Siguiente(itA)⟩ // θ(1)
            Lista::Avanzar(itA)                                            // θ(1)
        endwhile

        // En el peor caso, en éste while recorro todo B
        while HaySiguiente(itB) do // O(m)
            Lista::AgregarAtrás(res.listaElementos, Lista::Siguiente(itB)⟩ // θ(1)
            Lista::Avanzar(itB)                                            // θ(1)
        endwhile


Fin Interfaz

    

