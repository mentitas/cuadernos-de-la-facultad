Florencia Rosenzuaig - LU: 118/21

Ejercicio 2. Divide and Conquer

// Los elfos de Papá Noel tienen una fiesta de fin de año llena de juegos. Uno de estos juegos consiste en lo
// siguiente. Se tiene una fila de cajas que contienen alfajores. Cada caja indica el número de alfajores que
// contiene. El jugador puede elegir una caja (cualquiera, a gusto) y se lleva todos los alfajores que ella
// contiene, pero hay un pequeño truco: cuando elige una caja, una máquina vacı́a las cajas inmediatamente a la
// izquierda y a la derecha de la elegida. Luego, el jugador sigue eligiendo cajas hasta que no queden alfajores
// para tomar, que es el momento donde el juego termina y el elfo se retira con su premio. Posteriormente, se
// vuelven a llenar aleatoriamente las cajas para que pueda participar otro jugador.

// Uno de los elfos que está haciendo fila para jugar quiere ganar a toda costa la mayor cantidad de alfajores que
// sea posible. Nos pidió que le facilitemos un algoritmo para la función MaxAlfajores, que dado un arreglo con las
// cantidades de alfajores en las cajas devuelva la mayor cantidad de alfajores que se pueden conseguir en el juego:
// MaxAlfajores(in cajas : arreglo(nat)) → res : nat

// > Ejemplos:
// 	MaxAlfajores([1,3,4,3]) = 6
// 	MaxAlfajores([15,2,1,10,5,8,11,6]) = 39

// Nos pide además que este algoritmo utilice la técnica de Divide&Conquer y que tenga complejidad temporal O(n),
// siendo n la cantidad de cajas.

// Escribir el pseudocódigo del algoritmo, justificar detalladamente que este resuelve el problema planteado y demos-
// trar formalmente que cumple con la complejidad solicitada. Para esto último, se deberá utilizar alguno de los
// métodos vistos de acuerdo a las caracterı́sticas del problema que se está resolviendo.

// Nota: pueden suponer que n, la cantidad de cajas, es una potencia de dos.
// Pista: quizás sea necesario calcular varias cosas auxiliares al mismo tiempo.



// En el pdf primero se encuentra el algoritmo, luego la demostración de la complejidad y luego el
// razonamiento detrás del algoritmo. 
// El razonamiento está al final porque voy a escribirlo luego de exportar el pdf usando otro programa,
// sin embargo creo que sería mejor si primero se lee el razonamiento y luego el algoritmo.



*** Algoritmo ***

int maxAlfajores(array<int> A){
	int n = A.size();                            θ(1)

	if (n==1){                                   θ(1)
		return A[0];                             θ(1)
	}
	if (n==2){                                   θ(1)
		return max(A[0], A[1]);                  θ(1)
	}

	return maxTripla(maxAlfajores_aux(A,0,n));   θ(n)
}

Yo voy a devolver una tupla ⟨A : int,  // Con principio y sin final
                             B : int,  // Con principio y con final
                             C : int⟩  // Sin principio y con final

pair<int,int,int> maxAlfajores_aux(array<int> A, int l, int r){ // Rango [l,r)
	
	int n = r-l θ(1)

	if (n==4){                                                                             θ(1)
		int A = A[l]   + A[l+2];                                                           θ(1)
		int B = A[l]   + A[l+3];                                                           θ(1)
		int C = A[l+1] + A[l+3];                                                           θ(1)

		return make_pair(A,B,C);                                                           θ(1)

	} else {

		int m = (l+r)/2;                                                                   θ(1)

		pair<int,int,int> recursionIzq = maxAlfajores_aux(A,l,m);                          T(n/2)
		pair<int,int,int> recursionDer = maxAlfajores_aux(A,m,r);                          T(n/2)

		int A = recursionIzq.A + recursionDer.A;                                           θ(1)
		int B = max(recursionIzq.A+recursionDer.B, recursionIzq.B + recursionDer.A);       θ(1)
		int C = recursionIzq.C + recursionDer.C;                                           θ(1)

 		return make_pair(A,B,C);                                                           θ(1)
	}
}



*** Funciones auxiliares ***

//θ(1)
int max(pair<int,int> p){
	if (p.first > p.second){
		return p.first;
	} else {
		return p.second;
	}
}

// θ(1)
int maxTripla(pair<int,int,int> t){
	if (t.first > t.second){
		return max(t.first, t.third);
	} else {
		return max(t.second, t.third);
	}
}



*** Demostración de la complejidad de maxAlfajores_aux ***

// Voy a intentar demostrar la complejidad usando el teorema maestro.

// T(n) = T(n/2) + θ(1)


// > Caso 1: Si ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε)), entonces T(n) = θ(n^log_c(a))

// f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_2(2)-ε))
// f(n) ∈ O(n^(1-ε))

// *tomo ε = 1/2 *

// f(n) ∈ O(n^1/2)
// f(n) ∈ O(√n)     → lo cual se cumple, ya que f(n) ∈ O(1) y O(1) ⊆ O(√n), por lo que f(n) ∈ O(√n).

// Entonces, dado que me encuentro en el caso 1 del teorema maestro, puedo afirmar que:

// T(n) = θ(n^log_c(a)) = θ(n^log_2(2)) = θ(n)



*** Razonamiento ***

Yo tengo un arreglo de enteros y quiero encontrar la máxima cantidad de alfajores que puedo conseguir
siguiendo las reglas del juego.

Inicialmente, para resolverlo pensé dividir el arreglo en 2, encontrar el máximo de cada lado y luego sumarlos,
pero me di cuenta que eso no era posible porque el máximo de la izquierda quiźas no es compatible con el máximo
de la derecha.

¿A que me refiero con ésto? Que quizás al buscar el máximo de la izquierda, estoy agarrando los alfajores de la
caja del medio, entonces puede que esté alterando el subarreglo de la derecha (o al revés).

Entonces, para resolver éste problema debía saber de antemano si la recursión del lado izquierdo afectaba al 
subarreglo derecho y viceversa. Una solución podría haber sido hacer la recursión derecha e izquierda, ver si
afectaban el otro subarreglo, y luego volver a hacer 2 recursiones teniendo en cuenta éste dato, sin embargo
la complejidad de todo el algoritmo hubiese sido T(n) = 4*T(n/2) + θ(1), lo cual es mayor que θ(n).

Entonces me puse a pensar el caso base, y me di cuenta que un caso base con n=2 no me aportaba mucha info al
momento de hacer la recursión.

Probando algunas cosas, me di cuenta que cuando n=4 sólo hay 3 combinaciones posibles para elegir la máxima
suma, y éstas son:

Dado el arreglo N = [a,b,c,d], entonces

A = a + c, N = [a,_,c,_] (es decir, un elfo agarró alfajores de las cajas a y c, y la máquina vacío b y d)
B = a + d, N = [a,_,_,d] (es decir, un elfo agarró alfajores de las cajas a y d, y la máquina vació b y c)
C = b + d, N = [_,b,_,d] (es decir, un elfo agarró alfajores de las cajas a y c, y la máquina vacío a y c)

y me di cuenta de ciertas características:
> La combinación A conserva el principio y pierde el final. Voy a decir que tiene principio y no tiene final.
> La combinación B conserva el principio y conserva el final, voy a decir que tiene principio y final.
> La combinación C pierde el principio y conserva el final, voy a decir que no tiene principio y tiene final.

Entonces, si yo quisiera concatenar combinaciones, yo no puedo agarrar alfajores de dos cajas consecutivas,
entonces se cumple una combinación con final solo es compatible con una combinación sin principio.

Por ejemplo:

Dado el arreglo [1,2,3,4,4,5,6,7]

divido en 2 subarrays de 4 elementos:

[1,2,3,4] [4,5,6,7]

y me doy cuenta de que si en mi combinación de la izquierda elijo el 4, en la derecha no puedo elegir el 5,
y si en la derecha elijo el 5, en la izquierda no puedo elegir el 4.

Entonces pienso en las combinaciones posibles de mis dos subarrays:

[1,2,3,4]             [4,5,6,7]
Ai = [1,_,3,_]        Ad = [4,_,6,_]
Bi = [1,_,_,4]        Bd = [4,_,_,7]
Ci = [_,2,_,4]        Cd = [_,5,_,7]

y teniendo en cuenta que una combinación con final solo es compatible con una combinación sin principio,
las concatenaciones compatibles son:

Ai × Ad
Ai × Bd
Bi × Cd
Ci × Cd


Entonces, la máxima cantidad de alfajores se obtiene de hacer dos combinaciones compatibles y sumar los elementos
restantes. Dado que son solo 4 combinaciones compatibles, las calculo y busco el máximo:

Ai × Ad = Ai + Ad = (1+3) + (4+6) = 14
Ai × Bd = Ai + Bd = (1+4) + (4+6) = 15
Bi × Cd = Bi + Cd = (1+4) + (5+7) = 17
Ci × Cd = Ci + Cd = (2+4) + (5+7) = 18


El máximo es 18, entonces llego a la conclusión de que la máxima cantidad de alfajores que se puede obtener
del arreglo N = [1,2,3,4,5,6,7] es 18, agarrando los alfajores de las cajas marcadas con paréntesis:

Ci + Cd = (2+4) + (5+7) = 18

Ci = [_,2,_,4]
Cd = [_,5,_,7]

N = [1,(2),3,(4),4,(5),6,(7)]


Buenísimo, ahora que resolví el problema para n=8, quiero ver cómo lo resuelvo para n>8.

Siguiendo la idea de las compatibilidades, me doy cuenta de que el resultado del ejemplo anterior es de tipo C,
es decir, no tiene principio y tiene final, entonces si quisiera concatenarlo con otro array, debería ser con uno
sin principio.

Entonces me pongo a pensar cómo, a partir de dos combinaciones ya conocidas, averiguo de que tipo es su
concatenación. 

Dado que sólo hay 4 combinaciones posibles, las evalúo:

Ai × Ad = Ai tiene principio y Ad no tiene final, entonces Ai × Ad tiene principio y no tiene final: es de tipo A 
Ai × Bd = Ai tiene principio y Bd tiene final, entonces Ai × Bd tiene principio y tiene final, es de tipo B
Bi × Cd = Bi tiene principio y Cd tiene final, entonces Bi × Cd tiene principio y tiene final, es de tipo B
Ci × Cd = Ci no tiene principio y Cd tiene final, entonces Ci × Cd no tiene principio y tiene final, es de tipo C


Me doy cuenta que:

Ai × Ad = A
Ai × Bd = B
Bi × Cd = B
Ci × Cd = C

Entonces puedo seguir usando un razonamiento similar al de n=8 para situaciones de n>8.

Quizás la explicación no haya quedado muy clara, entonces voy a "debugear" un ejemplo:







(sigue en la siguiente página)