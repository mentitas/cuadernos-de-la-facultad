// Ejercicio 11 (Stock)
// Especifique un tipo abstracto de datos Stock que permita las siguientes funcionalidades:
// · Registrar un nuevo producto con su stock mı́nimo (un natural).
// · Registrar las compras y las ventas de un producto, indicando la cantidad de elementos comprados y
// vendidos, respectivamente.
// · Permitir que sea informado un producto sustituto para otro dado.
// · Devolver el conjunto de todos los productos con stock debajo del mı́nimo que no tengan sustituto, o bien
// tales que el total del stock del producto más el de su sustituto esté por debajo del mı́nimo.

TAD producto es string

TAD Stock

    géneros: stock

    exporta: stock, observadores básicos, generadores

    usa: bool, nat, producto

    igualdad observacional:
        (∀k₁, k₂ : stock)(k₁ =obs k₂ ⇔
                            (∀ p : producto)(productoEnStock(k₁, p) ⟺ productoEnStock(k₂, p))
                         ∧L (∀ p : producto)(productoEnStock(k₁, p) ⇒L (compras(k₁, p) = compras(k₂, p)
                                  ∧ ventas(k₁, p)      = ventas(k₂, p)
                                  ∧ stockMinimo(k₁, p) = stockMinimo(k₂, p)
                                  ∧ (tieneSustituto(k₁, p) ⟺ tieneSustituto(k₂, p))))
                         ∧L (∀ p : producto)(productoEnStock(k₁, p) ∧L tieneSustituto(k₁, p)
                                             ⇒L sustituto(k₁, p) = sustituto(k₂, p)))

    generadores:
        nuevoStock         : → stock
        agregarProducto    : stock × producto × nat → stock
        comprarProducto    : stock k × producto p → stock                 {productoEnStock(k, p)}
        venderProducto     : stock k × producto p → stock                 {productoEnStock(k, p)}
        registrarSustituto : stock k × producto p × producto q → stock    {productoEnStock(k, p)
                                                                        ∧ productoEnStock(k, q)}
    observadores básicos:
        compras            : stock k × producto p → nat                      {productoEnStock(k, p)}
        ventas             : stock k × producto p → nat                      {productoEnStock(k, p)}
        stockMinimo        : stock k × producto p → nat                      {productoEnStock(k, p)}
        sustituto          : stock k × producto p → producto                 {productoEnStock(k, p) 
                                                                            ∧ tieneSustituto(k, p)}
        productoEnStock    : stock × producto → bool
        tieneSustituto     : stock × producto → bool                         {productoEnStock(k, p)}       


    otras operaciones:
        stockActual : stock × producto → nat

    axiomas:
        compras(nuevoStock, p)                  ≡ 0
        compras(agregarProducto(k, p, n), q)    ≡ if p=q then 0 else compras(k, q) fi
        compras(comprarProducto(p, k), q)       ≡ compras(k, q) + β(p=q)
        compras(venderProducto(p, k), q)        ≡ compras(k, q)
        compras(registrarSustituto(k, p, s), q) ≡ compras(k, s)

        ventas(nuevoStock, p)                  ≡ 0
        ventas(agregarProducto(k, p, n), q)    ≡ if p=q then 0 else ventas(k, q) fi
        ventas(comprarProducto(p, k), q)       ≡ ventas(k, q)
        ventas(venderProducto(p, k), q)        ≡ ventas(k, q) + β(p=q) 
        ventas(registrarSustituto(k, p, s), q) ≡ ventas(k, q)

        stockMinimo(agregarProducto(k, p,  n), q)   ≡ if p=q then n else stockMinimo(k, q) fi
        stockMinimo(comprarProducto(k, p), q)       ≡ stockMinimo(k, q)
        stockMinimo( venderProducto(k, p), q)       ≡ stockMinimo(k, q)
        stockMinimo(registrarSustituto(k, p, s), q) ≡ stockMinimo(k, q)

        sustituto(agregarProducto(k, p, n), q)    ≡ sustituto(k, q)
        sustituto(comprarProducto(k, p), q)       ≡ sustituto(k, q)
        sustituto(venderProducto(k, p), q)        ≡ sustituto(k, q)
        sustituto(registrarSustituto(k, p, s), q) ≡ if p=q then s else sustituto(k, q)

        productoEnStock(nuevoStock, p)               ≡ false
        productoEnStock(agregarProducto(k, p, n), q) ≡ if p=q then true else productoEnStock(k, q) fi

        tieneSustituto(agregarProducto(k, p, n), q)    ≡ tieneSustituto(k, q)
        tieneSustituto(comprarProducto(k, p), q)       ≡ tieneSustituto(k, q)
        tieneSustituto(venderProducto(k, p),  q)       ≡ tieneSustituto(k, q)
        tieneSustituto(registrarSustituto(k, p, s), q) ≡ if p=q then true else tieneSustituto(k, q) fi

        stockActual(k, p) ≡ stockMinimo(k, p) + compras(k, p) - ventas(k, p)

Fin TAD