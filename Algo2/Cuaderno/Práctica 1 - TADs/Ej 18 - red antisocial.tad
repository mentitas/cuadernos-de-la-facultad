//CA: apenas enjuiciás, se resuelve automáticamente

TAD persona es string

TAD Secta

    géneros: secta

    exporta: secta, observadores básicos, generadores

    usa: bool, nat, conjunto, secuencia, persona

    igualdad observacional:
    	(∀s₁, s₂ : algo)(s₁ =obs s₂ ⇔ (líder(s₁) =obs líder(s₂)
    		                           ∧ seguidores(s₁) =obs seguidores(s₂)
    		                           ∧ expulsados(s₁) =obs expulsados(s₂)))

    generadores:
        nuevaSecta      : persona → secta
        invitarSeguidor : secta s × persona p × persona q → secta      
                                                        {esMiembro?(s, p) ∧ ¬esMiembro?(s, q) ∧ ¬(q ∈ expulsados(s))}
        enjuiciar       : secta s × persona p × conj(persona) t → secta 
                                                        {esMiembro?(s, p) ∧ sonMiembros?(s, t) ∧ #(t)=3}
        absolver        : secta s × persona p → secta   {p ∈ expulsados(s)}

    observadores básicos:
        líder      : secta → persona
        seguidores : secta → sec(secta)
        expulsados : secta → conj(persona)

    otras operaciones:
    	esMiembro?   : secta × personas → bool
    	sonMiembros? : secta × conj(persona) → bool
    	#seguidores  : secta s × persona p → nat                 {esMiembro?(s, p)}
    	expulsar     : secu(secta) ss × persona p → secu(secta)  {esMiembro?_secu(ss, p)}       

    	subsecta     : secta s × persona p → secta               {esMiembro?(s, p)}

    axiomas: ∀ s : secta, ∀ ss : secu(secta), ∀ p,q : persona, ∀ t : conj(persona)
        líder(nuevaSecta(p))          ≡ p
        líder(invitarSeguidor(s,p,q)) ≡ líder(s)
        líder(enjuiciar(s,p,t))       ≡ líder(s)
        líder(absolver(s,p))          ≡ líder(s)

        seguidores(nuevaSecta(p))          ≡ ⟨⟩

        seguidores(invitarSeguidor(s,p,q)) ≡ if p = líder(s)
                                                then seguidores(s) • nuevaSecta(q)
                                                else seguidores_secu(seguidores(s), p, q)
                                             fi


        
        seguidores_secu(ss, p, q) ≡ if vacío?(ss)
                                      then ⟨⟩
                                      else 
                                          if esMiembro?(prim(ss), p)
                                             then invitarSeguidor(prim(ss), p, q) • fin(ss)
                                             else prim(ss) • seguidores_secu(fin(ss), p, q)
                                          fi
                                    fi



        seguidores(enjuiciar(s,p,t)) ≡ if ganaJuicio?(s,p,t)
        	                              then seguidores(s)
        	                              else expulsar(seguidores(s),p)
        	                           fi
        	                           
        subsecta(s, p)             ≡ if p = líder(s) then s else subsecta_secuencias(seguidores(s), p) fi

        subsecta_secuencias(ss, p) ≡ if p = líder(prim(ss))
                                        then  prim(ss)
                                        else
                                            if esMiembro?(p, prim(ss))
                                            then subsecta(prim(ss), p)
                                            else subsecta_secuencias(fin(ss), p)
                                        fi
                                    fi

        expulsar(ss, p) ≡ if p = líder(prim(ss))
                            then fin(ss)
                            else
                                if esMiembro?(prim(ss))
                                   then (expulsar(seguidores(prim(ss)),p)) • fin(ss)
                                   else prim(ss) • expulsar(fin(ss), p)
                                fi
                          fi

Fin TAD