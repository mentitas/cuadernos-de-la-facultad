TAD persona es string
TAD fila es cola(persona)

TAD Fila

    géneros: fila

    exporta: fila, observadores básicos, generadores

    usa: bool, nat, persona, cola

    igualdad observacional:
        (∀f₁, f₂ : fila)(f₁ =obs f₂ ⇔ (Vacía(f₁) ⟺ Vacía(f₂))
                                ∧L ¬Vacía(f₁) ⇒L (∀ p : persona)((Esperando(p, f₁) ⟺ Esperando(p, f₂))
                                ∧ (Entró?(p, f₁) ⟺ Entró?(p, f₂))
                                ∧ (SeRetiró?(p, f₁) ⟺ SeRetiró?(p, f₂)) 
                                ∧ (Esperando(p, f₁) ⇒L (Posición(p, f₁) = Posición(p, f₂) ∧ (SeColó?(p, f₁) ⟺ SeColó?(p, f₂))))))

    generadores:
        AbrirVentanilla   : → fila
        Llegar            : persona p × fila f → fila                   {¬Esperando(p, f)}
        ColarseAdelanteDe : persona p × persona q × fila f → fila       {¬Esperando(p, f) ∧ Esperando(q, f)}
        Retirarse         : persona p × fila f → fila                   {Esperando(p, f)}

    observadores básicos:
        Vacía        : fila → bool
        Esperando    : persona × fila → bool
        Posición     : persona p × fila f → nat                         {Esperando(p, f)}
        SeColó?      : persona p × fila f → bool                        {Esperando(p, f)}
        Entro?       : persona × fila → bool
        SeRetiró?    : persona × fila → bool

    otras operaciones:
        Atender   : fila f → fila                                       {¬Vacía(f)}
        Longitud  : fila → nat
        FueAtendido? : persona × fila → bool

    axiomas:
        Vacía(AbrirVentanilla) ≡ true
        Vacía(Llegar(p, f))    ≡ false
        Vacía(Retirarse(p, f)) ≡ Vacía(Atender(f))

        Esperando(p, AbrirVentanilla)            ≡ false
        Esperando(p, Llegar(q, f))               ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true else  Esperando(p, f) fi
        Esperando(p, Retirarse(q, f))            ≡ if p=q then false else Esperando(p, f) fi

        Posición(p, Llegar(q, f))               ≡ if p=q then Longitud(f) else Posición(p, f) fi
        Posición(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then Posición(r) else (Posición(p, f) + β(Posición(p, f)>Posición(r, f))) fi //if Posición(p, f)<Posición(r, f) then Posición(p, f) else Posición(p, f) + 1 fi
        Posición(p, Retirarse(q, f))            ≡ Posición(p) + β(Posición(q)<Posición(p)) //p≠q

        SeColó?(p, Llegar(q, f))               ≡ if p=q then false else SeColó?(q, f) fi
        SeColó?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true  else SeColó?(p, f) fi
        SeColó?(p, Retirarse(q, f))            ≡ SeColó?(p, f)

        Entró?(p, AbrirVentanilla)            ≡ false
        Entró?(p, Llegar(q, f))               ≡ if p=q then true else Entró?(p, f) fi
        Entró?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then true else Entró?(p, f) fi 
        Entró?(p, Retirarse(q, f))            ≡ Entró(p, f)

        SeRetiró?(p, AbrirVentanilla)            ≡ false
        SeRetiró?(p, Llegar(q, f))               ≡ if p=q then false else SeRetiró?(p, f) fi
        SeRetiró?(p, ColarseAdelanteDe(q, r, f)) ≡ if p=q then false else SeRetiró?(p, f) fi
        SeRetiró?(p, Retirarse(q, f))            ≡ if p=q then true  else SeRetiró?(p, f) fi

        FueAtendido?(p, f) ≡ Entró?(p, f) ∧ ¬Esperando(p, f) ∧ ¬SeRetiró?(p, f)

        Longitud(f) ≡ if Vacía(f) then 0 else 1 + Longitud(Atender(f)) fi

        Atender(f) ≡ desencolar(f)

Fin TAD

