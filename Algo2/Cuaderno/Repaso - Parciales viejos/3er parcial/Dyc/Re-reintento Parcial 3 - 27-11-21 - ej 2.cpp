// Ej. 2. Divide and Conquer
// Como parte de un proyecto de generación automática de contenido audiovisual para Internet, se desea que, a partir
// de una lista dada, combinar aquellos videos consecutivos que traten sobre un mismo tema, para obtener pelı́culas
// de la mayor duración posible.

// Al momento se cuenta con dos operaciones ya realizadas, las cuales que no se pueden modificar:

// > MismoTema(in v : video, in v' : video) → res : bool, que nos dice si dos videos v y v' tratan sobre el mismo
// tema. Complejidad temporal: θ(1).
// > Combinar(in v : video, in v' : video) → res : video, que combina dos videos v y v'. Complejidad temporal:
// Θ(d(v) + d(v')), siendo d(x) la duración del video x.

// Dar un algoritmo que utilice la técnica Divide and Conquer para calcular la función

// Peliculas(in V : arreglo(video)) → res : arreglo(video)

// Por ejemplo, siendo los videos v1 , v3 y v4 del mismo tema, Peliculas([v1,v2,v3,v4]) → [v1,v2,v3−4]

// Se pide que el algoritmo tenga complejidad temporal O(n + (D log n)), donde D = ∑^n_i=1 d(V[i]) (es decir, es
// la suma de las duraciones de todos los videos). Justificar detalladamente que el algoritmo efectivamente resuelve
// el problema y demostrar formalmente que cumple con la complejidad solicitada. Para esto último, se deberá utilizar
// alguno de los métodos vistos de acuerdo a las caracterı́sticas del problema que se está resolviendo.


// Voy a resolverlo como en la resolución que subieron al campus









array<video> Peliculas(array<video> V){

	list<video> listaRes;                     θ(1)
	list<video> listaParaCombinar;            θ(1)

	listaParaCombinar.push_back(V[0]);        θ(1)

	for(video e : V){                                                O(n + log(n)*D)                              
		if (mismoTema(listaParaCombinar[0], e)){                     θ(1) 
			listaParaCombinar.push_back(e);                          θ(1)            
		} else {
			listaRes.push_back(combinarLista(listaParaCombinar));    O(log(n)*D)
			listaParaCombinar = {};                                  θ(1)
			listaParaCombinar.push_back(e);                          θ(1)                 
		}
	}

	listaRes.push_back(combinarLista(listaParaCombinar));	

	array<video> arrayRes("", listaRes.size());

	for(int i=0; i<listaRes.size(); i++){
		arrayRes[i] = listaRes[0];
		listaRes.pop_first();
	}

	return arrayRes;

}

video combinarLista(list<video> L){
	return combinarArreglo(lista_a_arreglo(L), 0, L.size());
}

video combinarArreglo(array<video> A, int l, int r){ // Rango: [l,r)
	
	int n = r-l;                                       θ(1)    

	if (n==1){                                         θ(1)
		
		return A[l];                                   θ(1)
	
	} else {

		int m = (l+r)/2;                               θ(1)

		video recursiónIzq = combinarArreglo(A,l,m);   T(n/2)
		video recursiónDer = combinarArreglo(A,m,r);   T(n/2)

		return combinar(recursiónDer, recursiónIzq);   O(d(recursiónDer) + d(recursiónIzq)) = O(D)

	}
}





***Demostración de complejidad de combinarArreglo***

T(n) = 2T(n/2) + D

Uso un árbol de recurrencia para demostrar la complejidad, ver el otro documento.
⟹ La complejidad es O(log(n)*D)










// Intento 2: uso listas de listas

array<video> Peliculas(array<video> V){

	list<video> videosConMismoTema;            θ(1)
	list<list<video>> listaDeListas;           θ(1)

	videosConMismoTema.push_back(V[0]);        θ(1)

	for(video e : V){                                                 O(n)                              
		if (mismoTema(videosConMismoTema[0], e)){                     θ(1) 
			videosConMismoTema.push_back(e);                          θ(1)            
		} else {
			listaDeListas.push_back(videosConMismoTema);              θ(1)
			videosConMismoTema = {e};                                 θ(1)                
		}
	}

	listaDeListas.push_back(videosConMismoTema);	

	array<video> arrayRes("", listaDeListas.size());

	int i=0;
	for(list<video> l : listaDeListas){         O(log(n)*D)
		arrayRes[i] = combinarLista(l);         O(log(|l|)*D)
		i++;
	}

	return arrayRes;

}

// ¿Puedo asumir que recorrer una lista con un for in range es O(n)?
