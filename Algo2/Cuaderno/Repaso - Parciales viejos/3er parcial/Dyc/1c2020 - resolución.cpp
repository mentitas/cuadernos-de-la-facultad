// Ejercicio 2: dividir y conquistar

// Se tiene un arreglo C de N conjuntos C[1],...,C[N]. El cardinal de cada conjunto es exactamente M , es decir
// #(C[i]) = M para todo 1 ≤ i ≤ N .

// 1. Proponer un algoritmo para determinar si los conjuntos C[1], . . . , C[N] son disjuntos dos a dos, es decir
// si para todo i ≠ j en el rango 1..N se tiene que C[i] ∩ C[j] = ∅. La complejidad temporal en peor caso del
// algoritmo debe ser O(M*N*log(N)). Justificar la complejidad temporal obtenida.

// 2. (optativo) Suponiendo que los elementos de todos los conjuntos están en el rango comprendido entre 1
// y el producto M*N , proponer un algoritmo para resolver el mismo problema con complejidad temporal
// en peor caso O(M*N). Notar que este algoritmo sirve para determinar si los conjuntos C[1],...,C[N]
// constituyen una partición del conjunto {1,2,..., M*N−1,M*N}.
// Justificar la complejidad temporal obtenida.

// Para manipular los conjuntos tener en cuenta la nota (★) de arriba.

// Aclaración: el ejercicio que define la aprobación es el 2.1, el que está marcado como optativo define si, de estar
// aprobado el anterior, se sube la nota para que quede una P en lugar de una A.
// De estar mal resuelto el 2.1, la nota final serı́a de una R o I independientemente del 2.2.




// Observación: Si armo una mega unión con los N conjuntos y todos los conjuntos son disjuntos dos a dos,
// el cardinal de la unión sería ser M*N.
// En cambio, si 2 conjuntos o más tienen algún elemento en común, el cardinal de la unión sería estrictamente
// menor a M*N.
// Pero si lo resuelvo de ésta manera no estoy usando dyc.




// Ya sea que los conjuntos son una estructura estática o dinámica, la complejidad de ésta operación sigue siendo
// O(|A|+|B|). 
// Si los conjuntos son una estructura dinámica como una lista, hacer la unión de dos conjuntos implica:
// > Crear un conjunto vacío: O(1)
// > Recorrer ambos conjuntos, e ir agregando en orden los elementos al conjunto resultante: O(1)

// Si los conjuntos son una estructura estática como un arreglo, hacer la unión de dos conjuntos implica:
// > Crear un conjunto vacío de tamaño |A|+|B|: O(|A|+|B|)
// > Recorrer ambos conjuntos, e ir agregando en orden los elementos al conjunto resultante: O(1)

// En ambos casos, la complejidad es O(|A|+|B|)

conjunto unión(conjunto A, conjunto B){
	conjunto res = {};

	itA = crearIt(A);
	itB = crearIt(B);

	while(haySiguiente(itA) && haySiguiente(itB)){         O(|A| + |B|)
		if (Siguiente(itA) < Siguiente(itB)){              θ(1)
			AgregarAtrás()
			Avanzar(itA);                                  θ(1)
		} else {                                           θ(1)
			Avanzar(itB);                                  θ(1)
		}
	}

}


// Pre: los conjuntos están ordenados de manera creciente
bool sonDisjuntos(conjunto A, conjunto B){
	bool res = true;	                                   θ(1)
	
	itA = crearIt(A);                                      θ(1)
	itB = crearIt(B);                                      θ(1)

	while(haySiguiente(itA) && haySiguiente(itB)){         O(|A| + |B|)
		if (Siguiente(itA) < Siguiente(itB)){              θ(1)
			Avanzar(itA);                                  θ(1)
		} else if (Siguiente(itA) > Siguiente(itB)){       θ(1)
			Avanzar(itB);                                  θ(1)
		} else {
			res = false;                                   θ(1)
		}
	}

	return res;                                            θ(1)
}



// Idea: lo intento pensar como el ejercicio de los puntitos


bool sonTodosDisjuntos(vector<conjunto> C, int l, int r){ // Rango [l,r)
	int N = r-l;

	if (N==1) return true;                                                       θ(1)
	//if (N==2) return sonDisjuntos(C[l], C[l+1]);

	int m = (l+r)/2;                                                             θ(1)

	bool recursionIzq = sonTodosDisjuntos(C, l, m);                              T(N/2)
	bool recursionDer = sonTodosDisjuntos(C, m, r);                              T(N/2)

	conjunto uniónIzq = unión(C,l,m);                                            O(N*M)
	conjunto uniónDer = unión(C,m,r);                                            O(N*M)  
  
	bool seCumpleEnAmbasMitades = sonDisjuntos(uniónIzq, uniónDer);              O(N*M)

	return recursionIzq && recursionDer && seCumpleEnAmbasMitades;
}



T(N) = 2*T(N/2) + O(N*M)


// Voy a resolver la complejidad con un árbol recursivo:

// T(N)    = 2*T(N/2) +   O(N*M)
//         = 4*T(N/4) + 2*O(N*M)
//         = 8*T(N/8) + 3*O(N*M)
//        ...
//         = N*T(N/N) + log₂(N)*O(N*M)
//         = N*T(1)   + log₂(N)*O(N*M)
//         = O(N + N*M*log₂(N))
//         = O(N*M*log₂(N))

