// Ej. 2. Dividir y conquistar
// Dado un árbol AVL binario con valores enteros positivos y negativos en sus nodos, se pide escribir un
// algoritmo D&C que devuelva la mayor suma posible que se pueda lograr sumando todos los nodos del árbol
// o de algún subárbol. El algoritmo debe tener complejidad lineal con respecto a la cantidad total de nodos.
// Justificar detalladamente la complejidad del algoritmo obtenido, y mostrar que efectivamente resuelve el
// problema.

int sumaMaxima(árbolBinario ab){
	
}





T(n) = 2T(n/2) + O(1)

// Caso 1: Si ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε))

// f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_2(2)-ε))
// f(n) ∈ O(n^(1-ε))

// *tomando ε = 1*

// f(n) ∈ O(n^0)
// f(n) ∈ O(1)

// Entonces como me encuentro en el caso 1, T(n) = θ(n)