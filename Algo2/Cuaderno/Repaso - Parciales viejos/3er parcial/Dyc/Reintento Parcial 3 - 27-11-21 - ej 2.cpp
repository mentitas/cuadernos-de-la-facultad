// Ej. 2. Divide and Conquer
// Como parte de un proyecto de generación automática de contenido audiovisual para Internet, se desea que, a partir
// de una lista dada, combinar aquellos videos consecutivos que traten sobre un mismo tema, para obtener pelı́culas
// de la mayor duración posible.

// Al momento se cuenta con dos operaciones ya realizadas, las cuales que no se pueden modificar:

// > MismoTema(in v : video, in v' : video) → res : bool, que nos dice si dos videos v y v' tratan sobre el mismo
// tema. Complejidad temporal: θ(1).
// > Combinar(in v : video, in v' : video) → res : video, que combina dos videos v y v'. Complejidad temporal:
// Θ(d(v) + d(v')), siendo d(x) la duración del video x.

// Dar un algoritmo que utilice la técnica Divide and Conquer para calcular la función

// Peliculas(in V : arreglo(video)) → res : arreglo(video)

// Por ejemplo, siendo los videos v1 , v3 y v4 del mismo tema, Peliculas([v1,v2,v3,v4]) → [v1,v2,v3−4]

// Se pide que el algoritmo tenga complejidad temporal O(n + (D log n)), donde D = ∑^n_i=1 d(V[i]) (es decir, es
// la suma de las duraciones de todos los videos). Justificar detalladamente que el algoritmo efectivamente resuelve
// el problema y demostrar formalmente que cumple con la complejidad solicitada. Para esto último, se deberá utilizar
// alguno de los métodos vistos de acuerdo a las caracterı́sticas del problema que se está resolviendo.



vector<video> Peliculas(vector<videos> V){

}


lista<video> Peliculas(lista<video> L, int l, int r){ // Rango [l, r)
	int n = L.size(); // θ(1)

	if(n==1){
		return L;     // θ(1)
	}

	int m = (l+r)/2;  // θ(1)

	lista<video> recursiónDer = Peliculas_aux(L, l, m); // T(n/2)
	lista<video> recursiónIzq = Peliculas_aux(L, m, r); // T(n/2)

	return concatenar_y_combinar(recursiónDer, recursiónIzq); // O(n + d(L[m-1]) + d(L[m]))

}


lista<video> concatenar_y_combinar(vector<video> A, vector<video> B){
	int a = A.size();
	int b = B.size();

	if(MismoTema(A[a-1]), B[0]){             // O(d(A[a-1]) + d(B[0]))
		video c = Combinar(A[a-1], B[0]);
		A[a-1] = c;
		B.pop_front();
	}

	return concatenar(A, B);
}



/* > Cálculo de complejidad: ???

T(n)   = 2T(n/2) + n   + d(V[n/2 - 1]) + d(V[n/2])
T(n)   = 2T(n/2) + n   + ∑ⁿ⁻¹ᵢ₌₀(d[V[i]])

T(n)   = 2T(n/2) + n   + D
T(n/2) = 4T(n/4) + n/2 + 2D
T(n/4) = 8T(n/8) + n/4 + 3D

...

T(n/2ᵏ) = 2ᵏ⁺¹T(n/2ᵏ) + n/2ᵏ + D

(siendo k = log₂(n))





*/