// Ejercicio 10

// Se tienen dos arreglos de n naturales A y B. A está ordenado de manera creciente y B está ordenado de
// manera decreciente. Ningún valor aparece mas de una vez en el mismo arreglo. Para cada posición i
// consideramos la diferencia absoluta entre los valores de ambos arreglos |A[i] − B[i]|. Se desea buscar el mı́nimo
// valor posible de dicha cuenta. Por ejemplo, si los arreglos son A = [1, 2, 3, 4] y B = [6, 4, 2, 1] los valores
// de las diferencias son 5, 2, 1, 3 y el resultado es 1.

// a) Implementar la función
// minDif(in A: arreglo(nat), in B: arreglo(nat)) → nat
// que resuelve el problema planteado. La función debe ser de tiempo O(log n), dónde n = tam(A) = tam(B).



// IDEAS:
// A lo bruto: O(n)
// 

// Pre: n = |A| = |B|
int minDif(vector<int> A, vector<int> B){
	return minDif_aux(A,B,0,A.size());
}

int minDif_aux(vector<int> A, vector<int> B, int l, int r){ // Rango [l,r)
	int n = A.size();

}

// dyc choto
int minDif_aux(vector<int> A, vector<int> B, int l, int r){ // Rango [l,r)
	int n = A.size();

	if(n==1){
		return abs(A[0]-B[0]); // θ(1)
	}

	int m = (r+l)/2 // θ(1)
	int minL = minDif_aux(A,B,l,m); // T(n/2)
	int minR = minDif_aux(A,B,m,r); // T(n/2)

	return min(minL, minR); // θ(1)	
}



// Voy a pensar que cada vector es como una recta

// Casos a: no se intersecan, el minDif se encuentra en las puntas

// ⟹ Si A[0] > B[0], entonces los vectores divergen (al principio es donde están más cerca)

// Ej: A=[5,6,7,8]
//     B=[4,3,2,1]
//     d=[1,3,5,7]
//        ∧

// Entonces minDif(A,B) = |A[0]-B[0]| = 1

// ⟹ Si B[n-1] > A[n-1], entonces los vectores divergen (al final es donde están más cerca)

// Ej: A=[1,2,3,4]
//     B=[8,7,6,5]
//     d=[7,5,3,1]
//              ∧

// Entonces minDif(A,B) = |A[n-1]-B[n-1]| = 1


// Casos b: se intersecan, el minDif se encuentra en el cualquier lado
// Ej: A=[1,2,3,4],  A=[1,2,3,4]
//     B=[9,7,2,1],  B=[9,7,5,3]
//     d=[8,5,1,3],  d=[8,5,2,1]
//            ∧               ∧

// Idea: Quiero encontrar el cruce, el lugar donde A[j]<B[j] && A[j+1]>B[j-1],
// lo puedo buscar de manera binaria


int minDif_aux(vector<int> A, vector<int> B, int l, int r){ // Rango [l,r]

	int n = r-l+1; // θ(1)

	// Casos triviales
	if (A[0] > B[0])     return abs(A[0]-B[0]);     // θ(1)
	if (A[n-1] < B[n-1]) return abs(A[n-1]-B[n-1]); // θ(1)

	// casos base
	if (n==1) return abs(A[l]-B[l]);                      // θ(1)
	if (n==2) return min(abs(A[l]-B[l]), abs(A[r]-B[r])); // θ(1)

	// Caso recursivo
	int m = (l+r)/2 // θ(1)

    if(A[m]<B[m]){ // descarto primer mitad
		return minDif_aux(A,B,m,r); // T(n/2)
	} else {       // descarto segunda mitad
		return minDif_aux(A,B,l,m); // T(n/2)
	}
}


// b) Calcular y justificar la complejidad del algoritmo propuesto.

// T(n) = T(n/2) + θ(1)
// Observación: es la misma complejidad que la búsqueda binaria

// RTA: La complejidad es O(log(n))