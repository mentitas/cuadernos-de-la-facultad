// Ejercicio 7 ★
// La cantidad de parejas en desorden de un arreglo A[1 ... n] es la cantidad de parejas de posiciones
// 1 ≤ i < j ≤ n tales que A[i] > A[j]. Dar un algoritmo que calcule la cantidad de parejas en desorden
// de un arreglo y cuya complejidad temporal sea estrictamente mejor que O(n²) en el peor caso.
// Hint: Considerar hacer una modificación de un algoritmo de sorting.



int ParejasEnDesorden(array<int> A, int l, int r){
	int n = r-l;                                               θ(1)

	if(n==1){                                                  θ(1)
		return 0;                                              θ(1)
	}

	int m = (l+r)/2;                                           θ(1)
	int recursiónIzq = ParejasEnDesorden(A,l,m);               T(n/2)
	int recursiónDer = ParejasEnDesorden(A,m,r);               T(n/2)

	array<int> izq = subArray(A,l,m);                          θ(n)
	array<int> der = subArray(A,m,r);                          θ(n)

	ordenar(izq);                                              O(n*log(n))
	ordenar(der);                                              O(n*log(n))

	int parejasEnMitad = mergeLoco(izq, der);                  θ(n)

	return recursiónIzq + recursiónDer + parejasEnMitad;       θ(1)
}


// O(n)
int mergeLoco(array<int> A, array<int> B){

	int a=0;
	int b=0;

	int res=0;

	while(a<A.size() && b<B.size()){
		if(B[b]<A[a]){
			res += a+1;
			b++;
		} else {
			a++;
		}
	}

	while(b<B.size()){
		if(B[b]<A[A.size()-1]){
			res += A.size();
		}
		b++;
	}

	return res;

}







*** Cálculo de la complejidad ***

T(n) = 2T(n/2) + θ(n*log(n))

Resuelvo con el teorema maestro:

Caso 3: ∃ ε>0 tal que f(n) ∈ Ω(n^(log_c(a) + ε)) y ∃ δ<1, ∃ n₀>0 tal que ∀ n≥n₀ se cumple a*f(n/c) ≤ δ*f(n)

f(n) ∈ Ω(n^(log_c(a) + ε))
f(n) ∈ Ω(n^(log_2(2) + ε))
f(n) ∈ Ω(n^(1+ε))

*¿Existe ε lo suficientemente chico tal que O(n^(1+ε)) ⊆ O(n*log(n))?*

Voy a demostrar la complejidad por sustitución. Ver documento con demostración de complejidades.

⟹ La complejidad es O(n*log²(n))





*¿O(n*log²(n)) ⊆ O(n²)?* 
⟹ ¡ésto se cumple! Ver documento con demostración de complejidades.










===================================================================================================
> Ésta solución cumple con la consigna, pero creo que puedo mejorar la complejidad


// Post: modificable = ordenar(A)
int ParejasEnDesorden(array<int> A){
	array<int> modificable = A;
	return ParejasEnDesorden_aux(modificable);
}

// Hay aliasing
int ParejasEnDesorden(array<int> &A){
	int n = A.size();                                          θ(1)

	if(n==1){                                                  θ(1)
		return 0;                                              θ(1)
	}

	int m = n/2;

	array<int> izq = subArray(A,0,m);                          θ(n)
	array<int> der = subArray(A,m,n);                          θ(n)
	
	int recursiónIzq = ParejasEnDesorden(izq);                 T(n/2)
	int recursiónDer = ParejasEnDesorden(der);                 T(n/2)
	// Ahora cada mitad está ordenada

	int parejasEnMitad = mergeLoco(A, izq, der);               θ(n)
	// Ahora todo el array está ordenado

	return recursiónIzq + recursiónDer + parejasEnMitad;       θ(1)
}


// Complejidad: θ(n)
// Pre:   T = T₀ 
//      ∧ A = subarray(T₀,0,n/2)
//      ∧ B = subarray(T₀,n/2,n)

// Post: T = ordenar(T₀)
//       ∧ res = parejasDesordenadasEnMitad(T₀)

int mergeLoco(array<int> &T, array<int> A, array<int> B){

	int a=0;
	int b=0;

	int res=0;

	while(a<A.size() && b<B.size()){
		if(B[b]<A[a]){
			T[a+b]=B[b];
			b++;
			res += A.size()-a+1;     // ← Sumo las parejas en desorden
		} else {
			T[a+b]=A[a];
			a++;
		}
	}

	while(a<A.size()){
		T[a+b]=A[a];
		a++;
	}

	while(b<B.size()){
		T[a+b]=B[b];
		b++;
	}

	return res;
}







*** Cálculo de la complejidad ***

T(n) = 2T(n/2) + θ(n)

Observación: es la misma complejidad que el MergeSort.

⟹ La complejidad es O(n*log(n))