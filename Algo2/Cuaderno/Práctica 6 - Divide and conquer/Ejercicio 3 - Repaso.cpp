// Ejercicio 3 ★
// Encuentre un algoritmo para calcular aᵇ en tiempo logarı́tmico en b. Piense cómo reutilizar los
// resultados ya calculados. Justifique la complejidad del algoritmo dado.



// Idea: a^b = a^b/2 * a^b/2

int potencia(int a, int b){
	if (b==0){                                     θ(1)
		return 1;                                  θ(1)
	}
	if (b==1){                                     θ(1)
		return a;                                  θ(1)
	}

	if (b%2==0){                                   θ(1)
		int mediaPotencia = potencia(a, b/2);      T(b/2)
	} else {
		int mediaPotencia = potencia(a, b/2)*a;    T(b/2)
	}

	return mediaPotencia*mediaPotencia;            θ(1)

}







*** Cálculo de la complejidad ***

T(b) = T(b/2) + θ(1)

Observación: tiene la misma complejidad que la búsqueda binaria.

⟹ La complejidad es θ(log(b)).