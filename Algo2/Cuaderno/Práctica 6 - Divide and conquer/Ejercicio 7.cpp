// Ejercicio 7 ★
// La cantidad de parejas en desorden de un arreglo A[1 ... n] es la cantidad de parejas de posiciones
// 1 ≤ i < j ≤ n tales que A[i] > A[j]. Dar un algoritmo que calcule la cantidad de parejas en desorden
// de un arreglo y cuya complejidad temporal sea estrictamente mejor que O(n²) en el peor caso.
// Hint: Considerar hacer una modificación de un algoritmo de sorting.


// Algoritmo O(n²)
int parejasEnDesorden(vector<int> v){
	
	int n = v.size();
	int suma = 0;

	for(int i=0; i<n; i++){       // O(n*(n-i))
		for(int j=i; j<n; j++){   // O(n-i)
			if(v[i]>v[j]) suma++; // O(1)
		}
	}

	return suma;
}

// Ésto es *técnicamente* menor estricto que O(n²)

// Complejidad:

// ∑ⁿᵢ₌₀ n-i = [∑ⁿᵢ₌₀ n]-[∑ⁿᵢ₌₀ i] = n² - n*(n-1)/2




// Voy a hacer un MergeSort raro:

// Si el arreglo tiene 1 elemento, devuelvo 0
// Sino, lo divido en dos cachos: A (izq) y B (der)
// Mergeo A y B: por cada elemento de B que tengo que poner antes que un elemento de A, tengo que
// sumarle todos los restantes elementos de A

// Por ejemplo, si tengo que B[0] < A[0], y A está ordenado, tengo que sumar A.size() (porque todos los
// elementos de A son mayores que B[0], por lo que forman una relación desordenada con B[0])


int parejasEnDesorden(vector<int> v){
	vector<int> aux = v;
	return parejasEnDesorden(aux, 0, aux.size());
}


// Hay aliasing
int parejasEnDesorden(vector<int> &v, int l, int r){ // Rango [l,r)

	int n = r-l;
	int res = 0;

	if(n==0){
		return 0;
	}

	int m = (r+l)/2;

	int recursion = parejasEnDesorden(v, l, m) + parejasEnDesorden(v, m, r); // 2T(n/2)
	int res = MergeRaro(v,l,m,r);                                            // θ(n)

	return res + recursion;

}


// θ(n)
int MergeRaro(vector<int> &v, int l, int m, int r){
	int n = r-l;           // O(1)
	vector<int> A(n/2, 0); // O(n/2)
	vector<int> B(n/2, 0); // O(n/2)

	for(int i=0; i<n/2; i++){ // O(n/2)
		A[i] = v[i];          // O(1)
	}

	for(int i=0; i<n/2; i++){ // O(n/2)
		B[i] = v[i+n/2];      // O(1)
	}

	vector<int> vector_aux(n, 0); // O(n)
	int suma = 0;                 // O(1)

	int a=0; // O(1)
	int b=0; // O(1)
	int i=0; // O(1), recorre aux

	while(a<n/2 && b<n/2){ // O(n)
		if(B[b] < A[a]){
			suma += n/2-a;
			vector_aux[i] = B[b];
			b++;
		} else {
			vector_aux[i] = A[a];
			a++;
		}
		i++;
	}

	while(a<n/2){ // O(n)
		vector_aux[i] = A[a]; // O(1)
		a++;                  // O(1)
		i++;                  // O(1)
	}

	while(b<n/2){ // O(n)
		vector_aux[i] = B[b]; // O(1)
		b++;                  // O(1)
		i++;                  // O(1)
	}

	for(int i=0; i<n; i++){ // O(n)
		v[i+l] = res[i];    // O(1)
	} 

	return suma;
}


// La complejidad total del algoritmo es la misma que la del MergeSort, es decir, O(n*log(n))