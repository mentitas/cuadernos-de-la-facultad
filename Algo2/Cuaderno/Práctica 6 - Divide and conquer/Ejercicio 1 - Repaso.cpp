// Ejercicio 1 ★
// 
// Escriba un algoritmo con dividir y conquistar que determine si un arreglo de tamaño potencia
// de 2 es más a la izquierda, donde “más a la izquierda” significa que:
// 
// > La suma de los elementos de la mitad izquierda superan los de la mitad derecha.
// > Cada una de las mitades es a su vez “más a la izquierda”.
// 
// Por ejemplo, el arreglo [8, 6, 7, 4, 5, 1, 3, 2] es “más a la izquierda”, pero 
// [8, 4, 7, 6, 5, 1, 3, 2] no lo es.
// Intente que su solución aproveche la técnica de modo que complejidad del algoritmo sea
// estrictamente menor a O(n²).



bool esMásALaIzq(array<int> V){
	return esMásALaIzq_aux(V,0,V.size());
}


bool esMásALaIzq_aux(array<int> V, int l, int r){ // Rango [l,r)
	int n = r-l;                                                 θ(1)                        
                                                                                 
	if (n==1){                                                   θ(1)                      
		return true;                                             θ(1)                            
	}                                                             
                                                                                                                                  
	int m = (l+r)/2;                                             θ(1)                            
                                                                   
	int sumaIzq = sum(V,l,m);                                    θ(n)                                     
	int sumaDer = sum(V,m,r);                                    θ(n)                                     
                                                                   
	bool recursiónIzq = esMásALaIzq_aux(V,l,m);                  T(n/2)                                                       
	bool recursiónDer = esMásALaIzq_aux(V,m,r);                  T(n/2)                                                       
                                                                              
	return sumaIzq > sumaDer && recursiónIzq && recursiónDer;    θ(1)                                                              

}


int sum(array<int> V, int l, int r){ // Rango [l,r)
	int res = 0;
	for(int i=l, i<r; i++){
		res+=V[i];
	}
	return res;
}






*** Cálculo de la complejidad ***

T(n) = 2T(n/2) + n

Observación: es la misma complejidad que el merge sort

⟹ La complejidad del algoritmo es O(n*log(n))