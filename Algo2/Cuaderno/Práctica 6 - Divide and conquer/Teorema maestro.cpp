// Teorema maestro




// Caso 1: Si ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε))

// f(n) ∈ O(n^(log_c(a)-ε))
// ...

// Entonces como me encuentro en el caso 1, T(n) = θ(n^log_c(a))




// Caso 2: f(n) ∈ θ(n^log_c(a))

// f(n) ∈ θ(n^log_c(a))
// ...

// Entonces como me encuentro en el caso 2, T(n) = θ(n^log_c(a) * log(n))




// Caso 3: ∃ ε>0 tal que f(n) ∈ Ω(n^(log_c(a) + ε)) y ∃ δ<1, ∃ n₀>0 tal que ∀ n≥n₀ se cumple a*f(n/c) ≤ δ*f(n)

// f(n) ∈ Ω(n^(log_c(a) + ε))
// ...

//      a*f(n/c) ≤ δ*f(n)
//      ...

// Entonces como me encuentro en el caso 3, T(n) = θ(f(n)) = θ(log(n))