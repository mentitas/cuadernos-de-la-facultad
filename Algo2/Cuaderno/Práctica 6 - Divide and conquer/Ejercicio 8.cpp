// Ejercicio 8 ★
// Se tiene una matriz booleana A de n × n y una operación conjunciónSubmatriz que toma O(1) y que dados
// 4 enteros i₀, i₁ , j₀ , j₁ devuelve la conjunción de todos los elementos en la submatriz que toma las
// filas i₀ hasta i₁ y las columnas j₀ hasta j₁ . Formalmente:

// conjunciónSubmatriz(i₀, i₁ , j₀ , j₁) =               ∧             A[i,j]
//                                           i₀ ≤ i ≤ i₁, j₀ ≤ j ≤ j₁ 


// 1. Dar un algoritmo que tome tiempo estrictamente menor que O(n²) que calcule la posición de algún
// false, asumiendo que hay al menos uno. Calcular y justificar la complejidad del algoritmo.

// Pre: la matriz es cuadrada y hay al menos un false

pair<int,int> buscoUnFalse(vector<vector<int>> m){
	return buscoUnFalse_aux(m, make_pair(0, m.size(), 0, m.size()));
}


// Cuadrantes:
 
//    j₀ j₁
// i₀  A  B
// i₁  C  D

// Aclaración: n = |m|
//             ñ = |m|², es la cantidad de elementos

pair<int,int> buscoUnFalse_aux(vector<vector<int>> m, pair<int,int,int,int> coord){
	
	int i₀ = coord[0]; // θ(1)
	int i₁ = coord[1]; // θ(1)
	int j₀ = coord[2]; // θ(1)
	int j₁ = coord[3]; // θ(1)

	// Caso base: estoy evaluando un único casillero
	if(i₀ == i₁){                 // θ(1)
		return make_pair(i₀, j₀); // θ(1)
	}

	int iₘ = (i₀+i₁)/2; // θ(1)
	int jₘ = (j₀+j₁)/2; // θ(1)

	pair<int,int,int,int> cuadranteA = (i₀, iₘ, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteB = (i₀, iₘ, jₘ, j₁); // θ(1)
	pair<int,int,int,int> cuadranteC = (iₘ, i₁, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteD = (iₘ, i₁, jₘ, j₁); // θ(1)

	if (conjuncionSubmatriz(m, cuadranteA)) {       // O(1)
		return buscoUnFalse_aux(m, cuadranteA);     // T(n/2)
	} else if (conjuncionSubmatriz(m, cuadranteB)){ // O(1)
		return buscoUnFalse_aux(m, cuadranteB);     // T(n/2)
	} else if (conjuncionSubmatriz(m, cuadranteC)){ // O(1)
		return buscoUnFalse_aux(m, cuadranteC);     // T(n/2)
	} else {
		return buscoUnFalse_aux(m, cuadranteD);     // T(n/2)
	}
}


// T(n) = T(n/2) + O(1)

// Uso el teorema maestro:

// Caso 2: f(n) ∈ O(n^log_c(a))
//                O(n^log_2(1))
//                O(n^0)
//                O(1)         ⟹ ¡y ésto se cumple!
//
// Entonces como me encuentro en el caso 2, T(n) = O(n^(log_c(a)) * log(n)) = O(log(n))



// 2. Modificar el algoritmo anterior para que cuente cuántos false hay en la matriz. Asumiendo que hay
// a lo sumo 5 elementos false en toda la matriz, calcular y justificar la complejidad del algoritmo.

int buscoUnFalse(vector<vector<int>> m){
	return buscoUnFalse_aux(m, make_pair(0, m.size(), 0, m.size()));
}


// Cuadrantes:
 
//    j₀ j₁
// i₀  A  B
// i₁  C  D

// Aclaración: n = |m|
// Pre: la matriz es cuadrada y a lo sumo hay 5 falses

int buscoUnFalse_aux(vector<vector<int>> m, pair<int,int,int,int> coord){
	
	int i₀ = coord[0]; // θ(1)
	int i₁ = coord[1]; // θ(1)
	int j₀ = coord[2]; // θ(1)
	int j₁ = coord[3]; // θ(1)

	// Caso base: estoy evaluando un único casillero
	if(i₀ == i₁){                 // θ(1)
		return 1;                 // θ(1)
	}

	int iₘ = (i₀+i₁)/2; // θ(1)
	int jₘ = (j₀+j₁)/2; // θ(1)

	pair<int,int,int,int> cuadranteA = (i₀, iₘ, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteB = (i₀, iₘ, jₘ, j₁); // θ(1)
	pair<int,int,int,int> cuadranteC = (iₘ, i₁, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteD = (iₘ, i₁, jₘ, j₁); // θ(1)

	int res = 0;

	if (conjuncionSubmatriz(m, cuadranteA)) {       // O(1)
		res += buscoUnFalse_aux(m, cuadranteA);     // T(n/2)
	} 

	if (conjuncionSubmatriz(m, cuadranteB)){        // O(1)
		res += buscoUnFalse_aux(m, cuadranteB);     // T(n/2)
	}

	if (conjuncionSubmatriz(m, cuadranteC)){        // O(1)
		res += buscoUnFalse_aux(m, cuadranteC);     // T(n/2)
	}

	if(conjuncionSubmatriz(m, cuadranteD)){         // O(1)
		res += buscoUnFalse_aux(m, cuadranteD);     // T(n/2)
	}

	return res;
}




// En el peor caso, T(n) = 4T(n/2) + θ(1)

// Uso el teorema maestro:
// Caso 1: ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a) - ε))

// f(n) ∈ O(n^(log_c(a) - ε))
// f(n) ∈ O(n^(log_2(4) - ε))
// f(n) ∈ O(n^(2 - ε))

// *tomo ε = 2 *

// f(n) ∈ O(n^0)
// f(n) ∈ O(1)   ⟹ ¡y ésto se cumple!

// Entonces como me encuentro en el caso 1, T(n) = θ(n^log_c(a)) = θ(n^log_2(4)) = θ(n²)



// 3. Si obtuvo una complejidad O(n²) en el punto anterior, mejore el algoritmo y/o el cálculo para obtener
// una complejidad menor.

// Cuadrantes:
 
//    j₀ j₁
// i₀  A  B
// i₁  C  D

int hayMasDeUnFalse(vector<vector<int>> m){
	return hayMasDeUnFalse_aux(m, make_pair(0, m.size(), 0, m.size()));
}

// Aclaración: n = |m|
int hayMasDeUnFalse_aux(vector<vector<int>> m, pair<int,int,int,int> coord){
	
	int i₀ = coord[0]; // θ(1)
	int i₁ = coord[1]; // θ(1)
	int j₀ = coord[2]; // θ(1)
	int j₁ = coord[3]; // θ(1)

	// Caso base: estoy evaluando un único casillero
	if(i₀ == i₁){                 // θ(1)
		return false;             // θ(1)
	}

	int iₘ = (i₀+i₁)/2; // θ(1)
	int jₘ = (j₀+j₁)/2; // θ(1)

	pair<int,int,int,int> cuadranteA = (i₀, iₘ, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteB = (i₀, iₘ, jₘ, j₁); // θ(1)
	pair<int,int,int,int> cuadranteC = (iₘ, i₁, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteD = (iₘ, i₁, jₘ, j₁); // θ(1)

	bool hayEnA = conjuncionSubmatriz(m, cuadranteA); // θ(1)
	bool hayEnB = conjuncionSubmatriz(m, cuadranteB); // θ(1)
	bool hayEnC = conjuncionSubmatriz(m, cuadranteC); // θ(1)
	bool hayEnD = conjuncionSubmatriz(m, cuadranteD); // θ(1)

	int cuadrantesDondeHay = hayEnA + hayEnB + hayEnC + hayEnD; // θ(1)

	if (cuadrantesDondeHay == 0){       // θ(1)
		return false;                   // θ(1)
	} else if (cuadrantesDondeHay > 1){ // θ(1)
		return true;                    // θ(1)
	} else {
		// hay en UN cuadrante
		if (hayEnA) return hayMasDeUnFalse_aux(m, cuadranteA); // T(n/4) 
		if (hayEnB) return hayMasDeUnFalse_aux(m, cuadranteB); // T(n/4)
		if (hayEnC) return hayMasDeUnFalse_aux(m, cuadranteC); // T(n/4)
		if (hayEnD) return hayMasDeUnFalse_aux(m, cuadranteD); // T(n/4)
	}
}

// T(n) = T(n/4) + θ(1)

// Uso el teorema maestro:

// Caso 2: f(n) ∈ θ(n^log_c(a))
//                θ(n^log_4(1))
//                θ(n^0)
//                θ(1)         ⟹ ¡y ésto se cumple!
//
// Entonces como me encuentro en el caso 2, T(n) = θ(n^(log_c(a)) * log(n)) = θ(log(n))




bool cuantosFalsesHay(vector<vector<int>> m){
	return cuantosFalsesHay_aux(m, make_pair(0, m.size(), 0, m.size()));
}

// Aclaración: n = |m|
// Pre: la matriz es cuadrada y a lo sumo hay 5 falses
int cuantosFalsesHay_aux(vector<vector<int>> m, pair<int,int,int,int> coord){
	
	int i₀ = coord[0]; // θ(1)
	int i₁ = coord[1]; // θ(1)
	int j₀ = coord[2]; // θ(1)
	int j₁ = coord[3]; // θ(1)

	// Caso base: estoy evaluando un único casillero
	if(i₀ == i₁){                   // θ(1)
		return 1;                   // θ(1)
	}

	int iₘ = (i₀+i₁)/2; // θ(1)
	int jₘ = (j₀+j₁)/2; // θ(1)

	pair<int,int,int,int> cuadranteA = (i₀, iₘ, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteB = (i₀, iₘ, jₘ, j₁); // θ(1)
	pair<int,int,int,int> cuadranteC = (iₘ, i₁, j₀, jₘ); // θ(1)
	pair<int,int,int,int> cuadranteD = (iₘ, i₁, jₘ, j₁); // θ(1)

	bool hayEnA = conjuncionSubmatriz(m, cuadranteA); // O(1)
	bool hayEnB = conjuncionSubmatriz(m, cuadranteB); // O(1)
	bool hayEnC = conjuncionSubmatriz(m, cuadranteC); // O(1)
	bool hayEnD = conjuncionSubmatriz(m, cuadranteD); // O(1)	

	int cuadrantesDondeHay = hayEnA + hayEnB + hayEnC + hayEnD; // θ(1)

	int res = 0; // θ(1)

	if (cuadrantesDondeHay == 0){       // θ(1)
		return 0;                       // θ(1)     
	} else if (cuadrantesDondeHay < 4){ // 3T(n/4)
		if (hayEnA) res += cuantosFalsesHay_aux(m, cuadranteA); // T(n/4)
		if (hayEnB) res += cuantosFalsesHay_aux(m, cuadranteB); // T(n/4)
		if (hayEnC) res += cuantosFalsesHay_aux(m, cuadranteC); // T(n/4)
		if (hayEnD) res += cuantosFalsesHay_aux(m, cuadranteD); // T(n/4)
	} else {
		// cuadrantesDondeHay == 4
		bool hayMasDeUnoEnA = hayMasDeUnFalse_aux(m, cuadranteA); // θ(log(n))
		bool hayMasDeUnoEnB = hayMasDeUnFalse_aux(m, cuadranteB); // θ(log(n))
		bool hayMasDeUnoEnC = hayMasDeUnFalse_aux(m, cuadranteC); // θ(log(n))
		bool hayMasDeUnoEnD = hayMasDeUnFalse_aux(m, cuadranteD); // θ(log(n))
		
		bool enAlgunoHayMasDeUno = hayMasDeUnoEnA || hayMasDeUnoEnB || hayMasDeUnoEnC || hayMasDeUnoEnD; // θ(1)

		if (enAlgunoHayMasDeUno){ // θ(1) 
			return 5; // θ(1)
		} else {
			return 4; // θ(1)
		}
	}
}

// T(n) = 3T(n/4) + θ(log(n))

// Uso el teorema maestro:

// log_4(3) ≃ 0,79

-------------------------------------------------
// Caso 1: ∃ ε>0 tal que f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_c(a)-ε))
// f(n) ∈ O(n^(log_4(3)-ε))

// *tomo ε = 0.001*

// f(n) ∈ O(n^(log_4(3)-0.001))

// Y se cumple que O(log(n)) ⊆ O(√n) ⊆ O(n^(log_4(3)-0.001)) (chequeado en geogebra)

// Entonces como me encuentro en el caso 1, T(n) = θ(n^log_c(a)) = θ(n^log_4(3))