// Ejercicio 5
// 
// Suponga que se tiene un método potencia que, dada un matriz cuadrada A de orden 4 × 4 y un número n,
// computa la matriz Aⁿ. Dada una matriz cuadrada A de orden 4 × 4 y un número natural n que es potencia
// de 2 (i.e., n = 2ᵏ para algun k ≥ 1), desarrollar, utilizando la técnica de dividir y conquistar y el
// método potencia, un algoritmo que permita calcular
// 	
// 	A¹ + A² + ... + Aⁿ .
// 
// Calcule el número de veces que el algoritmo propuesto aplica el método potencia. Si no es
// estrictamente menor que O(n), resuelva el ejercicio nuevamente.


// Metodo O(n)
vector<vector<int>> sumaDePotencias(vector<vector<int>> m, int n){
	vector<vector<int>> suma = A;
	for(int i=2; i<n; i++){
		suma+=potencia(A,i);
	}
	return suma;
}



// Idea:  

//     A¹ + A² + A³ + ... + A^n/2  +  ... + Aⁿ
// =  (A¹ + A² + A³ + ... + A^n/2) + A^n/2 * (A¹ + A² + A³ + ... + A^n/2)


vector<vector<int>> sumaDePotencias(vector<vector<int>> m, int n){
	
	if(n == 1){
		return m;                                                  θ(1)
	}
	
	vector<vector<int>> recursión = sumaDePotencias(m, n/2);       T(n/2)

	return recursión + potencia(m, n/2) * recursión;               → usé "potencia"! 
	
}


// Como n es potencia de 2, la operación va a hacer log₂(n) llamados recursivos, por lo que va a aplicar log₂(n)
// veces el método "potencia".

// RTA: es menor que O(n)