// Ejercicio 8 ★
// Se tiene una matriz booleana A de n × n y una operación conjunciónSubmatriz que toma O(1) y que dados
// 4 enteros i₀, i₁ , j₀ , j₁ devuelve la conjunción de todos los elementos en la submatriz que toma las
// filas i₀ hasta i₁ y las columnas j₀ hasta j₁ . Formalmente:

// conjunciónSubmatriz(i₀, i₁ , j₀ , j₁) =               ∧             A[i,j]
//                                           i₀ ≤ i ≤ i₁, j₀ ≤ j ≤ j₁ 


// 1. Dar un algoritmo que tome tiempo estrictamente menor que O(n²) que calcule la posición de algún
// false, asumiendo que hay al menos uno. Calcular y justificar la complejidad del algoritmo.

// Pre: la matriz es cuadrada y hay al menos un false

// Cuadrantes:
// 
//   j →
// i A B
// ↓ C D

pair<int,int> buscarFalse(array<array<bool>> m){
	return buscarFalse_aux(m, make_pair(0,m.size(),0,m.size()));
}


// Siendo n = |m|
pair<int,int> buscarFalse_aux(array<array<bool>> m, pair<int,int,int,int> cuadrante){ // Rango: [i₀, iₙ) × [j₀, jₙ)
	
	int i₀ = cuadrante.first;                                            θ(1) 
	int iₙ = cuadrante.second;                                           θ(1)
	int j₀ = cuadrante.third;                                            θ(1) 
	int jₙ = cuadrante.forth;                                            θ(1) 

	if (i₀ == i₁-1){
		return make_pair(i₀, j₀);                                        θ(1)
	}

	int iₘ = (i₀+iₙ)/2;                                                  θ(1)
	int jₘ = (j₀+jₙ)/2;                                                  θ(1)

	pair<int,int,int,int> cuadranteA = make_pair(i₀, iₘ, j₀, jₘ);        θ(1)
	pair<int,int,int,int> cuadranteB = make_pair(i₀, iₘ, jₘ, jₙ);        θ(1)
	pair<int,int,int,int> cuadranteC = make_pair(iₘ, iₙ, j₀, jₘ);        θ(1)
	pair<int,int,int,int> cuadranteD = make_pair(iₘ, iₙ, jₘ, jₙ);        θ(1)

	bool hayEnA = !conjunción(m, cuadranteA);                            O(1)
	bool hayEnB = !conjunción(m, cuadranteB);                            O(1)
	bool hayEnC = !conjunción(m, cuadranteC);                            O(1)
	bool hayEnD = !conjunción(m, cuadranteD);                            O(1)

	if (hayEnA){
		return buscarFalse_aux(m, cuadranteA);                           T(n/2)
	} else if (hayEnB){
		return buscarFalse_aux(m, cuadranteB);                           T(n/2)
	} else if (hayEnC){
		return buscarFalse_aux(m, cuadranteC);                           T(n/2)
	} else {
		return buscarFalse_aux(m, cuadranteD);                           T(n/2)
	}
}







*** Cálculo de la complejidad ***

T(n) = T(n/2) + θ(1)

Observación: es igual que la búsqueda binaria.
⟹ La complejidad es θ(log(n)).










// 2. Modificar el algoritmo anterior para que cuente cuántos false hay en la matriz. Asumiendo que hay
// a lo sumo 5 elementos false en toda la matriz, calcular y justificar la complejidad del algoritmo.

// Pre: la matriz es cuadrada y hay como mucho 5 falses

// Cuadrantes:
// 
//   j →
// i A B
// ↓ C D

int cuantosFalses(array<array<bool>> m){
	return cuantosFalses_aux(m, make_pair(0,m.size(),0,m.size()));
}


// Siendo n = |m|
int cuantosFalses_aux(array<array<bool>> m, pair<int,int,int,int> cuadrante){ // Rango: [i₀, iₙ) × [j₀, jₙ)
	
	int i₀ = cuadrante.first;                                            θ(1) 
	int iₙ = cuadrante.second;                                           θ(1)
	int j₀ = cuadrante.third;                                            θ(1) 
	int jₙ = cuadrante.forth;                                            θ(1) 

	if (i₀ == i₁-1){

	}

	int iₘ = (i₀+iₙ)/2;                                                  θ(1)
	int jₘ = (j₀+jₙ)/2;                                                  θ(1)

	pair<int,int,int,int> cuadranteA = make_pair(i₀, iₘ, j₀, jₘ);        θ(1)
	pair<int,int,int,int> cuadranteB = make_pair(i₀, iₘ, jₘ, jₙ);        θ(1)
	pair<int,int,int,int> cuadranteC = make_pair(iₘ, iₙ, j₀, jₘ);        θ(1)
	pair<int,int,int,int> cuadranteD = make_pair(iₘ, iₙ, jₘ, jₙ);        θ(1)

	bool hayEnA = !conjunción(m, cuadranteA);                            O(1)
	bool hayEnB = !conjunción(m, cuadranteB);                            O(1)
	bool hayEnC = !conjunción(m, cuadranteC);                            O(1)
	bool hayEnD = !conjunción(m, cuadranteD);                            O(1)

	int res = 0;

	if (hayEnA){
		res += cuantosFalses_aux(m, cuadranteA);                         T(n/2)
	} else if (hayEnB){
		res += cuantosFalses_aux(m, cuadranteB);                         T(n/2)
	} else if (hayEnC){
		res += cuantosFalses_aux(m, cuadranteC);                         T(n/2)
	} else if (hayEnD){
		res += cuantosFalses_aux(m, cuadranteD);                         T(n/2)
	}

	return res;
}

// 3. Si obtuvo una complejidad O(n²) en el punto anterior, mejore el algoritmo y/o el cálculo para obtener
// una complejidad menor.

