#include <iostream>
#include <utility>
#include <vector>

using namespace std;

pair<int, int> suma( pair<int, int> a, pair<int, int> b){
    return make_pair(a.first + b.first, a.second + b.second);
}

pair<int, int> incrementoQ( pair<int, int> a){
    return make_pair(a.first, a.second + 1);
}

pair<int, int> mejor_solucion( pair<int, int> a, pair<int, int> b){
    if (a.first == b.first){ // ¿Tienen mismo exceso?
        if (a.second < b.second){ // Devuelvo la de menos billetes
            return a;
        } else {
            return b;
        }
    } else {
        if (a.first < b.first){ // Devuelvo la de menos exceso
            return a;
        } else {
            return b;
        }
    }
}

pair<int, int> cc(vector<int> B, int c, int n){
    if (n == -1) {
        if (c >= 0) {
            return make_pair(c, 0);
        } else {
            return make_pair(99999,99999);
        }
    } else {
        return mejor_solucion(cc(B,c,n-1), incrementoQ(cc(B,c-B[n],n-1)));
    }
}

int main() {

    vector<int> B = {2,3,5,10,20,20};
    int c = 14;
    pair<int,int> res = cc(B,c,B.size()-1);

    std::cout << "Dado c = 14 y B = {2,3,5,10,20,20}" << endl;
    std::cout << "La respuesta es (" << res.first << ", " << res.second << ")" << std::endl;

    B = {2,3,5,10,20,20};
    c = 7;
    res = cc(B,c,B.size()-1);

    std::cout << "Dado c = 7 y B = {2,3,5,10,20,20}" << endl;
    std::cout << "La respuesta es (" << res.first << ", " << res.second << ")" << std::endl;
    return 0;
}
