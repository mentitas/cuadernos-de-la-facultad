!classDefinition: #Falso category: #'Ejercicio 3'!
DenotativeObject subclass: #Falso
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 3'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Falso class' category: #'Ejercicio 3'!
Falso class
	instanceVariableNames: ''!

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:20:18'!
no
	^Verdadero ! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:23:00'!
o: unBooleano
	^unBooleano! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 21:08:34'!
siFalso: unaAcciónARealizar
	^ unaAcciónARealizar value
! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 21:09:13'!
siVerdadero: unaAcciónAIgnorar
! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:29:11'!
y: unBooleano
	^ Falso! !


!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:20:18'!
no
	^Verdadero ! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:23:00'!
o: unBooleano
	^unBooleano! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 21:08:34'!
siFalso: unaAcciónARealizar
	^ unaAcciónARealizar value
! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 21:09:13'!
siVerdadero: unaAcciónAIgnorar
! !

!Falso class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:29:11'!
y: unBooleano
	^ Falso! !


!classDefinition: #Verdadero category: #'Ejercicio 3'!
DenotativeObject subclass: #Verdadero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 3'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Verdadero class' category: #'Ejercicio 3'!
Verdadero class
	instanceVariableNames: ''!

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:21:03'!
no
	^Falso! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:27:07'!
o: unBooleano
	^ Verdadero ! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/23/2024 16:53:35'!
siFalso: unaAcciónARealizar
	^ unaAcciónARealizar value! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/23/2024 17:00:13'!
siVerdadero: unaAcciónARealizar
	^ unaAcciónARealizar value.! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:30:30'!
y: unBooleano
	^ unBooleano ! !


!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:21:03'!
no
	^Falso! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:27:07'!
o: unBooleano
	^ Verdadero ! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/23/2024 16:53:35'!
siFalso: unaAcciónARealizar
	^ unaAcciónARealizar value! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/23/2024 17:00:13'!
siVerdadero: unaAcciónARealizar
	^ unaAcciónARealizar value.! !

!Verdadero class methodsFor: 'methods' stamp: 'hola 8/19/2024 20:30:30'!
y: unBooleano
	^ unBooleano ! !


!classDefinition: #tests category: #'Ejercicio 3'!
DenotativeObject subclass: #tests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio 3'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'tests class' category: #'Ejercicio 3'!
tests class
	instanceVariableNames: ''!

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 20:53:30'!
test01_Falso_no
	Assert that: Verdadero isEqualTo: Falso no! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 20:59:22'!
test02_Verdadero_no
	Assert that: Falso isEqualTo: Verdadero no! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 20:59:28'!
test03_Falso_o_Falso
	Assert that: Falso isEqualTo: (Falso o: Falso)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:01:14'!
test04_Falso_o_Verdadero
	Assert that: Verdadero isEqualTo: (Falso o: Verdadero)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:01:02'!
test05_Verdadero_o_Falso
	Assert that: Verdadero isEqualTo: (Verdadero o: Falso)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:00:02'!
test06_Verdadero_o_Verdadero
	Assert that: Verdadero isEqualTo: (Verdadero o: Verdadero)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:00:36'!
test07_Falso_y_Falso
	Assert that: Falso isEqualTo: (Falso y: Falso)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:01:28'!
test08_Falso_y_Verdadero
	Assert that: Falso isEqualTo: (Falso y: Verdadero)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:02:02'!
test09_Verdadero_y_Falso
	Assert that: Falso isEqualTo: (Verdadero y: Falso)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:02:24'!
test10_Verdadero_y_Verdadero
	Assert that: Verdadero isEqualTo: (Verdadero y: Verdadero)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/19/2024 21:10:12'!
test11_Falso_si_Falso
	Assert that: Verdadero isEqualTo: (Verdadero y: Verdadero)! !

!tests class methodsFor: 'tests' stamp: 'hola 8/23/2024 16:52:21'!
test11_Verdadero_si_Verdadero_Evalua
	| resultado |
	resultado := #NoEvalúa
	Verdadero siVerdadero: [resultado := #Evalúa].
	Assert that: resultado isEqualTo: #Evalúa! !

!tests class methodsFor: 'tests' stamp: 'hola 8/23/2024 16:52:44'!
test12_Verdadero_si_Falso_No_Evalua
	| resultado |
	resultado := #NoEvalúa
	Verdadero siVerdadero: [resultado := #Evalúa].
	Assert that: resultado isEqualTo: #NoEvalúa! !

!tests class methodsFor: 'tests' stamp: 'hola 8/23/2024 16:58:00'!
test13_Falso_si_Falso_Evalua
	Falso siFalso: [^self].
	Assert fail.! !

!tests class methodsFor: 'tests' stamp: 'hola 8/23/2024 16:59:44'!
test14_Falso_si_Falso_NoEvalua
	Falso siVerdadero: [Assert fail].! !
