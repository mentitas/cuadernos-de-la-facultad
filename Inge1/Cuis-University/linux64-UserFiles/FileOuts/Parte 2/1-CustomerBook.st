!classDefinition: #CantSuspend category: #'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: #'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: #'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing'!
assertNumberOfActiveCustomersIn: arg1 is: arg2 andSuspendedIs: arg3
	self
		assert: arg2
		equals: arg1 numberOfActiveCustomers.
	self
		assert: arg3
		equals: arg1 numberOfSuspendedCustomers.
	self
		assert: arg2 + arg3
		equals: arg1 numberOfCustomers.! !

!CustomerBookTest methodsFor: 'testing'!
should: arg1 raise: arg2 andThen: arg3
	[
	arg1 value.
	self fail ]
		on: arg2
		do: [ arg3 ].! !

!CustomerBookTest methodsFor: 'testing'!
should: arg1 raiseCantSuspendAndThen: arg2
	self
		should: arg1
		raise: CantSuspend
		andThen: arg1.! !

!CustomerBookTest methodsFor: 'testing'!
should: arg1 takeLessThan: arg2
	| temp3 temp4 |
	temp4 := Time millisecondClockValue * millisecond.
	[ arg1 ].
	temp3 := Time millisecondClockValue * millisecond.
	self assert: temp3 - temp4 < arg2.! !

!CustomerBookTest methodsFor: 'testing'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds
	| temp1 temp2 temp3 |
	temp1 := CustomerBook new.
	self
		should: [ temp1 addCustomerNamed: 'John Lennon' ]
		takeLessThan: 50 * millisecond.! !

!CustomerBookTest methodsFor: 'testing'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds
	| temp1 temp2 temp3 temp4 |
	temp1 := CustomerBook new.
	temp4 := 'Paul McCartney'.
	temp1 addCustomerNamed: temp4.
	self
		should: [ temp1 removeCustomerNamed: temp4 ]
		takeLessThan: 100 * millisecond.! !

!CustomerBookTest methodsFor: 'testing'!
test03CanNotAddACustomerWithEmptyName
	| temp1 |
	temp1 := CustomerBook new.
	self
		should: [ temp1 addCustomerNamed: '' ]
		raise: Error
		andThen: [ :argm1_2 |
			self assert: argm1_2 messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: temp1 isEmpty ].! !

!CustomerBookTest methodsFor: 'testing'!
test04CanNotRemoveAnInvalidCustomer
	| temp1 temp2 |
	temp1 := CustomerBook new.
	temp2 := 'John Lennon'.
	temp1 addCustomerNamed: temp2.
	self
		should: [ temp1 removeCustomerNamed: 'Paul McCartney' ]
		raise: NotFound
		andThen: [ :argm3_3 |
			self assert: temp1 numberOfCustomers = 1.
			self assert: (temp1 includesCustomerNamed: temp2) ].! !

!CustomerBookTest methodsFor: 'testing'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook
	| temp1 temp2 |
	temp1 := CustomerBook new.
	temp2 := 'Paul McCartney'.
	temp1 addCustomerNamed: temp2.
	temp1 suspendCustomerNamed: temp2.
	self
		assertNumberOfActiveCustomersIn: temp1
		is: 0
		andSuspendedIs: 1.
	self assert: (temp1 includesCustomerNamed: temp2).! !

!CustomerBookTest methodsFor: 'testing'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook
	| temp1 temp2 |
	temp1 := CustomerBook new.
	temp2 := 'Paul McCartney'.
	temp1 addCustomerNamed: temp2.
	temp1 suspendCustomerNamed: temp2.
	temp1 removeCustomerNamed: temp2.
	self
		assertNumberOfActiveCustomersIn: temp1
		is: 0
		andSuspendedIs: 0.
	self deny: (temp1 includesCustomerNamed: temp2).! !

!CustomerBookTest methodsFor: 'testing'!
test07CanNotSuspendAnInvalidCustomer
	| temp1 temp2 |
	temp1 := CustomerBook new.
	temp2 := 'John Lennon'.
	temp1 addCustomerNamed: temp2.
	self
		should: [ temp1 suspendCustomerNamed: 'George Harrison' ]
		raiseCantSuspendAndThen: [ :argm3_3 |
			self assert: temp1 numberOfCustomers = 1.
			self assert: (temp1 includesCustomerNamed: temp2) ].! !

!CustomerBookTest methodsFor: 'testing'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	| temp1 temp2 |
	temp1 := CustomerBook new.
	temp2 := 'John Lennon'.
	temp1 addCustomerNamed: temp2.
	temp1 suspendCustomerNamed: temp2.
	self
		should: [ temp1 suspendCustomerNamed: temp2 ]
		raiseCantSuspendAndThen: [ :argm4_3 |
			self assert: temp1 numberOfCustomers = 1.
			self assert: (temp1 includesCustomerNamed: temp2) ].! !


!classDefinition: #CustomerBook category: #'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing'!
includesCustomerNamed: arg1
	^ (active includes: arg1) or: [ suspended includes: arg1 ].! !

!CustomerBook methodsFor: 'testing'!
isEmpty
	^ active isEmpty and: [ suspended isEmpty ].! !


!CustomerBook methodsFor: 'initialization'!
initialize
	active := OrderedCollection new.
	suspended := OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management'!
addCustomerNamed: arg1
	arg1 isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	((active includes: arg1) or: [ suspended includes: arg1 ]) ifTrue: [ self signalCustomerAlreadyExists ].
	active add: arg1.! !

!CustomerBook methodsFor: 'customer management'!
numberOfActiveCustomers
	^ active size.! !

!CustomerBook methodsFor: 'customer management'!
numberOfCustomers
	^ active size + suspended size.! !

!CustomerBook methodsFor: 'customer management'!
numberOfSuspendedCustomers
	^ suspended size.! !

!CustomerBook methodsFor: 'customer management'!
removeCustomerNamed: arg1
	1
		to: active size
		do: [ :temp2 |
			arg1 = (active at: temp2) ifTrue: [
				active removeAt: temp2.
				^ arg1 ]].
	1
		to: suspended size
		do: [ :temp4 |
			arg1 = (suspended at: temp4) ifTrue: [
				suspended removeAt: temp4.
				^ arg1 ]].
	^ NotFound signal.! !

!CustomerBook methodsFor: 'customer management'!
signalCustomerAlreadyExists
	self error: self class customerAlreadyExistsErrorMessage.! !

!CustomerBook methodsFor: 'customer management'!
signalCustomerNameCannotBeEmpty
	self error: self class customerCanNotBeEmptyErrorMessage.! !

!CustomerBook methodsFor: 'customer management'!
suspendCustomerNamed: arg1
	(active includes: arg1) ifFalse: [ ^ CantSuspend signal ].
	active remove: arg1.
	suspended add: arg1.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: #'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages'!
customerAlreadyExistsErrorMessage
	^ 'Customer already exists!!!!!!'.! !

!CustomerBook class methodsFor: 'error messages'!
customerCanNotBeEmptyErrorMessage
	^ 'Customer name cannot be empty!!!!!!'.! !
