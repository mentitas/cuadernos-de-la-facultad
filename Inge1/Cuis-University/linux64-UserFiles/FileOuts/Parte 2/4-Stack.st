!classDefinition: #OOStackTest category: #'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: #'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'FR 9/23/2024 21:37:52'!
test01AfterSearchingStackHasTheSameSize

	| stack pushedObject sentenceFinder stackSize results |
	
	stack                  := OOStack new.
	sentenceFinder  := SentenceFinderByPrefix new.
	
	pushedObject := 'something'.
	stack push: pushedObject.
	stackSize  := stack size.
	
	results := sentenceFinder findSentencesIn: stack thatStartWith: 'some'.
	
	self assert: stack size = stackSize.! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'FR 9/23/2024 21:38:06'!
test02AfterSearchingStackHasSameElementsInTheSameOrder

	| stack sentenceFinder results |
	
	stack                  := OOStack new.
	sentenceFinder  := SentenceFinderByPrefix new.
	
	stack push: 'something'.
	stack push: 'thingsome'.
	
	results := sentenceFinder findSentencesIn: stack thatStartWith: 'some'.
	
	self assert:  stack pop equals: 'thingsome'.
	self assert:  stack pop equals: 'something' ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'FR 9/23/2024 21:38:22'!
test03ShouldReturnTheExpectedResults

	| stack stackBackup sentenceFinder expectedResults results |
	
	stack                  := OOStack new.
	stackBackup      := stack.
	
	sentenceFinder  := SentenceFinderByPrefix new.
	stack push: 'something'.
	stack push: 'thingsome'.
	
	expectedResults := OrderedCollection new.
	expectedResults add: 'something' .
	results := sentenceFinder findSentencesIn: stack thatStartWith: 'some'.
	self assert: expectedResults equals: results! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'FR 9/23/2024 21:38:35'!
test04ShouldReturnTheResultsInTheCorrectOrder

	| stack sentenceFinder expectedResults results |
	
	stack                  := OOStack new.
	sentenceFinder  := SentenceFinderByPrefix new.
	
	stack push: 'something'.	
	stack push: 'thingsome'.
	stack push: 'somewhat'.
	
	expectedResults := OrderedCollection new.
	expectedResults add: 'somewhat'.
	expectedResults add: 'something'.
	
	results := sentenceFinder findSentencesIn: stack thatStartWith: 'some'.
	
	self assert: expectedResults = results.! !


!classDefinition: #OOStack category: #'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'top'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'FR 9/23/2024 20:16:15'!
= aStack

	^(aStack isKindOf: self class)! !

!OOStack methodsFor: 'initialization' stamp: 'FR 9/23/2024 17:17:26'!
initialize

	top := OOStackBase new! !


!OOStack methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:06:26'!
isEmpty

	^ top isEmpty! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:29:47'!
pop
	
	| pushedObject |
	
	pushedObject := self top.
	top := top previous.
	
	 ^ pushedObject.! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:33:20'!
push: anObject

	top := OOStackPushedObjectContainer over: top with: anObject.! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:06:50'!
size
	 ^ top size! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:19:46'!
top
	^ top content! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: #'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/14/2023 08:12:21'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #OOStackTop category: #'Stack-Exercise'!
Object subclass: #OOStackTop
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTop methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:19:56'!
content

	self subclassResponsibility! !


!classDefinition: #OOStackBase category: #'Stack-Exercise'!
OOStackTop subclass: #OOStackBase
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:32:20'!
content

	self error: OOStack stackEmptyErrorDescription! !

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:05:46'!
isEmpty
	^ true! !

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:07:05'!
size
	^ 0! !


!classDefinition: #OOStackPushedObjectContainer category: #'Stack-Exercise'!
OOStackTop subclass: #OOStackPushedObjectContainer
	instanceVariableNames: 'previous content'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackPushedObjectContainer methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:20:24'!
content
	^ content! !

!OOStackPushedObjectContainer methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:19:36'!
initializeOver: aStackTop with: anObject
	previous := aStackTop.
	content  := anObject.! !

!OOStackPushedObjectContainer methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:05:56'!
isEmpty
	^ false! !

!OOStackPushedObjectContainer methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 16:08:12'!
previous
	^ previous! !

!OOStackPushedObjectContainer methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 17:20:38'!
size
	^ previous size + 1! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackPushedObjectContainer class' category: #'Stack-Exercise'!
OOStackPushedObjectContainer class
	instanceVariableNames: ''!

!OOStackPushedObjectContainer class methodsFor: 'instance creation' stamp: 'FR 9/23/2024 17:19:25'!
over: aStackTop with: anObject
	^ self new initializeOver: aStackTop with: anObject.! !


!classDefinition: #SentenceFinderByPrefix category: #'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 21:39:37'!
findIterativetalySentencesIn: aStack thatStartWith: aPrefix
	
	"Version iterativa"
	
	| sentence results poppedElements |
	
	results                 := OrderedCollection new.
	poppedElements := OrderedCollection new.

	[aStack isEmpty] whileFalse: [
	
		sentence := aStack pop.
	
		(sentence beginsWith: aPrefix) ifTrue: [
			results add: sentence.	
		].
	
		poppedElements add: sentence.
	].

	[poppedElements isEmpty] whileFalse: [
		aStack push: (poppedElements removeLast).
	].

	^ results.! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 21:42:19'!
findSentencesIn: aStack thatStartWith: aPrefix
	
	"Version recursiva"
	
	aStack isEmpty ifTrue:     [ ^ OrderedCollection new ].
	aStack isEmpty ifFalse:    [
		
		| poppedSentence results |
		
		poppedSentence := aStack pop.
		
		results := self findSentencesIn: aStack thatStartWith: aPrefix.
		
		(poppedSentence beginsWith: aPrefix) ifTrue: [results addFirst: poppedSentence	].
		
		aStack push: poppedSentence.	
		
		^ results.
	].
! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'FR 9/23/2024 21:33:14'!
searchSentencesIn2: aStack withPrefix: aPrefix
	
	"Version iterativa"
	
	| sentence results poppedElements |
	
	results                 := OrderedCollection new.
	poppedElements := OrderedCollection new.

	[aStack isEmpty] whileFalse: [
	
		sentence := aStack pop.
	
		(sentence beginsWith: aPrefix) ifTrue: [
			results add: sentence.	
		].
	
		poppedElements add: sentence.
	].

	[poppedElements isEmpty] whileFalse: [
		aStack push: (poppedElements removeLast).
	].

	^ results.! !
