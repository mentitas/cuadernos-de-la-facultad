/*************************(Ejercicio 1)*************************/

bool familiaRiendo(mapa m, posicion p){
	estado familia = m[p.first][p.second];
	int tiempoRiendose = familia.first;
	return 0<tiempoRiendose && tiempoRiendose<=15;
}

bool vecinoRiendose(mapa m, posicion p){
	bool hayVecinoRiendose = false;
	int alto  = m.size();
	int ancho = m[0].size();
	posicion pv = p;
	//hay vecinos riendose en el norte
	if (p.first<alto-1){
		pv.first++;
		hayVecinoRiendose = familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el sur
	if (p.first>0){ 
		pv.first--;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el oeste
	if(pv.second<ancho-1){ 
		pv.second++;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el este
	if(p.second>0){
		pv.second--;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
	}
	return hayVecinoRiendose;
}



/*************************(Ejercicio 2)*************************/

bool familiaAburrida(mapa m, posicion p){
	estado familia         = m[p.first][p.second];
	int tiempoRiendose     = familia.first;
	int periodosAnteriores = familia.second;
	return tiempoRiendose==0 && periodosAnteriores>0;
}

bool familiaRiendo(mapa m, posicion p){
	estado familia     = m[p.first][p.second];
	int tiempoRiendose = familia.first;
	return 0<tiempoRiendose && tiempoRiendose<=15;
}

bool familiaNuncaEscucho(mapa m, posicion p){
	estado familia         = m[p.first][p.second];
	int tiempoRiendose     = familia.first;
	int periodosAnteriores = familia.second;
	return tiempoRiendose==0 && periodosAnteriores==0;
}

void situacionComica(mapa m, int &riendo, int &aburridos, int &nuncaEscucharon){
	riendo    = 0;
	aburridos = 0;
	nuncaEscucharon = 0;
	int alto  = m.size();
	int ancho = m[0].size();
	for(int i=0; i<alto; i++){
		for(int j=0; j<ancho; j++){
			posicion p = make_pair(i, j);
			if(familiaRiendo(m, p)){
				riendo++;
			}
			if(familiaAburrida(m, p)){
				aburridos++;
			}
			if(familiaNuncaEscucho(m, p)){
				nuncaEscucharon++;
			}
		}
	}
}

/*************************(Ejercicio 3)*************************/

int devuelvoPeriodosAnteriores (mapa m, posicion p){
	estado familia = m[p.first][p.second];
	return familia.second;
}

vector<posicion> mayorCantidadCarcajadasPasadas(mapa m){
	int max = 0;
	int alto  = m.size();
	int ancho = m[0].size();
	vector<posicion> posicionesMaximas;
	for(int i=0; i<alto; i++){
		for(int j=0; j<ancho; j++){
			posicion p = make_pair(i,j);
			int periodosAnteriores = devuelvoPeriodosAnteriores(m, p);
			if(periodosAnteriores>max){
				max = periodosAnteriores;
				posicionesMaximas = {p};
			} 
			if (periodosAnteriores == max){
				posicionesMaximas.push_back(p);
			}
		}
	}
	return posicionesMaximas;
}


/*************************(Ejercicio 4)*************************/

bool familiaRiendo(mapa m, posicion p){
	estado familia     = m[p.first][p.second];
	int tiempoRiendose = familia.first;
	return 0<tiempoRiendose && tiempoRiendose<=15;
}

bool vecinoRiendose(mapa m, posicion p){
	bool hayVecinoRiendose = false;
	int alto  = m.size();
	int ancho = m[0].size();
	posicion pv = p;
	//hay vecinos riendose en el norte
	if (p.first<alto-1){
		pv.first++;
		hayVecinoRiendose = familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el sur
	if (p.first>0){ 
		pv.first--;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el oeste
	if(pv.second<ancho-1){ 
		pv.second++;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
		pv = p;
	}
	//hay vecinos riendose en el este
	if(p.second>0){
		pv.second--;
		hayVecinoRiendose = hayVecinoRiendose || familiaRiendo(m, pv);
	}
	return hayVecinoRiendose;
}

int terminaPeriodo (mapa m, posicion p){
	estado familia = m[p.first][p.second];
	return familia.first==15;
}

estado actualizarEstadoFamilia (mapa m0, posicion p){
	estado res = m0[p.first][p.second];
	if(familiaRiendo(m0, p) && !terminaPeriodo(m0, p)){
		res.first++;
	} else if (familiaRiendo(m0, p) && terminaPeriodo(m0, p)){
		res.first=0;
		res.second++;
	} else if (!familiaRiendo(m0, p) && vecinoRiendose(m0, p)){
		res.first=1;
	}
	return res;
}

void actualizarMapa (mapa &m){
	mapa m0 = m;
	int alto  = m.size();
	int ancho = m[0].size();
	for(int i=0; i<alto; i++){
		for(int j=0; j<ancho; j++){
			posicion p = make_pair(i,j);
			m[i][j] = actualizarEstadoFamilia(m0, p);
		}
	}
}