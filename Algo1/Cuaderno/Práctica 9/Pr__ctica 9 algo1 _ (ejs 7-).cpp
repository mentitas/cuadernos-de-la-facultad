// Ejercicio 7.
// Definimos a una secuencia de enteros como “de cuentas” si sus elementos aparecen tantas veces en la secuencia
// como su valor lo indique. Por ejemplo, [4, 1, 5, 5, 4, 4, 4, 5, 5, 5] es una secuencia “de cuentas”.
// Escribir un programa que determine si una secuencia de enteros cualquiera s es o no “de cuentas” con tiempo de
// ejecución de peor caso perteneciente a O(|s|). Justificar por qué este algoritmo cumple con dicha complejidad.

bool esDeCuentas (vector<int> v){
	int max = 0;
	bool res = true;
	//busco el máximo
	for (int i=0; i<v.size(); i++){
		if (v[i]>max){
			max = v[i];
		}
	}
	vector<int> indice(max+1); // mejor armar esto con un for para poder estar segura de que es O(n)
	//armo un indice
	for (int j=0; j<v.size(); j++){
		indice(v[j])++;
	}
	//el indice debe ser de cuentas (sin tener en cuenta los ceros)
	for (int k=0; k<indice.size(); k++){ 
		if(k!=0 && indice[k]!=k){
			res = false;
		}
	}
	return res;
}

// El peor tiempo de ejecución es O(3n), que pertenece a O(n)
// El algoritmo tiene esa complejidad porque tiene 3 Fors, y como cada uno recorre la secuencia una vez,
// tienen complejidad O(n), entonces O(n)+O(n)+O(n)=O(3n)

//acá verifico la complejidad correctamente
// bool esDeCuentas (vector<int> v){
// 	int max = 0;        //1
// 	bool res = true;    //1
// 	for (int i=0; i<v.size(); i++){ //init 1, guarda 3, incr 1, iteraciones n
// 		if (v[i]>max){  //4
// 			max = v[i]; //3
// 		}
// 	}
// 	vector<int> indice(max+1); //1
// 	for (int j=0; j<v.size(); j++){ //init 1, guarda 3, incr 1, iteraciones max (max<=n, pero en el peor caso max=n)
// 		indice(v[j])++;        //4
// 	}
// 	for (int k=0; k<max+1; k++){ //init 1, guarda 3, incr 1, iteraciones max+1 (en el peor caso, max+1=n+1)
// 		if(k!=0 && indice[k]!=k){  // 7
// 			res = false;           // 1
// 		}
// 	}
// 	return res;
// }

// t(n) = 2 + [1+3+n(3+1+4+3)] + 1 + [1+3+n(3+1+4)] + [1+3+(n+1)(3+1+7+1)] ---> O(3n)





// Ejercicio 8.
// Escribir un programa que devuelva la versión ordenada de un string s con costo de ejecución perteneciente a
// O(|s|). Por ejemplo, si s = "hola Homero!", el resultado deberá ser "!Haehlmooor". Suponer que se cuenta con la
// función ord(c) que dado un caracter indica su código en el standard Unicode 1 .

string ordenarString (string palabra){
	int max = 0;
	string res;
	//Busco máximo
	for (int i=0; i<palabra.size(); i++){
		if (ord(palabra[i])>max){
			max = ord(palabra[i]);
		}
	}
	//Armo índice y cantidades
	vector<char> indice(max+1);
	vector<int> cantidades(max+1);
	for (int j=0; j<max+1; j++){
		char c = palabra[j];
		cantidades(ord(c))++
		indice[ord(c)]=c
	}
	//Armo un string res ordenado
	int h=0; //qué elemento del indice estamos viendo
	int k=0; //cantidad de iteraciones
	while (k<palabra.size()){
		if (cantidades[h]>0){
			res.push_back(indice(h));
			cantidades[h]=cantidades[h]-1;
		} else {
			h++;
		}
		k++;
	}
	return res;
}

//El algoritmo es de complejidad O(n)

// string ordenarString (string palabra){
// 	int max = 0; // 1
// 	string res;  // 1
// 	for (int i=0; i<palabra.size(); i++){ // init 1, guarda 3, incr 1, iteraciones n
// 		if (ord(palabra[i])>max){  // 5
// 			max = ord(palabra[i]); //4
// 		}
// 	}
// 	vector<char> indice(max+1);    // 1
// 	vector<int> cantidades(max+1); // 1
// 	for (int j=0; j<max+1; j++){ // init 1, guarda 3, incr 1, iteraciones max+1 (en el peor caso, max=112956)
// 		char c = palabra[j];       // 3
// 		cantidades(ord(c))++       // 3
// 		indice[ord(c)]=c           // 4
// 	}
// 	int h=0; // 1
// 	int k=0; // 1
// 	while (k<palabra.size()){ //guarda 3, iteraciones n
//  		if (cantidades[h]>0){              // 3, en el peor caso el if es true 
// 			res.push_back(indice(h));      // 3
// 			cantidades[h]=cantidades[h]-1; // 1 o 6 :(
// 		} else {
// 			h++; // 1

// 		}
// 		k++;     // 1
// 	}
// 	return res;
// }

// t(n) = 2 + [1+3+n(3+1+5+4)] + 2 + [1+3+(max+1)(3+1+3+3+4)] + 2 + [3+n(3+3+6+1)] ---> O(n)





// Ejercicio 9.
// Escribir un programa que, dado una secuencia de enteros v y dos enteros e y k (con k ≤ |v|), devuelva los k
// números más cercanos a e. En caso de empates, considerar el de menor valor. Calcular el tiempo de ejecución
// de peor caso.

//voy a considerar que son cercanos en valor, onda en v=[3,9,9,9,9,9,4], 3 es cercano a 4 (aunque estén lejos
//en distancia)

bool esMasCercano (int e, int a, int b){// --> ¿a es mas cercano que b?
	return (abs(e-a)<abs(e-b));
}

int encontrarMasCercanoDesde (vector<int> v, int e, int desde){
	int elMasCercano =desde;
	for (int i=desde; i<v.size(); i++){
		if(esMasCercano(e,v[i],v[elMasCercano])){
			elMasCercano=i;
		}
	}
	return elMasCercano;
}

vector<int> encontrarKMasCercanos (vector<int> v, int elem, int k){
	int elMasCercano=0;
    vector<int> vaux = v;
	for(int i=0; i<v.size(); i++){
		elMasCercano=encontrarMasCercanoDesde(vaux, elem, i);
		swap(vaux, i, elMasCercano);
	}
	for(int j=0; j<v.size()-k; j++){
		vaux.pop_back();
	}
	return vaux;
}



// Ejercicio 10.
// El algoritmo de ordenamiento cocktail sort es una variante del selection sort que consiste en buscar en cada
// iteración el máximo y el mı́nimo de la secuencia por ordenar, intercambiando el mı́nimo con i y el máximo con
// |s| − i − 1. Escribir un programa implementando el algoritmo cocktail sort.

int encontrarMinimoConRango(vector<int> v, int desde, int hasta){  //Desde incluye, Hasta no incluye
	int idminimo = v[desde];
	for (int i=desde; i<hasta; i++){
		if(v[i]<v[idminimo]){
			idminimo = i;
		}
	}
	return idminimo;
}

int encontrarMaximoConRango(vector<int> v, int desde, int hasta){
	int idmaximo = v[desde];
	for (int i=desde; i<hasta; i++){
		if(v[i]>v[idmaximo]){
			idmaximo = i;
		}
	}
	return idmaximo;
}

void swap (vector<int> &v, int a, int b){
	int aux = v[a];
	v[a]=v[b];
	v[b]=aux;
}

void cocktailSort (vector<int> &v){
	int minimoEncontrado = 0;
	int maximoEncontrado = 0;
    std::cout<<""<<std::endl;
	for (int i=0; i<v.size()/2; i++){
		minimoEncontrado = encontrarMinimoConRango(v, i, v.size()-i);
		maximoEncontrado = encontrarMaximoConRango(v, i, v.size()-i);
		swap(v,minimoEncontrado,i);
		swap(v,maximoEncontrado,v.size()-1-i);
	}
}
// Ejercicio 11.
// Consideremos el siguiente programa de ordenamiento, llamado ordenamiento por burbujeo (bubble sort):

void bubbleSort(vector<int> a){
	int i = 0;
	int j;
	while (i < a.size()-1) {
		j = 0;
		while (j < a.size()-1) {
			if (a[j] > a[j+1]){
				swap(a, j, j+1);
			}
			j++;
		}
		i++
	}
}
// a) Describir con palabras qué hace este programa.
// b) Proponer un invariante para el ciclo principal (el más externo).
// c) Proponer un invariante para el ciclo interno.
// d) Calcular el tiempo de ejecución de peor caso.

//a. El algoritmo recorre la secuencia y se fija de a pares (a,b) cuál es el más grande. Una vez que decide cuál es
//   el más grande, lo intercambia para que quede en segundo lugar (posicion b). Luego, compara ese elemnto con 
//   el siguiente (b,c), y si decide que b es más grande lo vuelve a cambiar de lugar (c,b). De esta manera, los
//   el elemento más grande de la secuencia se va hasta el fondo.
//   Luego, esto se repite, y se encuentra el segundo más grande, y se coloca anteúltimo,
//   y así sucesivamente, hasta que la secuencia está ordenada.

//b. I = { 0 <= i <= |a|-1 && estaOrdenado(subSeq(a,|a|-1-j, |a|-1))
//        && (A g:Z)(0 <= g< |a|-1-j ->L a[g]<a[|a-1-j])}

//c. I = {0 <= j <= |a|-1 && (j>0 && a[j]>a[j-1])}

//d.

// void bubbleSort(vector<int> a){
// 	int i = 0; //1
// 	int j;     //1
// 	while (i < a.size()-1) { // guarda 4, iteraciones |a|-1
// 		j = 0;                   //1
// 		while (j < a.size()-1) { //guarda 4, iteraciones |a|-1
// 			if (a[j] > a[j+1]){  //6
// 				swap(a, j, j+1); //12
// 			}
// 			j++; //1
// 		}
// 		i++ //1
// 	}
// }

// |a|=n; t(n)= 2 + [4+(n-1)(1+ [4+(n-1)(6+12 +1)]+1)]
//        t(n)= 2 + [4+(n-1)(6+ 19(n-1))]                 --> O(n^2)

// RTA: es de complejidad O(n^2)



// Ejercicio 12.
// El bubble sort puede ser modificado de manera que el burbujeo se realice en ambas direcciones
// (cocktail shaker sort). Una primera pasada para adelante en la lista, y una segunda pasada de regreso.
// Este patrón alternado se repite hasta que no sea necesario continuar.

// a) Implementar un programa para el algoritmo descripto.
// b) Calcular el tiempo de ejecución de peor caso.
// c) Mostrar paso a paso como el algoritmo ordenarı́a la siguiente secuencia: [4, 2, 1, 5, 0]

//a.
void cocktailShakerSort(vector<int> & a){
	int i = 0;
	int j;
	while (i < a.size()-1) {
		j = 0;
		while (j < a.size()-1) {
			if (a[j] > a[j+1]){
				swap(a, j, j+1);
			}
			j++;
		}
		while (j > 0) {
			if (a[j] < a[j-1]){
				swap(a, j, j-1);
			}
			j=j-1;
		}
		i++;
	}
}

//b.

void cocktailShakerSort(vector<int> & a){
	int i = 0; //1
	int j;     //1
	while (i < a.size()/2) { //guarda 4, iteraciones |a|/2
		j = 0; //1
		while (j < a.size()-1) { //guarda 4, iteraciones |a|-1
			if (a[j] > a[j+1]){    //6
				swap(a, j, j+1);   //12
			}
			j++; //1
		}
		while (j > 0) { //guarda 2, iteraciones |a|-1
			if (a[j] < a[j-1]){   //6
				swap(a, j, j-1);  //12
			}
			j=j-1; //4
		}
		i++; //1
	}
}

|a|=n, t(n)=2 + [4+n/2(4+1+4+ [(n-1)(4+6+12+1)] + [2+(n-1)(2+6+12+4)] +1)]  --> O(n^2)

//c. 0.   [4, 2, 1, 5, 0]
//   0,5. [4, 2, 1, 0, 5]
//   1.   [0, 2, 1, 4, 5]
//   1,5. [0, 2, 1, 4, 5]
//   2.   [0, 1, 2, 4, 5]

// Ejercicio 14.
// El algoritmo de ordenamiento bingo sort es una variante del selection sort que consiste en ubicar todas las
// apariciones del valor mı́nimo en la secuencia por ordenar, y mover todos estos valores al mismo tiempo
// (siendo efectivo cuando hay muchos valores repetidos).
// Escribir un programa que implemente el algoritmo bingo sort.

vector<int> encontrarMinimosDesde (vector<int> v, int desde){
	int idminimo = desde;
	for (int i=desde; i<v.size(); i++){
		if (v[i]<v[idminimo]){
			idminimo = i;
		}
	}
	vector<int> minimos;
	for (int j=desde; j<v.size(); j++){
		if (v[j]==v[idminimo]){
			minimos.push_back(j);
		}
	}
	return minimos;
}

void bingoSort (vector<int> & v){
	vector<int> minimos;
	for (int i=0; i<v.size(); i++){
		minimos = encontrarMinimosDesde(v, i);
		for (int j=0; j<minimos.size(); j++){
			swap(v, minimo[j], j+i);
		}

	};
}

// Ejercicio 16.
// Dar un algoritmo que resuelva este problema:
// proc dosMitades (inout a: seq(Z)) {
// Pre {|a| ≥ 2 ∧ (∃i : Z) 0 ≤ i < |a| ∧ ordenado(subseq(a, 0, i)) ∧ ordenado(subseq(a, i, |a|)) }
// Post{ordenado(a)}
// }

// Yo no se cuánto es i, pero se que i es un Pico
// Ej [1,3,5,7,9,2,4,6,8] ---> 9 es un pico

//traigo un ejercicio del labo10
int indicePico(vector<int> v){
    if(v.size()==0){ // es una lista vacia
        return -1;
    }
    if(v[0]>v[1]){ // el primer elemento es un pico
        return 0;        
    }
    if(v[v.size()-1]>v[v.size()-2]){ // el último elemento es un pico
        return v.size()-1;
    }
    
    for(int i=1; i<v.size()-1; i++){
        if(v[i]>v[i+1] && v[i]>v[i-1]){
            return i;
        }
    }
    return -1;
}
                  
void quitar (vector<int> &v, int n){
	vector<int> res;
	for(int i=0; i<v.size(); i++){
		if(i!=n){
			res.push_back(v[i]);
		}
	}
	v = res;
}

void insertar (vector<int> &v, int pos, int elem){
	vector<int> res;
	for(int i=0; i<v.size(); i++){
		if(i==pos){
			res.push_back(elem);
		}
		res.push_back(v[i]);
	}
	v = res;
} 


void cambiarDeLugar (vector<int> &v, int a, int b){ // a=pos actual, b=pos a donde lo muevo
	int elem = v[a];
	quitar(v, a);
	insertar(v, b, elem);
}

void dosMitades(vector<int> &a){
	int m = indicePico(a); // m de mitad
	int p = 0              // p de principio
	while(m<v.size()){
		if(v[p]>v[m]){
			cambiarDeLugar(v, m, p);
			p++;
			m++;
		}
	}
}
