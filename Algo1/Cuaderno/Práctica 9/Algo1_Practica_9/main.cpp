#include"ejercicios.h"

void mostrarVector(vector<int> v){
    cout << "[";
    for(int i=0; i<v.size(); i=i+1){
        if (i==v.size()-1){
            cout <<v[i]<<"]"<< endl;
        } else{
            cout <<v[i]<<", ";
        }
    }
}


#include <iostream>
#include "gtest/gtest.h"

using namespace std;
int main(int argc, char **argv) {
    std::cout << "Implementando TPI!!" << std::endl;
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

/*
int main(int argc, char **argv) {
    bool res = false;
    std::cout << "***Pruebo ejercicio 7***" << std::endl;
    std::cout << "Ingreso el vector v={4,1,5,5,4,4,4,5,5,5}, debería devolver True" << std::endl;
    res = esDeCuentas({4,1,5,5,4,4,4,5,5,5});
    std::cout << res << std::endl;
    std::cout << "Ingreso el vector v={4,1,4,4,4}, debería devolver True" << std::endl;
    res = esDeCuentas({4,1,4,4,4});
    std::cout << res << std::endl;
    std::cout << "Ingreso el vector v={1}, debería devolver True" << std::endl;
    res = esDeCuentas({1});
    std::cout << res << std::endl;
    std::cout << "Ingreso el vector v={1,2,3,4}, debería devolver False" << std::endl;
    res = esDeCuentas({1,2,3,4});
    std::cout << res << std::endl;
    
    std::cout <<""<<std::endl;
    
    string res2;
    std::cout << "***Pruebo ejercicio 8***" << std::endl;
    std::cout << "Ingreso el string 'hola Homero!', debería devolver ' !Haehlmooor'" << std::endl;
    res2 = ordenarString("hola Homero!");
    std::cout << res2 << std::endl;
    std::cout << "Ingreso el string 'Pirincho', debería devolver 'Pchiinor'" << std::endl;
    res2 = ordenarString("Pirincho");
    std::cout << res2 << std::endl;
    
    std::cout <<""<<std::endl;
    
    vector<int> res3;
    std::cout << "***Pruebo ejercicio 9***" <<std::endl;
    std::cout << "Ingreso el vector v={0,5,4,0,0,7}, con e=6 y k=3, debería devolver {5,4,7}" << std::endl;
    res3=encontrarKMasCercanos({0,5,4,0,0,7},6,3);
    mostrarVector(res3);
    std::cout << "Ingreso el vector v={1,2,3,4,5,6}, con e=3 y k=3, debería devolver {3,2,4}" << std::endl;
    res3=encontrarKMasCercanos({1,2,3,4,5,6},3,3);
    mostrarVector(res3);
     std::cout << "Ingreso el vector v={3,5,1,4,2,6,7}, con e=4 y k=3, debería devolver {4,3,5}" << std::endl;
    res3=encontrarKMasCercanos({3,5,1,4,2,6,7},4,3);
    mostrarVector(res3);
    
    
    
    std::cout <<""<<std::endl;
    
    std::cout << "***Pruebo ejercicio 10***" << std::endl;
    std::cout << "Ingreso el vector v={0,9,8,7,6,5,4,3,2,1}, debería devolver {0,1,2,3,4,5,6,7,8,9}" << std::endl;
    res3 = {0,9,8,7,6,5,4,3,2,1};
    cocktailSort(res3);
    mostrarVector(res3);
    std::cout << "Ingreso el vector v={5,4,6,7,2,1,54}, debería devolver {1,2,4,5,6,7,54}" << std::endl;
    res3 = {5,4,6,7,2,1,54};
    cocktailSort(res3);
    mostrarVector(res3);
    
    std::cout <<""<<std::endl;
    
    std::cout << "***Pruebo ejercicio 12***" << std::endl;
    std::cout << "Ingreso el vector v={0,9,8,7,6,5,4,3,2,1}, debería devolver {0,1,2,3,4,5,6,7,8,9}" << std::endl;
    res3 = {0,9,8,7,6,5,4,3,2,1};
    cocktailShakerSort(res3);
    mostrarVector(res3);
    std::cout << "Ingreso el vector v={5,4,6,7,2,1,54}, debería devolver {1,2,4,5,6,7,54}" << std::endl;
    res3 = {5,4,6,7,2,1,54};
    cocktailShakerSort(res3);
    mostrarVector(res3);
    
    std::cout <<""<<std::endl;
    
    std::cout << "***Pruebo ejercicio 14" << std::endl;
    std::cout << "Ingreso el vector v={0,9,8,7,6,5,4,3,2,1}, debería devolver {0,1,2,3,4,5,6,7,8,9}" << std::endl;
    res3 = {0,9,8,7,6,5,4,3,2,1};
    bingoSort(res3);
    mostrarVector(res3);
    std::cout << "Ingreso el vector v={5,4,6,7,2,1,54}, debería devolver {1,2,4,5,6,7,54}" << std::endl;
    res3 = {5,4,6,7,2,1,54};
    bingoSort(res3);
    mostrarVector(res3);    
    std::cout << "Ingreso el vector v={5,5,5,2,4,2,1,3,3,3,4,3,0}, debería devolver {0,1,2,2,3,3,3,3,4,4,5,5,5}" << std::endl;
    res3 = {5,5,5,2,4,2,1,3,3,3,4,3,0};
    bingoSort(res3);
    mostrarVector(res3);
    
    std::cout <<""<<std::endl;
    
    std::cout << "***Pruebo ejercicio 16***" << std::endl;
    std::cout << "Ingreso el vector v={2,4,6,8,1,3,5,7,9}, debería devolver {1,2,3,4,5,6,7,8,9}" << std::endl;
    res3 = {2,4,6,8,1,3,5,7,9};
    dosMitades(res3);
    mostrarVector(res3);
    
    
    return 0;
}
*/
