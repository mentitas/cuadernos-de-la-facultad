#include "ejercicios.h"
#include <ostream>
#include <iostream>

using namespace std;

int ord (char c){
    return c;
}

void swap (vector<int> &v, int a, int b){
	int aux = v[a];
	v[a]=v[b];
	v[b]=aux;
}

//**despues borrar**//
void muestroVector(vector<int> v){
    cout << "[";
    for(int i=0; i<v.size(); i=i+1){
        if (i==v.size()-1){
            cout <<v[i]<<"]"<< endl;
        } else{
            cout <<v[i]<<", ";
        }
    }
}
//Ejercicio1
/*
pair<int,int> buscoEnMatriz (vector<vector<int> m, int x){
    pair<int,int> = make_pair(-1,-1);
    bool encontre  = false;
    for(int i=0; !encontre && i<m.size(); i++){
        for(int j=0; !encontre && j<m[0].size(); j++){
            if(m[i][j]==x){
                res.first = i;
                res.second = j;
                encontre = true;
        }
    }
    return res;
}
*/

//Ejercicio7
bool esDeCuentas (vector<int> v){
	int max = 0;
	bool res = true;

	//busco el máximo
	for (int i=0; i<v.size(); i++){
		if (v[i]>max){
			max = v[i];
		}
	}

	vector<int> indice(max+1);
	//armo un indice
	for (int j=0; j<v.size(); j++){
		indice[v[j]]++;
	}
    
	//el indice debe ser de cuentas (sin tener en cuenta los ceros)
	for (int k=0; k<indice.size(); k++){ 
		if(indice[k]!=0 && indice[k]!=k){
			res = false;
		}
	}
	return res;
}

//Ejercicio8

string ordenarString (string palabra){
	int max = 0;
	string res;
	//Busco máximo
	for (int i=0; i<palabra.size(); i++){
		if (ord(palabra[i])>max){
			max = ord(palabra[i]);
		}
	}
	//Armo índice y cantidades
	vector<char> indice(max+1);
	vector<int> cantidades(max+1);
	for (int j=0; j<palabra.size(); j++){
		char c = palabra[j];
		cantidades[ord(c)]++;
		indice[ord(c)]=c;
	}
	
	
	//Armo un string res ordenado
	int h=0; //qué elemento del indice estamos viendo
	int k=0; //cantidad de iteraciones
	while (k<indice.size()+palabra.size()){
		if (cantidades[h]>0){
			res.push_back(indice[h]);
			cantidades[h]=cantidades[h]-1;
		} else {
			h++;
		}
		k++;
	}
	return res;
}

//Ejercicio9
bool esMasCercano (int e, int a, int b){// --> ¿a es mas cercano que b?
	return (abs(e-a)<abs(e-b));
}

int encontrarMasCercanoDesde (vector<int> v, int e, int desde){
	int elMasCercano =desde;
	for (int i=desde; i<v.size(); i++){
		if(esMasCercano(e,v[i],v[elMasCercano])){
			elMasCercano=i;
		}
	}
	return elMasCercano;
}

vector<int> encontrarKMasCercanos (vector<int> v, int elem, int k){
	int elMasCercano=0;
    vector<int> vaux = v;
	for(int i=0; i<v.size(); i++){
		elMasCercano=encontrarMasCercanoDesde(vaux, elem, i);
		swap(vaux, i, elMasCercano);
	}
	for(int j=0; j<v.size()-k; j++){
		vaux.pop_back();
	}
	return vaux;
}


//Ejercicio10
int encontrarMinimoConRango(vector<int> v, int desde, int hasta){  //Desde incluye, Hasta no incluye
	int idminimo = v[desde];
	for (int i=desde; i<hasta; i++){
		if(v[i]<v[idminimo]){
			idminimo = i;
		}
	}
	return idminimo;
}

int encontrarMaximoConRango(vector<int> v, int desde, int hasta){
	int idmaximo = v[desde];
	for (int i=desde; i<hasta; i++){
		if(v[i]>v[idmaximo]){
			idmaximo = i;
		}
	}
	return idmaximo;
}



void cocktailSort (vector<int> &v){
	int minimoEncontrado = 0;
	int maximoEncontrado = 0;
	for (int i=0; i<v.size()/2; i++){
		minimoEncontrado = encontrarMinimoConRango(v, i, v.size()-i);
		maximoEncontrado = encontrarMaximoConRango(v, i, v.size()-i);
		swap(v,minimoEncontrado,i);
		swap(v,maximoEncontrado,v.size()-1-i);
	}
}

//Ejercicio11
void bubbleSort (vector<int> &v){
	for(int i=0; i<v.size()-1; i++){
		for(int j=0; j<v.size()-1; j++){
			if(v[j]>v[j+1]){
				swap(v,j,j+1);
			}
		}
	}
}

//Ejercicio12
void cocktailShakerSort(vector<int> & a){
	int i = 0;
	int j;
	while (i < a.size()/2) {
		j = 0;
		while (j < a.size()-1) {
			if (a[j] > a[j+1]){
				swap(a, j, j+1);
			}
			j++;
		}
		while (j > 0) {
			if (a[j] < a[j-1]){
				swap(a, j, j-1);
			}
			j=j-1;
		}
		i++;
	}
}


//Ejercicio 14
vector<int> encontrarMinimosDesde (vector<int> v, int desde){
	int idminimo = desde;
	for (int i=desde; i<v.size(); i++){
		if (v[i]<v[idminimo]){
			idminimo = i;
		}
	}
	vector<int> minimos;
	for (int j=desde; j<v.size(); j++){
		if (v[j]==v[idminimo]){
			minimos.push_back(j);
		}
	}
	return minimos;
}

void bingoSort (vector<int> & v){
	vector<int> minimos;
	for (int i=0; i<v.size(); i++){
		minimos = encontrarMinimosDesde(v, i);
		for (int j=0; j<minimos.size(); j++){
			swap(v, minimos[j], j+i);
		}

	};
}

//Ejercicio16
int indicePico(vector<int> v){
    if(v.size()==0){ // es una lista vacia
        return -1;
    }
    if(v[0]>v[1]){ // el primer elemento es un pico
        return 0;        
    }
    if(v[v.size()-1]>v[v.size()-2]){ // el último elemento es un pico
        return v.size()-1;
    }
    
    for(int i=1; i<v.size()-1; i++){
        if(v[i]>v[i+1] && v[i]>v[i-1]){
            return i;
        }
    }
    return -1;
}
                  
void quitar (vector<int> &v, int n){
	vector<int> res;
	for(int i=0; i<v.size(); i++){
		if(i!=n){
			res.push_back(v[i]);
		}
	}
	v = res;
}

void insertar (vector<int> &v, int pos, int elem){
	vector<int> res;
	for(int i=0; i<v.size(); i++){
		if(i==pos){
			res.push_back(elem);
		}
		res.push_back(v[i]);
	}
	v = res;
} 


void cambiarDeLugar (vector<int> &v, int a, int b){ // a=pos actual, b=pos a donde lo muevo
	int elem = v[a];
	quitar(v, a);
	insertar(v, b, elem);
}

void dosMitades(vector<int> &a){
	int m = indicePico(a);  // m de mitad
	int p = 0;              // p de principio
	while(p<a.size()){
		if(a[p]>a[m]){
			cambiarDeLugar(a, m, p);
			p++;
			m++;
		}
		p++;
	}
}


//2do parcial 2c 2019
bool hayInterseccion(vector<int> s, vector<int> t){
	int i=s.size();
	int j=t.size();
	while (i>1 && j>1 && t[j-1]!=s[i-1]){
		if(s[i-1]>t[j-1]){
			i=i-1;
		}else{
			j=j-1;
		}
	}
	while(j>1 && t[j-1]!=s[i-1]){
		j=j-1;
	}
	while(i>1 && t[j-1]!=s[i-1]){
		i=i-1;
	}
	return t[j-1]==s[i-1];
}


//2do parcial 2c 2020
int cuantosEnComun (vector<int> A, vector<int> B){
	//ordenar(A);
	//ordenar(B);
	int i=0;
	int j=0;
	int res=0;
	while(i<A.size() && j<B.size()){
		if(A[i]>B[j]){
			j++;
		} else if (A[i]<B[j]){
			//A[i] no existe en B
			i++;
		} else {
			//lo encontré, A[i]==B[j]
			i++;
			j++;
			res++;
		}
	}
	return res;
}
