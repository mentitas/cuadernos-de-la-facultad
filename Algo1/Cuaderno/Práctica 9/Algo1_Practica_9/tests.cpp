#include "gtest/gtest.h"
#include "ejercicios.h"


TEST(swap, primeroConUltimo){
	vector<int> v = {1,2,3,4,5};
    vector<int> vEsperado = {5,2,3,4,1};
    swap(v,0,4);
	EXPECT_EQ(v, vEsperado);
}

//Ejercicio 7
TEST(esDeCuentas, esDeCuentasCon_1_4_5){
	vector<int> v = {4,1,5,5,4,4,4,5,5,5};
	EXPECT_EQ(true, esDeCuentas(v));
}
TEST(esDeCuentas, esDeCuentasCon_1_4){
	vector<int> v = {4,1,4,4,4};
	EXPECT_EQ(true, esDeCuentas(v));
}
TEST(esDeCuentas, esDeCuentasCon_1){
	vector<int> v = {1};
	EXPECT_EQ(true, esDeCuentas(v));
}
TEST(esDeCuentas, noEsDeCuentas){
	vector<int> v = {1,2,3,4};
	EXPECT_EQ(false, esDeCuentas(v));
}

//Ejercicio 8
TEST(ordenarString, holaHomero){
    string res = "hola Homero";
    EXPECT_EQ(ordenarString(res), " !Haehlmooor");
}
TEST(ordenarString, Pirincho){
    string res = "Pirincho";
    EXPECT_EQ(ordenarString(res), "Pchiinor");
}

//Ejercicio 9 
TEST(encontrarKMasCercanos, tresCercanosYCeros){
    vector<int> v={0,5,4,0,0,7};
    vector<int> vEsperada = {5,7,4};
    EXPECT_EQ(vEsperada, encontrarKMasCercanos(v,6,3));
}
TEST(encontrarKMasCercanos, tresCercanos){
    vector<int> v={1,2,3,4,5,6};
    vector<int> vEsperada = {3,2,4};
    EXPECT_EQ(vEsperada, encontrarKMasCercanos(v,3,3));
}
TEST(encontrarKMasCercanos, dosCercanosYElElem){
    vector<int> v={3,5,1,4,2,6,7};
    vector<int> vEsperada = {4,3,5};
    EXPECT_EQ(vEsperada, encontrarKMasCercanos(v,4,3));
}


//Ejercicio 10
TEST(cocktailSort, ordenDecrec){
    vector<int> v= {0,9,8,7,6,5,4,3,2,1};
    vector<int> vEsperada = {0,1,2,3,4,5,6,7,8,9};
    cocktailSort(v);
    EXPECT_EQ(v, vEsperada);
}
TEST(cocktailSort, ordenRandom){
    vector<int> v= {5,4,6,7,2,1,54};
    vector<int> vEsperada = {1,2,4,5,6,7,54};
    cocktailSort(v);
    EXPECT_EQ(v, vEsperada);
}

//Ejercicio 11
TEST(bubbleSort, ordenDecrec){
    vector<int> v= {0,9,8,7,6,5,4,3,2,1};
    vector<int> vEsperada = {0,1,2,3,4,5,6,7,8,9};
    bubbleSort(v);
    EXPECT_EQ(v, vEsperada);
}
TEST(bubbleSort, ordenRandom){
    vector<int> v= {5,4,6,7,2,1,54};
    vector<int> vEsperada = {1,2,4,5,6,7,54};
    bubbleSort(v);
    EXPECT_EQ(v, vEsperada);
}

//Ejercicio 12
TEST(cocktailShakerSort, ordenDecrec){
    vector<int> v= {0,9,8,7,6,5,4,3,2,1};
    vector<int> vEsperada = {0,1,2,3,4,5,6,7,8,9};
    cocktailShakerSort(v);
    EXPECT_EQ(v, vEsperada);
}
TEST(cocktailShakerSort, ordenRandom){
    vector<int> v= {5,4,6,7,2,1,54};
    vector<int> vEsperada = {1,2,4,5,6,7,54};
    cocktailShakerSort(v);
    EXPECT_EQ(v, vEsperada);
}

//Ejercicio 14
TEST(bingoSort, ordenDecrec){
    vector<int> v= {0,9,8,7,6,5,4,3,2,1};
    vector<int> vEsperada = {0,1,2,3,4,5,6,7,8,9};
    bingoSort(v);
    EXPECT_EQ(v, vEsperada);
}
TEST(bingoSort, ordenRandom){
    vector<int> v= {5,4,6,7,2,1,54};
    vector<int> vEsperada = {1,2,4,5,6,7,54};
    bingoSort(v);
    EXPECT_EQ(v, vEsperada);
}

//Ejercicio 16
TEST(dosMitades, paresEImpares){
    vector<int> v= {2,4,6,8,1,3,5,7,9};
    vector<int> vEsperada = {1,2,3,4,5,6,7,8,9};
    dosMitades(v);
    EXPECT_EQ(v, vEsperada);
}

//2do parcial 2c 2019
TEST(hayInterseccion, existeIntersecion){
    vector<int> s = {0,1,2,3,4,5,6,7};
    vector<int> t = {3,8,9,10};
    EXPECT_EQ(true, hayInterseccion(s,t));
}
TEST(hayInterseccion, existeIntersecion_2){
    vector<int> s = {0,1,2,4,5,6,7,8};
    vector<int> t = {3,8,9,10};
    EXPECT_EQ(true, hayInterseccion(s,t));
}
TEST(hayInterseccion, existeIntersecion_3){
    vector<int> s = {0,1,2,4,5,6,7};
    vector<int> t = {3,7,8,9,10};
    EXPECT_EQ(true, hayInterseccion(s,t));
}
TEST(hayInterseccion, noExisteIntersecion){
    vector<int> s = {0,1,2,4,5,6,7};
    vector<int> t = {3,8,9,10};
    EXPECT_EQ(false, hayInterseccion(s,t));
}


//2do parcial 2020
TEST(cuantosEnComun, todosEnComun){
    vector<int> A = {1,2,6};
    vector<int> B = {0,1,2,3,4,5,6,7,15};
    EXPECT_EQ(3, cuantosEnComun(A,B));
}
TEST(cuantosEnComun, algunosEnComun_AMasGrande){
    vector<int> A = {1,2,20};
    vector<int> B = {0,1,2,3,4,5,6,7,15};
    EXPECT_EQ(2, cuantosEnComun(A,B));
}
TEST(cuantosEnComun, algunosEnComun_AMasChico){
    vector<int> A = {0,2,20};
    vector<int> B = {1,2,3,4,5,6,7,15,20};
    EXPECT_EQ(2, cuantosEnComun(A,B));
}
TEST(cuantosEnComun, ningunoEnComun){
    vector<int> A = {50,55,60};
    vector<int> B = {1,2,3,4,5,6,7,15,20};
    EXPECT_EQ(0, cuantosEnComun(A,B));
}
