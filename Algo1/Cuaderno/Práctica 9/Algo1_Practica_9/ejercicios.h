#include <string>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

void swap (vector<int> &v, int a, int b);
bool esDeCuentas (vector<int> v);string ordenarString (string palabra);
void cocktailSort (vector<int> & v);
void cocktailShakerSort(vector<int> & a);
void bingoSort (vector<int> & v);
void dosMitades(vector<int> &a);
string ordenarString (string palabra);
vector<int> encontrarKMasCercanos (vector<int> v, int e, int k);
void bubbleSort (vector<int> &v);

//repaso 2do parcial
bool hayInterseccion(vector<int> s, vector<int> t);
int cuantosEnComun (vector<int> A, vector<int> B);
