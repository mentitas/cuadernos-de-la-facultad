// Ejercicio 3.
// 
// Varias listas de enteros no nulos pueden combinarse para formar una sola lista de enteros, usando al
// 0 como separador. Por ejemplo, las listas “chicas” [1, 2, 3], [4, 5] y [6, 7, 8, 9] se combinan para
// formar la lista “grande” [1, 2, 3, 0, 4, 5, 0, 6, 7, 8, 9]. Dada una posición k de la lista grande, se
// quieren recuperar dos ı́ndices: un ı́ndice i que indica a cuál de las listas chicas corresponde esa
// posición, y otro ı́ndice j que indica cuál es el ı́ndice dentro de la lista chica.
// 
// Como parte de la solución a este problema se cuenta con el siguiente programa S en SmallLang y la
// siguiente especificación:


Implementación en SmallLang:

if (s[k] = 0) then
	i := i+1;
	j := 0;
else
	j := j+1;
endif



Especificación:

proc avanzar ( in s: seq⟨ℤ⟩, in k: ℤ, inout i: ℤ, inout j: ℤ){
	Pre  { k+1 < |s| ∧L posicionesCorrespondientes(s,k,i,j) }
	Post { posicionesCorrespondientes(s,k+1,i,j) }
}

pred posicionesCorrespondientes(s: seq⟨ℤ⟩, k: ℤ, i: ℤ, j: ℤ){
	(0 ≤ k < |s| ∧ 0 ≤ j ≤ k)
	∧L cantApariciones( subseq(s, 0, k-j)) = i
	∧ (k-j = 0 ∨L s[k-j-1] = 0)
	∧ cantApariciones(subseq(s,k-j,k), 0) = 0
}

aux cantApariciones(s: seq⟨T⟩, x: T) : ℤ = ∑\^|s| ₜ₌₀ if s[t]=x then 1 else 0 fi;


// a) Calcular la precondición más débil del programa S con respecto a la postcondición
//    de la especificación: wp(S; Post)

A ≡ i:=i+1 ; j:=0
B ≡ j:=j+1



wp(A, Post) ≡ wp(i:=i+1 ; j:=0, Post)
            ≡ wp(i:=i+1, wp(j:=0, Post))
            ≡ wp(i:=i+1, def(0) ∧L Postʲ₀)
            ≡ wp(i:=i+1, true ∧L posicionesCorrespondientes(s,k+1,i,0))
            ≡ wp(i:=i+1, posicionesCorrespondientes(s,k+1,i,0))
            ≡ def(i+1) ∧L posicionesCorrespondientes(s,k+1,i+1,0)
            ≡ true ∧L posicionesCorrespondientes(s,k+1,i+1,0)
            ≡ posicionesCorrespondientes(s,k+1,i+1,0)

 wp(B, Post) ≡ wp(j:=j+1, Post)
             ≡ def(j+1) ∧L Postʲⱼ₊₁
             ≡ true ∧L posicionesCorrespondientes(s,k+1,i,j+1)
             ≡ posicionesCorrespondientes(s,k+1,i,j+1)

wp(S, Post) ≡ wp(if S[k]=0 then A else B fi, Post)
            ≡ def(S[k]=0) ∧L ((S[k]=0 ∧ wp(A, Post)) ∨ (S[k]≠0 ∧ wp(B, Post)))
            ≡ 0 ≤ k < |s| ∧L (S[k]=0 ∧ posicionesCorrespondientes(s,k+1,i+1,0)) ∨ (S[k]≠0 ∧ posicionesCorrespondientes(s,k+1,i,j+1))


b) Demostrar que el programa es correcto con respecto a la especificación propuesta.

El programa es correcto si y sólo si se cumple que la tripla {Pre} S {Post} es válida,
y ésto sucede cuando se cumple que Pre ⟹ wp(S, Post)


Quiero demostrar que Pre ⟹ wp(S, Post)

Sabiendo que se cumple Pre:
	. k+1 < |s|
	. posicionesCorrespondientes(s,k,i,j)

    // *Observación: **
    // posicionesCorrespondientes(s,k,i,j) ⟹ 0 ≤ k < |s|


Quiero ver que se cumple wp(S, Post)
	. 0 ≤ k < |s|  → se cumple!
	. S[k]=0 ∧ posicionesCorrespondientes(s,k+1,i+1,0)) ∨ 
      S[k]≠0 ∧ posicionesCorrespondientes(s,k+1,i,j+1))


Evaluo los dos casos por separado:

* s[k]=0

Sabiendo que se cumple Pre:
    . s[k]=0
	. k+1 < |s|
	. posicionesCorrespondientes(s,k,i,j)
	  ≡ (0 ≤ k < |s| ∧ 0 ≤ j ≤ k)
	    ∧L cantApariciones(subseq(s,0,k-j), 0) = i
	    ∧  cantApariciones(subseq(s,k-j,k), 0) = 0
	    ∧  (k-j = 0 ∨L s[k-j-1] = 0)

Quiero ver que se cumple:
	. posicionesCorrespondientes(s,k+1,i+1,0))
	  ≡ (0 ≤ k+1 < |s| ∧ 0 ≤ 0 ≤ k+1)
	    ∧L cantApariciones(subseq(s,0,k+1),0) = i+1
	    ∧  cantApariciones(subseq(s,k+1,k+1), 0) = 0
	    ∧  (k+1 = 0 ∨L s[k] = 0)


	  ≡ 0 ≤ k+1 < |s|
	    ∧L cantApariciones(subseq(s,0,k+1),0) = i+1
	    ∧  cantApariciones(⟨⟩, 0) = 0
	    ∧  (k+1 = 0 ∨L true)

	  ≡ 0 ≤ k+1 < |s|
	    ∧L cantApariciones(subseq(s,0,k+1),0) = i+1
	    ∧  0 = 0
	    ∧  true

	  ≡ 0 ≤ k+1 < |s| ∧L cantApariciones(subseq(s,0,k+1),0) = i+1

	       cantApariciones(subseq(s,0,k+1),0) = ∑ᵏₜ₌₀ if s[t]=0 then 1 else 0 fi;
	       cantApariciones(subseq(s,0,k+1),0) = ∑ᵏ⁻¹ₜ₌₀ if s[t]=0 then 1 else 0 fi + (if s[k]=0 then 1 else 0 fi);
	       cantApariciones(subseq(s,0,k+1),0) = ∑ᵏ⁻¹ₜ₌₀ if s[t]=0 then 1 else 0 fi + 1;
	       cantApariciones(subseq(s,0,k+1),0) = cantApariciones(subseq(s,0,k),0) + 1;
	       cantApariciones(subseq(s,0,k+1),0) = cantApariciones(subseq(s,0,k-j),0) + 1; // j=0
	       cantApariciones(subseq(s,0,k+1),0) = i+1;    

	  ≡ 0 ≤ k+1 < |s|                                   → se cumple
	    ∧L cantApariciones(subseq(s,0,k+1),0) = i+1     → se cumple


Se cumple Pre ⟹ wp(S, Post) para S[k]=0








Evaluo el caso para S[k]≠0

Sabiendo que se cumple Pre:
    . s[k]≠0
	. k+1 < |s|
	. posicionesCorrespondientes(s,k,i,j)
	  ≡ (0 ≤ k < |s| ∧ 0 ≤ j ≤ k)
	    ∧L cantApariciones(subseq(s,0,k-j), 0) = i
	    ∧  cantApariciones(subseq(s,k-j,k), 0) = 0
	    ∧  (k-j = 0 ∨L s[k-j-1] = 0)

Quiero ver que se cumple wp(S, Post)
	. 0 ≤ k < |s|  → se cumple!
    . posicionesCorrespondientes(s,k+1,i,j+1))
	  ≡ (0 ≤ k+1 < |s| ∧ 0 ≤ j+1 ≤ k+1)
	    ∧L cantApariciones(subseq(s,0,k-j+1-1), 0) = i
	    ∧  cantApariciones(subseq(s,k-j+1-1,k+1), 0) = 0
	    ∧  (k-j+1-1 = 0 ∨L s[k-j-1+1-1] = 0)


	  ≡ . 0 ≤ k+1 < |s|                                       → se cumple
	    . 0 ≤ j+1 ≤ k+1  ≡ -1 ≤ j ≤ k                         → se cumple
	    . cantApariciones(subseq(s,0,k-j), 0) = i             → se cumple
	    . cantApariciones(subseq(s,k-j,k+1), 0) = 0
	    . (k-j = 0 ∨L s[k-j-1] = 0)                           → se cumple


cantApariciones(subseq(s,k-j,k+1),0) = ∑ᵏₜ₌ₖ₋ⱼ if s[t]=0 then 1 else 0 fi;
cantApariciones(subseq(s,k-j,k+1),0) = ∑ᵏ⁼¹ₜ₌ₖ₋ⱼ if s[t]=0 then 1 else 0 fi + (if s[k]=0 then 1 else 0 fi);
cantApariciones(subseq(s,k-j,k+1),0) = ∑ᵏ⁼¹ₜ₌ₖ₋ⱼ if s[t]=0 then 1 else 0 fi + 0;
cantApariciones(subseq(s,k-j,k+1),0) = ∑ᵏ⁼¹ₜ₌ₖ₋ⱼ if s[t]=0 then 1 else 0 fi;
cantApariciones(subseq(s,k-j,k+1),0) = cantApariciones(subseq(s,k-j,k),0);
cantApariciones(subseq(s,k-j,k+1),0) = i; → se cumple


Se cumple Pre ⟹ wp(S, Post) para S[k]≠0

Entonces dado que demostré que se cumple Pre ⟹ wp(S, Post) para todos los casos posibles,
puedo afirmar que el programa es correcto respecto a la especificación.
