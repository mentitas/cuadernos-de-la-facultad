/*************************(Ejercicio 2)*************************/
//a.
// I={0<=i<=|s| && res = sumatoria(desde j=0 hasta i)[if s[j]=j then 1 else 0 fi]}


/*************************(Ejercicio 3)*************************/

int sumatoria (vector<int> s){
	int res=0;
	for(int i=0; i<s.size(); i++){
		res+=s[i];
	}
	return res;
}

vector<int> izquierdaMasGrande (vector<int> s){
	int frontera = 0;
	int izquierda = 0;
	int derecha = sumatoria(s);
	for(int i=0; izquierda>derecha /*&& i<s.size()*/; i++){
		izquierda+=s[i];
		derecha  -=s[i];
		frontera++;
	}
	vector<int> res;
	for(int j=0; j<frontera; j++){
		res.push_back(v[j]);
	}
	return res;
}


/*************************(Ejercicio 4)*************************/

//opción 1
bool esDeFilasBatidas(vector<vector<int>> m){
	vector<vector<int>> mAux = m;
	vector<int> fila;
	for(int i=0; i<m.size(); i++){
		fila = mAux[i];
		ordenar(fila);
		mAux[i] = fila;
	}
	bool res = true;
	for(int j=0; j<m.size(); j++){
		res&=mAux[0]==mAux[j];
	}
	return res;
}

//opción 2
int apariciones(vector<int> v, int elem){
	int res=0;
	for(int i=0; i<v.size(); i++){
		if(v[i]==elem){
			res++;
		}
	}
	return res;
}
bool esDeFilasBatidas_2(vector<vector<int>> m){
	bool res=true;
	for(int i=0; i<m.size(); i++){
		for(int j=0; j<m[0].size(); j++){
			res&= apariciones(m[i], m[i][j]) == apariciones(m[0], m[i][j]);
		}
	}
	return res;

}