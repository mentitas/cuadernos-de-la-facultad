// Ejercicio 2.
// a) Escribir un programa en C++ que cumpla la especificación y que tenga un ciclo que verifique el invariante.

void sumarConsecutivos(vector<int> &s){
	int i = s.size()-1;
	while(i>0){
		s[i]=s[i]+s[i-1];
		i = i-1;
	}
	// for(int i=s.size()-1; i>0; i--){
	//  s[i]+=s[i-1];
	// }
}

// b) Escribir un programa en C++ que cumpla la especificación y cuyo un ciclo NO verifique el invariante. Es decir,
// que no se pueda demostrar su correctitud utilizando el invariante.

void sumarConsecutivos_2(vector<int> &s){
	int i=0;
	while(i<s.size()-1){
		s[i]=s[i]+s[i+1];
		i++;
	}
}




//Ejercicio 3
// Se dice que una matriz M de dimensión n × n es de zooms si se cumplen las dos condiciones siguientes:
// (∀i : Z)(∀j : Z)((0 ≤ i < n ∧ 0 ≤ j < n − 1) −→ L M [i][j] < M [i][j + 1])
// (∀i : Z)(1 ≤ i < n −→ L (∃j : Z)(0 ≤ j < n − 1 ∧ L M [i − 1][j] < M [i][0] ∧ M [i][n − 1] < M [i − 1][j + 1]))



// a) Describir en castellano qué significa la propiedad enunciada, y dar un ejemplo de
// matriz de zooms para n = 5.

//   Para cada fila i, el primer elemento de la fila debe ser menor a un elemento j de la fila superior (i+1), y el continuo de
//   ese elemento j (j+1) debe ser mayor al último elemento de la fila i. Además, las filas están ordenadas de forma creciente.
//   Todas las filas están en orden creciente y además, cada fila es el "zoom" de un intervalo de elementos de la fila superior

//   Por ejemplo, con n=5;    

// [-50, -25,  0, 25, 50] --> le hago zoom al intervalo (0,  25) 
// [  1,   2,  3,  4, 24] --> le hago zoom al intervalo (4,  24)
// [  5,   6,  7, 22, 23] --> le hago zoom al intervalo (7,  22)
// [  8,   9  10, 11, 21] --> le hago zoom al intervalo (11, 21)
// [ 12,  13, 14, 15, 16]


/* {{-50, -25,  0, 25, 50}, 
{  1,   2,  3,  4, 24},
{  5,   6,  7, 22, 23},
{  8,   9  10, 11, 21},
{ 12,  13, 14, 15, 16}} */
// b) Implementar en C++ la siguiente función:
// bool buscarEnZooms(vector<vector<int>>& M, int elem, int& fi, int& co)
// que toma una matriz de zooms y un elemento, y devuelve true en caso de que el elemento
// esté presente, y en ese caso además devuelve en los parámetros fi y co las coordenadas
// de ese elemento en la matriz (fila y columna, respectivamente). El algoritmo propuesto
// debe tener complejidad temporal de peor caso O(n).

bool buscarEnZooms(vector<vector<int>>& M, int elem, int& fi, int& co){
	fi = -1;
	co = -1;
	for(int f = 0; f<M.size(); f++){
		if(elem >= M[f][0] && elem <= M[f][M.size()-1]){
			fi++;
		}
	}
	for(int c=0; fi!=-1 && co=-1 && c<M.size(); c++){
		if(M[fi][c]==elem){
			co = c;
		}
	}
	return fi!=-1 && co!=-1;
}



// c) Justificar detalladamente por qué el algoritmo cumple con la complejidad pedida.

// El primer ciclo tiene complejidad O(n) porque en el peor caso itera n veces, y el segundo ciclo también tiene complejidad O(n), porque
// en el peor caso itera n veces. Dado que ambos ciclos son O(n), el programa es O(n)+O(n), que es O(2n) que es O(n).

