section .data

section .text

global Intercalar

; void Intercalar(char *A, char *B, char *vectorResultado, int dimension);
; A[rdi], B[rsi], Res[rdx], dimension[ecx]

Intercalar:
	push rbp
	mov rbp, rsp

    ; Para que preserve el orden, rdx debe arrancar desde atrás
    add rdx, rcx  ; [rdx] apunta al final de Res
    shr ecx, 4    ; ecx = dimension/16

    .ciclo:
        movdqa xmm0, [rdi]
        movdqa xmm1, [rsi]
        pshufb xmm0, xmm1

        sub rdx, 16
        movdqa [rdx], xmm0

        add rdi, 16
        add rsi, 16

    loop .ciclo

	pop rbp
	ret


