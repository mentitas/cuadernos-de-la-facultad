#include "ejercicio.h"

int main()
{
    int dimension = 32;
    char* A         = malloc(dimension*sizeof(char));
    char* B         = malloc(dimension*sizeof(char));
    char* Resultado = malloc(dimension*sizeof(char));

    for(int i=0; i<dimension; i++) A[i] = i%10;
    for(int i=0; i<dimension; i++) B[i] = dimension-i-1;
    for(int i=0; i<dimension; i++) Resultado[i] = 0;

    Intercalar(A, B, Resultado, dimension);

    //printf("Mi vector Resultado: %zu\n",sizeof(char));
    printf("Mi vector Resultado: " );
    for(int i=0; i<dimension; i++) printf("%i", Resultado[i]);

    return 0;

}
