#include "ejercicio.h"

int main()
{
    /* >>> InicializarVector

    int dimension = 16;
    short* A = malloc(dimension*sizeof(short));

    InicializarVector(A, 8, dimension);

    printf("Mi vector A: " );
    for(int i=0; i<dimension; i++) printf("%i", A[i]);
    */


    /* >>> DividirVectorPorPotenciaDeDos

    int dimension = 16;
    int *A = malloc(dimension*sizeof(int));
    for(int i=0; i<dimension; i++) A[i]=8;

    printf("(Antes) Mi vector A: ");
    for(int i=0; i<dimension; i++) printf("%i", A[i]);
    printf("\n");

    DividirVectorPorPotenciaDeDos(A, 1, dimension);

    printf("(Despues) Mi vector A: ");
    for(int i=0; i<dimension; i++) printf("%i", A[i]);
    */

    // FiltrarMayores

    int dimension = 16;
    short * A = malloc(dimension*sizeof(short));
    for(int i=0; i<8; i++) A[i]=5;
    for(int i=8; i<dimension; i++) A[i]=4;

    printf("(Antes) Mi vector A: ");
    for(int i=0; i<dimension; i++) printf("%i", A[i]);
    printf("\n");

    FiltrarMayores(A, 4, dimension);
    printf("(Después) Mi vector A: ");
    for(int i=0; i<dimension; i++) printf("%i", A[i]);
    return 0;

}
