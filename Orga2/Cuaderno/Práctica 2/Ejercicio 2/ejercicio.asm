section .data
vectorDeUnos:   times 16 dw 1
vectorDeCeros:  times 16 dw 0
variableQ:               dq 0

section .text

global SumarVectores
global InicializarVector
global DividirVectorPorPotenciaDeDos
global FiltrarMayores

; void InicializarVector(short *A, short valorInicial, int dimension)
; A[rdi], valorInicial[rsi], dimension[edx]
InicializarVector:
    push rbp
    mov rbp, rsp

    movdqu xmm1, [vectorDeUnos]

    mov ecx, esi ; ecx := valor inicial
    .multiplicar:
        paddw xmm2, xmm1
    loop .multiplicar

    mov ecx, edx ; ecx := dimension / 4
    shr ecx, 2
    xor rax, rax
    .mover:
        movdqu [rdi+rax], xmm2
        add rax, 8
    loop .mover

    pop rbp
    ret


;void SumarVectores(char *A, char *B, char *Resultado, int dimension);
; A[rdi], B[rsi], Resultado[rdx], dimension[ecx]

SumarVectores:
	push rbp
	mov rbp, rsp

    ; ecx = dimension
    shr ecx, 3; ecx := ecx/8
    xor rax, rax
    .ciclo:
        ;movdqu = mov dw / w / qw unaligned
        movdqu  xmm0, [rdi+rax] ; xmm0 = | a_1 | a_2 | ... | a_8 |
        movdqu  xmm1, [rsi+rax] ; xmm1 = | b_1 | b_2 | ... | b_8 |

        ;paddw = packed add words
        paddw xmm0, xmm1        ; xmm0 = | a_1+b_1 | a_2+b_2 | ... | a_8+b_8|}

        movdqu [rdx+rax], xmm0
        add rax, 8

    loop .ciclo

	pop rbp
	ret


;void DividirVectorPorPotenciaDeDos(int *A, int potencia, int dimension)
; A[rdi], potencia[esi], dimension[edx]
DividirVectorPorPotenciaDeDos:
    push rbp
    mov rbp, rsp

    ; Porque no puedo hacer "mov xmm2, esi", inventé una variable de 64bit
    mov [variableQ], esi    ; mov normal
    movsd xmm2, [variableQ] ; mov mágico

    mov ecx, edx ; ecx := dimension
    xor rax, rax
    .ciclo:
        movdqu xmm0, [rdi+rax]
        psrld xmm0, xmm2        ; Shift Packed Data Right Logical Word
        movdqu [rdi+rax], xmm0
        add rax, 16
    loop .ciclo

    pop rbp
    ret


; void FiltrarMayores(short *A, short umbral, int dimension)
; A[rdi], umbral[si], dimension[edx]
FiltrarMayores:
    push rbp
    mov rbp, rsp

    movdqu xmm1, [vectorDeUnos]
    movdqu xmm2, [vectorDeCeros]

    cmp esi, 0       ; Si el umbral es cero, no hace falta incrementar xmm2
    je .noMultiplico ; Entonces salto

    mov ecx, esi ; ecx := umbral
    .multiplicar:
        paddw xmm2, xmm1
    loop .multiplicar

    .noMultiplico:

    ; xmm2 := | umbral | umbral | ... | umbral | umbral |
    ; xmm0 := | A_1    | A_2    | ... | A_n-1  | A_n    |

    mov ecx, edx ; ecx := dimension
    shr ecx, 3
    xor rax, rax
    .ciclo:
        movdqu  xmm0, [rdi+rax]
        pcmpgtw xmm0, xmm2        ; packed compare greater than word
        movdqu [rdi+rax], xmm0
        add rax, 16
    loop .ciclo

    pop rbp
    ret