#include "ejercicio.h"

int main()
{
    // Tenemos una matriz de 4x4 ints
    int dimension = 4*4;
    int* M   = malloc(dimension*sizeof(int));
    int* Res = malloc(dimension*sizeof(int));

    for(int i=0; i<dimension; i++) M[i]   = i;
    for(int i=0; i<dimension; i++) Res[i] = 0;


    Sumatoria(M, Res, dimension);

    printf("Mi matriz M: \n" );
    for(int i=0; i<4; i++) printf("%i ", M[i]);
    printf("\n" );
    for(int i=4; i<8; i++) printf("%i ", M[i]);
    printf("\n" );
    for(int i=8; i<12; i++) printf("%i ", M[i]);
    printf("\n" );
    for(int i=12; i<16; i++) printf("%i ", M[i]);

    printf("\n\nMi matriz Res: \n" );
    for(int i=0; i<4; i++) printf("%i ", Res[i]);
    printf("\n" );
    for(int i=4; i<8; i++) printf("%i ", Res[i]);
    printf("\n" );
    for(int i=8; i<12; i++) printf("%i ", Res[i]);
    printf("\n" );
    for(int i=12; i<16; i++) printf("%i ", Res[i]);


    return 0;

}
