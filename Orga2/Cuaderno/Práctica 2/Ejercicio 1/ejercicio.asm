section .data

section .text

global SumarVectores

;void SumarVectores(char *A, char *B, char *Resultado, int dimension);
; A[rdi], B[rsi], Resultado[rdx], dimension[ecx]

SumarVectores:
	push rbp
	mov rbp, rsp

    ; ecx = dimension
    shr ecx, 3; ecx := ecx/8
    xor rax, rax
    .ciclo:
        ;movdqu = mov dw / w / qw unaligned
        movdqu  xmm0, [rdi+rax] ; xmm0 = | a_1 | a_2 | ... | a_8 |
        movdqu  xmm1, [rsi+rax] ; xmm1 = | b_1 | b_2 | ... | b_8 |

        ;paddw = packed add words
        paddw xmm0, xmm1        ; xmm0 = | a_1+b_1 | a_2+b_2 | ... | a_8+b_8|}

        movdqu [rdx+rax], xmm0
        add rax, 8

    loop .ciclo

	pop rbp
	ret


