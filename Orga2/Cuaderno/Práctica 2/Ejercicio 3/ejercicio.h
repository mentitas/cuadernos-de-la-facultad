#ifndef EJERCICIO
#define EJERCICIO

#include <stdio.h>
#include <stdlib.h>

void MultiplicarVectores(short *A, short *B, int *Res, int dimension);
int ProductoInterno(short *A, short *B, int dimension);
#endif