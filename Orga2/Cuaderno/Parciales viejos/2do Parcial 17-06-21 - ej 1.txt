Ejercicio 1
Construir un conjunto de segmentos y un mapa de paginación (un solo directorio y varias tablas de página),
tal que las siguientes instrucciones sean válidas:

Lógica            | Lineal    	| Física     | Acción
0x0060:0x00123001 | 0x00123011	| 0x01123001 | Leer código
0x0060:0x88A94100 | 0x88A94110	| 0x00000110 | Ejecutar código
0x0030:0x00000000 | 0xF0000000	| 0x00000000 | Leer datos
0x0030:0x00399FFF | 0xF0399FFF	| 0x00000FFF | Escribir datos

Si no es posible completar alguna traducción, justificar, dejando clara cuál es la razón por la cual no es
posible realizar dicha traducción. Por simplicidad considerar que todas las traducciones corresponden a acciones
realizadas en nivel cero de exactamente 4 bytes


>>> Segmentación
En la GDT va a haber declarados al menos 2 segmentos: datos y código

GDT[0x0060] -> segmento de código
GDT[0x0030] -> segmento de datos

Sea una dirección lógica L = (seg_selector, offset), las direcciones lineales se calculan como:

	dire lineal = L.offset + base_address(L.seg_selector)

Como se que 0x0060:0x00123001 y 0x0060:0x88A94100 son direcciones en el mismo segmento, entonces su dirección lineal se calcula como

0x0060:0x00123001 -> seg_segmento = 0x0060, offset = 0x00123001
0x0060:0x88A94100 -> seg_segmento = 0x0060, offset = 0x88A94100

         dire lineal = L.offset   + base_address(L.seg_selector)
         0x00123011  = 0x00123001 + base_address(0x0060)
base_address(0x0060) = 0x00123011 - 0x00123001
base_address(0x0060) = 0x00000010

         dire lineal = L.offset   + base_address(L.seg_selector)
         0x88A94110  = 0x88A94100 + base_address(0x0060)
base_address(0x0060) = 0x88A94110 - 0x88A94100
base_address(0x0060) = 0x00000010

Entonces se que la base del segmento de código es 0x00000010.



Hago lo mismo para el segmento de datos

0x0030:0x00000000 -> seg_segmento = 0x0030, offset = 0x00000000
0x0030:0x00399FFF -> seg_segmento = 0x0030, offset = 0x00399FFF

         dire lineal = L.offset   + base_address(L.seg_selector)
         0xF0000000  = 0x00000000 + base_address(0x0030)
base_address(0x0030) = 0xF0000000 - 0x00000000
base_address(0x0030) = 0xF0000000

         dire lineal = L.offset   + base_address(L.seg_selector)
         0xF0399FFF  = 0x00399FFF + base_address(0x0030)
base_address(0x0030) = 0xF0399FFF - 0x00399FFF
base_address(0x0030) = 0xF0000000

Entonces se que la base del segmento de código es 0xF0000000


Como un selector de segmento se define como
Index : 13
TI    : 1
RPL   : 2

0x0030 = 0 b 0000 0000 0011 0000 -> Index =  0000000000110 = 0x6
0x0060 = 0 b 0000 0000 0110 0000 -> Index =  0000000001100 = 0xC




GDT[0x6] =
{	
	L            = 0              // L = 1 <-> estamos en 64 bits
	base_address = 0xF0000000
	AVL          = 1
	D/B          = 1              // 0 = 16 bit, 1 = 32 bit
	DPL          = 0              // porque el nivel de privilegio es 0
	Limit        = ???
	G            = 0
	P            = 1              // El segmento está presente
	S            = 1              // 0 = system, 1 = code or data
	Type         = 0010           // 1010 = Read/Write
}

GDT[0xC] =
{	
	L            = 0              // L = 1 <-> estamos en 64 bits
	base_address = 0x00000010
	AVL          = 1
	D/B          = 1              // 0 = 16 bit, 1 = 32 bit
	DPL          = 0              // porque el nivel de privilegio es 0
	Limit        = ???
	G            = 0
	P            = 1              // El segmento está presente
	S            = 1              // 0 = system, 1 = code or data
	Type         = 1010           // 1010 = Execute/Read
}



>>> Paginación

Dirección Lineal: 

0x00123011 = 0b (0000 0000 00)(01 0010 0011)(0000 0001 0001)  
					0    0      1    2    3     0    1    1

					-> Dir    : 0x0                                 <- Descartada!!!
					-> Table  : 0001 0010 0011  = 0x123
					-> Offset : 0x011


0x88A94110 = 0b (1000 1000 10)(10 1001 0100)(0001 0001 0000)
                    8    8     10    9    4     1    1    0 

                    -> Dir    : 0b 0010 0010 0010 = 0x222
                    -> Table  : 0b 0010 1001 0100 = 0x294
                    -> Offset : 0x110


0xF0000000 = 0b (1111 0000 00)(00 0000 0000)(0000 0000 0000)
                    F    0      0    0    0     0    0    0

                    -> Dir    : 0b 0011 1100 0000 = 0x3C0
                    -> Table  : 0x0
                    -> Offset : 0x0


0xF0399FFF = 0b (1111 0000 00)(11 1001 1001)(1111 1111 1111)
                    F    0      3    9    9     F    F    F

                    -> Dir    : 0b 0011 1100 0000 = 0x3C0
                    -> Table  : 0x399
                    -> Offset : 0xFFF



Se que la dirección lineal 0x00123011 es mapeada a la dirección física 0x01123001. 0x00123011 tiene un offset de 0x11.
Entonces como los 20 bits mas significativos de la dirección física son la base de la página, la base de la página de la dirección
física 0x01123001 es 0x01123. Pero dirección_fisica = dirección_base + offset, y eso no se está cumpliendo >:(
	-> Descarto la primer traducción.

El offset de la cuarta traducción es 0xFFF. Queremos mapearlo a la dirección física 0x00000FFF.
La base de la página es 0x00000, y termina en 0x01000. Si mi offset es 0xFFF, entonces tengo 1 byte de espacio para operar (y yo
quiero hacer operaciones de 4 bytes!), entonces voy a reservar la página siguiente para usar los 3 bytes restantes



Entonces sea DIR un page directory, defino las entradas:

(las direcciones están bien DIR tiene 1024 entradas)

Directorio de páginas
Índice | Page table entry | U/S | R/W | P | 
0x222  | &PT1 >> 12       | 0   |  0  | 1 | (Ejecutar código)
0x3C0  | &PT2 >> 12       | 0   |  1  | 1 | (Leer y escribir datos)

PT1
Índice | Base_address | U/S | R/W | P
0x294  | 0x00000      | 0   | 0   | 1

PT2 
Índice | Base_address | U/S | R/W | P | 
0x000  | 0x00000      | 0   | 1   | 1 |  
0x399  | 0x00000      | 0   | 1   | 1 | <- si solo quiero leer datos, no deberia cambiarle los permisos?
0x39A  | 0x00000      | 0   | 1   | 1 | <- Reservo una página extra 