section .data
A: dd 0x3892, 0xF145, 0xDEDA, 0xA163
B: dd 0x532F, 0x1768, 0xE234, 0x94BA

section .text

global Sumar

; void Sumar(int *A, int *B, int *Resultado, int dimension);
; A[rdi], B[rsi], Resultado[rdx], dimension[ecx]

Sumar:
	push rbp
	mov rbp, rsp

    movdqu xmm0, [rdi]
    movdqa xmm1, [rsi]

    ; Add packed integers (CON OVERFLOW)
    ; paddw xmm0, xmm1

    ; Add packed signed integers with saturation
    ; paddsw xmm0, xmm1

    ; Add packed unsigned integers with unsigned saturation
     paddusw xmm0, xmm1

    movdqa [rdx], xmm0





	pop rbp
	ret


