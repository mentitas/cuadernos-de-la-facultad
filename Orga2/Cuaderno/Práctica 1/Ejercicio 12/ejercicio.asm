section .data

section .text

global borrarTerminadasEn
global stringTerminaEn

; int stringTerminaEn(char* string, char c)
; string[rdi], char[sil]

stringTerminaEn:
	push rbp
	mov rbp, rsp

    xor rax, rax     ; en rax guardo la longitud

    .buscoFinal:
        mov dl, byte [rdi]
        cmp dl, 0
        je .salgoCiclo
        inc rax
        inc rdi
        jmp .buscoFinal

    .salgoCiclo:
        sub rdi, 1
        mov dl, byte [rdi]

        cmp dl, sil
        je .sonIguales

    .sonDistintos:
        mov rax, 0
        jmp .fin

    .sonIguales:
        mov rax, 1

    .fin:
        pop rbp
        ret


; void borrarTerminadasEn(node** l, char c);
; l[rdi], c[sil]
; Cada nodo ocupa 24 bytes
borrarTerminadasEn:
	push rbp
	mov rbp, rsp

    xor r8, r8 ;r8 = iterador

    ; rdi     =  l
    ; [rdi]   = *l
    ; [[rdi]] = **l = nodoA

    mov ecx, 1
    .ultraCiclo:
        mov r9,  [rdi+r8]
        ; [r9]    = nodo->next
        ; [r9+8]  = nodo->prev
        ; [r9+16] = nodo->string
        mov r10, [r9+16]  ; string = [r10]

        ; Veo si el string termina en C
        push rsi
        push rdi
        mov rdi, r10
        call stringTerminaEn
        pop rdi
        pop rsi

        cmp rax, 1
        jne .noBorroNodo

        .borroNodo:


        .noBorroNodo:
        ; loopeo al siguiente nodo
        mov r10, [r9]
        ;mov r9, r10
        add r8, 24

    loop .ultraCiclo

    .fin:
	pop rbp
	ret


