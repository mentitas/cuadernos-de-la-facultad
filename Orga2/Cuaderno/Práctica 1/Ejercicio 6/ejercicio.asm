extern malloc

section .text
global cesar_asm

;char* cesar_asm(char* s, int x);
; s[rdi], x[rsi] = x[sil]
cesar_asm:
	push rbp
	mov rbp, rsp

    ; s[r10]
	mov r10, rdi

	;x[r8b]
	mov r8b, sil

    xor rax, rax

    ; longitud[r12]
	xor r12, r12
	.calculoLength:
	    mov dl, [rdi] ; dl es el bit actual que estoy chequeando
	    cmp dl, 0     ; si dl vale 0 es porque ya recorrí todo el string
	    je .exit
	    inc r12       ; sino incremento la longitud
	    inc rdi       ; e incremento el puntero
	    jmp .calculoLength

    .exit:

    ; malloqueo un espacio de tamaño (longitud * sizeof(char))
    mov rdi, r12
    call malloc ; rax := ptr al espacio malloqueado

    ; [rax]   apunta al primer byte
    ; [rax+1] apunta al segundo byte

    ;x[r8b]
    ;num[dl]
    ;ptr para iterar res[r14]
    ;ptr para iterar s[r10]
    mov r14, rax

    mov rcx, r12
    .ciclo:
        mov dl, [r10]            ; num := s[i]
        add dl, r8b              ; num += x

        cmp dl, 90               ; if (num < 90):
        jl .todoJoya             ;    // no le resto nada
                                 ; else:
        sub dl, 26               ;    num = num - 26

        .todoJoya:
        mov [r14], byte dl       ; res[i] := num
        inc r14                  ; incremento ptr a res
        inc r10                  ; incremento ptr a s
    loop .ciclo

    mov [r14], byte 0            ; agrego 0 al final de res para indicar que termina el string

	pop rbp
	ret

section .data