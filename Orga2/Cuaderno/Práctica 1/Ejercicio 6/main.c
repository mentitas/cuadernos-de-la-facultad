#include "ejercicio.h"

int main()
{

    char* res1 = cesar_c("CALABAZA", 7);
    printf("%s %s\n", "cesar_c = ", res1);
    free(res1);

    char* res2 = cesar_asm("CALABAZA", 7);
    printf("%s %s\n", "cesar_asm = ", res2);
    free(res2);

    return 0;
}
