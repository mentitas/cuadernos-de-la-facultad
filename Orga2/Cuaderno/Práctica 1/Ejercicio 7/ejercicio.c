#include "ejercicio.h"

// Funciona bien unicamente cuando s es un string en mayúscula
int prefijo_de_c(char* s, char* t){

    // "longitud" es la longitud del string más corto
    int longitud = 0;
    for(int i=0; s[i] != 0 && t[i] != 0; i++){
        longitud++;
    }

    int res = 0;
    while (res < longitud && s[res] == t[res]){
        res++;
    }

    return res;
}