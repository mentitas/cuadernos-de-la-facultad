#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

#include "ejs.h"

int main (void){
	/* Acá pueden realizar sus propias pruebas */
    str_array_t* ptr = strArrayNew(10);

    printf("sizeof(str_array_t): %zu\n\n", sizeof (str_array_t));

    printf("strArrayGetSize: %i\n\n", strArrayGetSize(ptr));

    printf("ptr->size: %i\n", ptr->size);
    printf("ptr->capacity: %i\n\n", ptr->capacity);

    char* data = malloc(sizeof(char*));
    data = "holi";
    printf("data: %s\n", data);

    //ptr->data[0] = data;
    strArrayAddLast(ptr, data);

    printf("ptr->data[ptr->size-1]: %s\n", ptr->data[ptr->size-1]);
    printf("strArrayGetSize: %i\n\n", strArrayGetSize(ptr));

    return 0;
}


