--Trabajo Práctico 2, Florencia Rosenzuaig

-------------------------(Ejercicio 1.a)------------------------- 
-- a. sumaDeDivisoresPropios :: Int -> Int
--    que calcula los divisores propios positivos y los suma. 

esDivisor :: Int -> Int -> Bool
esDivisor n d = mod n d == 0

sumaDeDivisoresPropiosHasta :: Int -> Int -> Int
sumaDeDivisoresPropiosHasta _ 0 = 0
sumaDeDivisoresPropiosHasta n d
 | esDivisor n d = d + sumaDeDivisoresPropiosHasta n (d-1)
 | otherwise     =     sumaDeDivisoresPropiosHasta n (d-1)

sumaDeDivisoresPropios :: Int -> Int 
sumaDeDivisoresPropios 0 = 0
sumaDeDivisoresPropios n = sumaDeDivisoresPropiosHasta n (div n 2)




-------------------------(Ejercicio 1.b)-------------------------
-- b. esPerfecto :: Int -> Bool
--    que determina si un número es perfecto.

esPerfecto :: Int -> Bool
esPerfecto 0 = False
esPerfecto n = n == sumaDeDivisoresPropios n





-------------------------(Ejercicio 2.a)-------------------------
-- a. listaAlicuotaDeNDeLargo :: Int -> Int -> [Int]
--    de modo tal que listaAlicuotaDeNDeLargo k n calcule los primeros
--    k elementos de la lista alícuota de n.
 
listaAlicuotaDeNDeLargo :: Int -> Int -> [Int]
listaAlicuotaDeNDeLargo 0 _ = []
listaAlicuotaDeNDeLargo k n = n : (listaAlicuotaDeNDeLargo (k-1) (sumaDeDivisoresPropios n))





-------------------------(Ejercicio 2.b)-------------------------
-- b. sonSociables :: [Int] -> Bool
--    que determine si la lista especificada corresponde a un club.
 
sonSociables :: [Int] -> Bool
sonSociables c = elUltimoEsElPrimero c && sonDistintos c && esAlicuota c

sonSociables_YaEsAlicuota :: [Int] -> Bool           -- La voy a usar después
sonSociables_YaEsAlicuota c = elUltimoEsElPrimero c && sonDistintos c

ultimo :: [Int] -> Int
ultimo (a:b:cs) = ultimo (b:cs)
ultimo (a:cs)   = a

pertenece :: Int -> [Int] -> Bool
pertenece n [] = False   
pertenece n (x:xs)
 | n == x    = True
 | otherwise = pertenece n xs


--Devuelve si la suma de los divisores del ultimo elemento es igual al primero.
elUltimoEsElPrimero :: [Int] -> Bool        
elUltimoEsElPrimero c = (head c) == (sumaDeDivisoresPropios (ultimo c))


--Devuelve si todos los elementos de la lista son distintos.
sonDistintos :: [Int] -> Bool
sonDistintos [] = True
sonDistintos (x:xs)
 | pertenece x xs = False
 | otherwise      = sonDistintos xs


--Devuelve si la suma de los divisores del elemento k es igual al elemento k+1  
esAlicuota :: [Int] -> Bool           
esAlicuota (a:b:cs) = (sumaDeDivisoresPropios a == b) && esAlicuota (b:cs)
esAlicuota (a:cs)   = True







-------------------------(Ejercicio 3.a)-------------------------
-- a. minimosDeKClubesMenoresQue :: Int -> Int -> [Int]
--    que dada una longitud k y una cota c, devuelva una lista de elementos
--    menores o iguales a c tales que sean el elemento mínimo de un club de largo k.


elPrimeroEsElMinimo :: [Int] -> Bool
elPrimeroEsElMinimo (a:b:cs) = (a < b) && (elPrimeroEsElMinimo (a:cs))
elPrimeroEsElMinimo (a:cs)   = True


minimosDeKClubesMenoresQueDesde :: Int -> Int -> Int -> [Int]
minimosDeKClubesMenoresQueDesde k c i
 | c < i = []
 | esSociable && esMinimo = i : minimosDeKClubesMenoresQueDesde k c (i+1)
 | otherwise              =     minimosDeKClubesMenoresQueDesde k c (i+1)
 where listaAlicuota = listaAlicuotaDeNDeLargo k i
       esSociable    = sonSociables_YaEsAlicuota (listaAlicuota)
       esMinimo      = elPrimeroEsElMinimo       (listaAlicuota)


minimosDeKClubesMenoresQue :: Int -> Int -> [Int]
minimosDeKClubesMenoresQue k c = minimosDeKClubesMenoresQueDesde k c 1





-------------------------(Ejercicio 3.b)-------------------------
-- b. listaDeNClubesConNrosMenoresQue :: Int -> Int -> [[Int]]
--    una función tal que listaDeNClubesConNrosMenoresQue n k devuelva una
--    lista de todos los clubes con n miembros cuyos elementos son todos
--    menores que k



--Dada una longitud k y una lista con minimos, devuelve una lista con las listas alicuotas de longitud k
--que empiezan en dichos minimos.

listaDeNClubesSabiendoMinimos :: Int -> [Int] -> [[Int]]
listaDeNClubesSabiendoMinimos k []     = []
listaDeNClubesSabiendoMinimos k (m:ms) = (listaAlicuotaDeNDeLargo k m) : (listaDeNClubesSabiendoMinimos k ms)



--Devuelve todas las listas cuyos elementos son menores que el entero
listasConNrosMenoresQue :: [[Int]] -> Int -> [[Int]]
listasConNrosMenoresQue []       _ = []
listasConNrosMenoresQue (xs:xss) n
 | xs `esMenorQue` n = xs : listasConNrosMenoresQue xss n
 | otherwise         =      listasConNrosMenoresQue xss n



--Devuelve si todos los elementos de la lista son menores que el entero
esMenorQue :: [Int] -> Int -> Bool
esMenorQue []     _ = True
esMenorQue (x:xs) n = (n > x) && (esMenorQue xs n)



listaDeNClubesConNrosMenoresQue :: Int -> Int -> [[Int]] 
listaDeNClubesConNrosMenoresQue n k = listasConNrosMenoresQue (listaDeNClubes) k
 where minimos        = minimosDeKClubesMenoresQue n (k-1)
       listaDeNClubes = listaDeNClubesSabiendoMinimos n minimos 

--n=longitud, k=cota superior

