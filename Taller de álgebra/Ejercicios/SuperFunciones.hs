module SuperFunciones
where
     {- | Documentación

    Las funciones que hay en éste módulo son:

     - superpertenece 
     - superagregar
     - superunion
     -}


     indices x = x
    -- esta función no hace nada, pero su doc devuelve el índice


     superpertenece :: Eq a => a -> [a] -> Bool
     superpertenece n []     = False
     superpertenece n (x:xs) = (n == x) || superpertenece n xs

     superagregar :: Eq a => a -> [a] -> [a]
     superagregar n l | superpertenece n l = l
                      | otherwise          = n : l

     superunion :: Eq a => [a] -> [a] -> [a]
     superunion [] b     = b
     superunion (a:as) b = superunion as (superagregar a b)