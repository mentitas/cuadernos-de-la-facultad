module FuncionesClase03
where
     {- | Documentación

    Las funciones que hay en éste módulo son:

     - factorial
     - 
     - 
     - 
     - 
     - 
     -
     -}


     indice3 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


     {- |Documentación 

     - Nombre: factorial
     - Declaración: Int -> Int
     - Descripción: Recibe un valor n y devuelve el factorial de n
     - Parámetros: Int: n
     - Retorna: Int: el factorial de n
     -}

     factorial :: Int -> Int
     factorial n | n == 0 = 1
                 | n > 0 = n * factorial (n-1)

     {- |Documentación 

     - Nombre: factorial
     - Declaración: Int -> Int
     - Descripción: Recibe un valor n y devuelve el enésimo
     número de fibonacci
     - Parámetros: Int: n
     - Retorna: Int: el enésimo número de fibonacci
     -}
     
     fib :: Int -> Int
     fib 0 = 0 
     fib 1 = 1
     fib n | n > 0 = (fib (n-1) + fib (n-2))
           | otherwise = undefined

