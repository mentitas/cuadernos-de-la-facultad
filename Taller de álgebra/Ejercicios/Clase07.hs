type Set a = [a]


vacio1 :: [Int]
vacio1 = []

--clase06
pertenece1 :: Int -> [Int] -> Bool
pertenece1 n a | a == [] = False
               | head a == n = True
               | otherwise = pertenece1 n (tail a)

agregar1 :: Int -> [Int] -> [Int]
agregar1 n c | pertenece1 n c == True = c
             | otherwise = (n:c)

vacio :: Set Int
vacio = []

--clase06
pertenece :: Int -> Set Int -> Bool
pertenece n a | a == [] = False
              | head a == n = True
              | otherwise = pertenece n (tail a)

agregar :: Int -> Set Int -> [Int]
agregar n c | pertenece n c == True = c
            | otherwise = (n:c)


incluido :: Set Int -> Set Int -> Bool
incluido [] c = True
incluido (x:xs) c = pertenece x c && incluido xs c


iguales :: Set Int -> Set Int -> Bool
iguales c1 c2 = incluido c1 c2 && incluido c2 c1


{- Ejercicio 1: union :: Set Int -> Set Int -> Set Int 
que dado dos conjuntos, devuelve la unión entre ellos. -}



{- |Documentación:

    - Nombre: union 
    - Declaración: Set Int -> Set Int -> Set Int
    - Descripción: Dadas dos listas (c1 y c2) (conjuntos), devuelve la
    unión de las dos listas.
    - Parámetros: Set Int: c1 y c2, conjuntos
    - Retorna: Set Int: la unión de c1 y c2
    - Necesita: agregar
    -}


union :: Set Int -> Set Int -> Set Int
union []     c = c
union (x:xs) c = union xs (agregar x c)







{- Ejercicio 2 : intereseccion :: Set Int -> Set Int -> Set Int 
que dado dos conjuntos, devuelve la interesección entre ellos. -}

{- |Documentación:

    - Nombre: interseccion 
    - Declaración: Set Int -> Set Int -> Set Int
    - Descripción: Dadas dos listas (c1 y c2) (conjuntos), devuelve la
    intersección de las dos listas. La función evalúa si el primer 
    elemento de c1 (la cabeza) pertenece en c2. Si pertenece, lo agrega 
    a c2 y evalua el próximo primer elemento (la cabeza de la cola.) Si
    no pertenece, evalua el proximo elemento. La función termina cuando 
    c1 es una lista vacía, es decir, no tiene más elementos para evaluar.
    - Parámetros: Set Int: c1 y c2, conjuntos
    - Retorna: Set Int: la intersección de c1 y c2
    - Necesita: pertenece
    -}

interseccion :: Set Int -> Set Int -> Set Int
interseccion [] _ = []
interseccion (x:xs) c
  | pertenece x c = x : (interseccion xs c)
  | otherwise     = interseccion xs c


interseccion' :: Set Int -> Set Int -> Set Int
interseccion' c1 c2 | c1 == []       = []
                    | pertenece x c2 = agregar x (interseccion' xs c2)
                    | otherwise      = interseccion' xs c2
                    where x  = head c1
                          xs = tail c1







{- Ejercicio 3: diferencia :: Set Int -> Set Int -> Set Int
que dado los conjuntos A y B, devuelve A \ B.-}

{- |Documentación:

    - Nombre: diferencia 
    - Declaración: Set Int -> Set Int -> Set Int
    - Descripción: Dadas dos listas (c1 y c2) (conjuntos), devuelve la
    diferencia entre las dos listas. La función evalúa si el primer 
    elemento de c1 (la cabeza) pertenece en c2. Si pertenece, evalua el
    próximo primer elemento (la cabeza de la cola.) Si no pertenece, 
    lo agrega a c2 y evalua el proximo elemento. La función termina 
    cuando c1 es una lista vacía, es decir, no tiene más elementos para
    evaluar.
    - Parámetros: Set Int: c1 y c2, conjuntos
    - Retorna: Set Int: la diferencia entre c1 y c2
    - Necesita: pertenece
    -}


diferencia :: Set Int -> Set Int -> Set Int
diferencia [] _ = []
diferencia (x:xs) c 
 | pertenece x c = diferencia xs c
 | otherwise     = x : (diferencia xs c)


{- |Documentación:

    - Nombre: quitar 
    - Declaración: Int -> Set Int -> Set Int
    - Descripción: Dado un número entero n y una lista de enteros
    (conjunto), si n pertenece al conjunto lo quita. La función se
    fija si n es la cabeza del conjunto. Si n es la cabeza, sigue
    evaluando con la cola. Si no es la cabeza, agrega la cabeza y
    sigue evaluando con la cola.
    - Parámetros: Int: n, el número
                  Set Int: el conjunto
    - Retorna: Set Int: el conjunto sin el elemento n
    -}


quitar :: Int -> Set Int -> Set Int
quitar n [] = []
quitar n (x:xs) 
 | n == x = quitar n xs
 | otherwise = x : (quitar n xs)


diferencia' :: Set Int -> Set Int -> Set Int
diferencia' c [] = c
diferencia' c (x:xs)
 | pertenece x c = quitar x (diferencia' c xs)
 | otherwise = diferencia' c xs









{- Ejercicio 4: diferenciaSimetrica :: Set Int -> Set Int -> Set Int 
que dado los conjuntos A y B, devuelve la diferencia simétrica, es 
decir, A (triangulito) B -}

{- |Documentación:

    - Nombre: diferenciasimetrica 
    - Declaración: Set Int -> Set Int -> Set Int
    - Descripción: Dados dos listas de enteros (conjuntos), la función calcula la 
    diferencia simétrica.
    - Parámetros: Set Int: c1 y c2
    - Retorna: Set Int: la diferencia simétrica entre c1 y c2
    - Necesita: diferencia, unión e intersección.
    -}



diferenciasimetrica :: Set Int -> Set Int -> Set Int
diferenciasimetrica c1 c2 = diferencia (union c1 c2) (interseccion c1 c2)







{- Ejercicio 1: partes :: Set Int -> Set (Set Int) 
que genere el conjunto de partes de un conjunto dado. Por ejemplo,
todos los subconjuntos del conjunto {1,2,3}. Ejemplo: 
partes [1,2,3] = [[], [1], [2], [3], [1, 2], [1,3], [2,3],[1,2,3]]-}



{-
partes []        = [[]]
partes [1]       = [[], [1]]
partes [1, 2]    = [[], [1], [2], [1,2]]
partes [1, 2, 3] = [[], [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3]]

partes [1,2,3] = agregaratodos 3 (partes([1,2]))

-}

-- Tambien le agrega n al conjunto vacio


--------------(Funciones auxiliares)--------------


{- |Documentación:

    - Nombre: subpertenece 
    - Declaración: Set Int -> Set (Set Int) -> Bool
    - Descripción: Dado un conjunto l y un conjunto de conjuntos c, la
    función determina si el conjunto l pertenece en c, es decir,
    si alguno de los elementos de c es igual a l. Si pertenece devuelve
    True, sino devuelve False.
    - Parámetros: Set Int: conjunto l
                  Set (Set Int): conjunto de conjuntos c
    - Retorna: Bool: si l pertenece o no en c
    - Necesita: iguales
    -}


subpertenece :: Set Int -> Set (Set Int) -> Bool
subpertenece n [] = False
subpertenece n (x:xs)
 | iguales n x = True
 | otherwise   = subpertenece n xs



{- |Documentación:

    - Nombre: subpertenecepm
    - Declaración: Set Int -> Set (Set Int) -> Bool
    - Descripción: Dado un conjunto l y un conjunto de conjuntos c, la
    función determina si el conjunto l pertenece en c, es decir,
    si alguno de los elementos de c es igual a l. Si pertenece devuelve
    True, sino devuelve False.
    - Parámetros: Set Int: conjunto l
                  Set (Set Int): conjunto de conjuntos c
    - Retorna: Bool: si l pertenece o no en c
    - Necesita: iguales
    -}

subpertenecepm :: Set Int -> Set (Set Int) -> Bool
subpertenecepm l [] = False
subpertenecepm l (cs:css) = iguales l cs || subpertenecepm l css 





{- |Documentación:

    - Nombre: subagregar
    - Declaración: Set Int -> Set (Set Int) -> Set (Set Int)
    - Descripción: Dado un conjunto l y un conjunto de conjuntos c, 
    la función agrega l a c, (si es que l no pertenece en c). Si l
    pertenece en c, devuelve c.
    - Parámetros: Set Int: conjunto l a agregar en c
                  Set (Set Int): conjunto de conjuntos c
    - Retorna: Set (Set Int): conjunto de conjuntos con l agregado
    - Necesita: subpertenece (que necesita iguales)
    -}

subagregar :: Set Int -> Set (Set Int) -> Set (Set Int)
subagregar c [] = [c]
subagregar c cs 
 | subpertenece c cs = cs
 | otherwise         = c : cs 


{- |Documentación:

    - Nombre: subunion
    - Declaración:  Set (Set Int) -> Set (Set Int) -> Set (Set Int)
    - Descripción: Dados dos conjuntos de conjuntos a y b, la función
    los une, y devuelve un conjunto de conjuntos con todos los elementos
    de a y b (sin repetir).
    - Parámetros: Set (Set Int): conjuntos de conjuntos a y b
    - Retorna: Set (Set Int): conjunto de conjuntos con elementos de a y b
    - Necesita: subagregar y subunion
    -}

subunion :: Set (Set Int) -> Set (Set Int) -> Set (Set Int)
subunion []     c = c
subunion (x:xs) c = subunion xs (subagregar x c)




--------------(Funciones principales)--------------


{- |Documentación:

    - Nombre: agregaratodos
    - Declaración:  Int -> Set (Set Int) -> Set (Set Int)
    - Descripción: Dado un entero n y un conjunto de conjuntos c, la
    función agrega n a todos los conjuntos de c. 
    - Parámetros: Int: n, el entero a agregar
                  Set (Set Int): c, conjunto de conjunto
    - Retorna: Set (Set Int): conjunto de conjuntos cuyos conjuntos
    contienen n.
    - Necesita: agregar
    -}

agregaratodos :: Int -> Set (Set Int) -> Set (Set Int)
agregaratodos n [] = [[]]
agregaratodos n (cs:css) = (agregar n cs) : (agregaratodos n css)


{- |Documentación:

    - Nombre: partes
    - Declaración:  Set Int -> Set (Set Int) 
    - Descripción: Dado un conjunto c, devuelve un conjunto
    con todas los posibles subconjuntos que se pueden formar con los
    elementos de c.
    - Parámetros: Set Int: el conjunto c
    - Retorna: Set (Set Int): el conjunto de partes de c
    - Necesita: subunion y agregaratodos
    -}


partes :: Set Int -> Set (Set Int)
partes [] = [[]]
partes (c:cs) = partescs `subunion` agregaratodos c partescs 
 where partescs = partes cs

--uso un where para que no calcule dos veces el mismo valor










{- Ejercicio 2: productocartesiano :: Set Int -> Set Int -> Set (Int, Int) 
que dados dos conjuntos genere todos los pares posibles (como pares 
de dos elementos) tomando el primer elemento del primer conjunto y el segundo
elemento del segundo conjunto.

Ejemplo> productocartesiano [1, 2, 3] [3, 4]
= [(1, 3), (2, 3), (3, 3), (1, 4), (2, 4), (3, 4)]                        

productocartesiano [1,2,3] [3] = [(1, 3), (2, 3), (3, 3)]
productocartesiano [1,2,3] [4] = [(1, 4), (2, 4), (3, 4)]

-}


--------------(Funciones auxiliares)--------------

{- |Documentación:

    - Nombre: pertenecedupla
    - Declaración: (Int, Int) -> Set (Int, Int) -> Bool
    - Descripción: Dada una dupla n y un conjunto de duplas x, la
    función determina si la dupla n pertenece a x, es decir,
    si alguno de los elementos de x es igual a n. Si pertenece devuelve
    True, sino devuelve False.
    - Parámetros: (Int, Int): n, la dupla
                  Set (Int, Int): x, el conjunto de duplas
    - Retorna: Bool: si n pertenece o no en x
    -}



pertenecedupla :: (Int, Int) -> Set (Int, Int) -> Bool
pertenecedupla n []     = False
pertenecedupla n (x:xs) = (n == x) || pertenecedupla n xs


{- |Documentación:

    - Nombre: agregardupla
    - Declaración: (Int, Int) -> Set (Int, Int) -> Set (Int, Int)
    - Descripción: Dada una dupla n y un conjunto de duplas c, 
    la función agrega n a c, (si es que n no pertenece en c). Si n
    pertenece en c, devuelve c.
    - Parámetros: (Int, Int): n, la dupla a agregar en c
                  Set (Int, Int): c, el conjunto de duplas
    - Retorna: Set (Int, Int): conjunto de conjuntos con n agregado
    - Necesita: pertenecedupla
    -}

agregardupla :: (Int, Int) -> Set (Int, Int) -> Set (Int, Int)
agregardupla n c 
 | pertenecedupla n c = c
 | otherwise         = n : c
 

{- |Documentación:

    - Nombre: uniondupla
    - Declaración: Set (Int, Int) -> Set (Int, Int) -> Set (Int, Int)
    - Descripción: Dados dos conjuntos de duplas a y b, la función
    los une, y devuelve un conjunto de conjuntos con todos los elementos
    de a y b (sin repetir).
    - Parámetros: Set (Int, Int): conjuntos de conjuntos a y b
    - Retorna: Set (Int, Int): conjunto de conjuntos con elementos de a y b
    - Necesita: agregardupla y uniondupla
    -}

uniondupla :: Set (Int, Int) -> Set (Int, Int) -> Set (Int, Int)
uniondupla [] b     = b
uniondupla (a:as) b = uniondupla as (agregardupla a b) 



--------------(Funciones principales)--------------

{- |Documentación:

    - Nombre: combinar
    - Declaración: Set Int -> Int -> Set (Int, Int)
    - Descripción: Dados un conjunto de enteros c y un entero n, la función
    devuelve todas las duplas posibles cuyo primer elemento es un elemento de
    c y su segundo elemento es b. 
    Por ejemplo, combinar [2,3] 5 = [(2,5), (3,5)]
    - Parámetros: Set Int: Conjunto de enteros c
                  Int: Entero n
    - Retorna: Set (Int, Int): Conjunto de duplas, cuyo primer elemento son elementos
    de c y su segundo elemento es n.
    - Necesita: agregardupla
    -}


combinar :: Set Int -> Int -> Set (Int, Int)
combinar [] n = []
combinar (c:cs) n = (c,n) `agregardupla` combinar cs n





{- |Documentación:

    - Nombre: productocartesiano
    - Declaración: Set Int -> Set Int -> Set (Int, Int)
    - Descripción: Dados dos conjuntos de enteros a y b, la función devuelve un
    conjunto cuyos elementos son duplas en las cuales el primer elemento es un 
    elemento de a, y el segundo elemento es un elemento de b.
    Por ejemplo, productocartesiano [1,2] [3,4] = [1,3),(1,4),(2,3),(2,4)]
    - Parámetros: Set Int: a y b, conjuntos de enteros
    - Retorna: Set (Int, Int): Conjunto de todas las duplas cuyo primer elemento
    es un elemento de a y cuyo segundo elemento es un elemento de b
    - Necesita: uniondupla
    -}

productocartesiano :: Set Int -> Set Int -> Set (Int, Int)
productocartesiano a [] = []
productocartesiano a (b:bs) = combinar a b `uniondupla` productocartesiano a bs


