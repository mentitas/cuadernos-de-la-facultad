import FuncionesClase07
import FuncionesClase03
import SuperFunciones

-- type Set a = [a]
-- no declaro Set porque viene con el modulo FuncionesClase07

{- Ejercicio 1:  Escribir una función que dados n, k e N 
tal que 0 ≤ k ≤ n, compute el combinatorio (n sobre k) -}



combinatorio :: Int -> Int -> Int
combinatorio n k = (factorial n) `div` ((factorial k) * (factorial (n-k)))



{- Ejercicio 2: Escribir una función que dados n, k e N tal que 0 ≤ k ≤ n,
compute el combinatorio (n sobre k). Hacerlo usando la igualdad
(n k) = ((n-1) k) + ((n-1) (k-1)) para 1 ≤ k ≤ n − 1 -}


combinatorio' :: Int -> Int -> Int
combinatorio' n 0 = 1
combinatorio' n k | n == k = 1 
                  | otherwise = (combinatorio' (n-1) k) + (combinatorio' (n-1) (k-1))





{- Ejercicio 3: variaciones :: Set Int -> Int -> Set [Int]
que dado un conjunto c y una longitud k genere toda las posibles
listas de longitud k a partir de elementos de c. -}


{- variaciones [4,7] 3 = [[4,4,4], [4,4,7], [4,7,4], [4,7,7], [7,4,4], [7,4,7],
[7,7,4], [7,7,7]] 

--agregarTODOSatodos [4,7] [[4],[7]]
[4] -> [4,4], [4,7]                   -> agregarUNOatodos 4 [[4], [7]]  
[7] -> [7,4], [7,7]                   -> agregarUNOatodos 7 [[4], [7]]

--agregarTODOSatodos [4,7] [[4,4], [4,7], [7,4], [7,7]]
[4,4] -> [4,4,4], [4,4,7]
[4,7] -> [4,7,4], [4,7,7]
[7,7] -> [7,7,4], [7,7,7]
[7,4] -> [7,4,4], [7,4,7]

agregarTODOSatodos 4 [[4], [7]] = [[4, 4], [4,7]]

agregarTODOSatodos [4, 7] [[4], [7]]
[4] -> [4,4], [4,7]
[7] -> [7,4], [7,7]          
-}

variaciones :: Set Int -> Int -> Set [Int]
variaciones c 0 = [[]]
variaciones c k = agregarTODOSatodos c (variaciones c (k-1))



--no puedo usar subunion porque subunion es para conjuntos, y estoy trabajando con
--listas de enteros.

--agrega todos los elementos de una lista a todas las listas de una lista de listas
agregarTODOSatodos :: Set Int -> Set [Int] -> Set [Int]
agregarTODOSatodos [] c     = []
agregarTODOSatodos (n:ns) c = (agregarUNOatodos n c) `superunion` (agregarTODOSatodos ns c)



--agrega un elemento int a todas las listas de una lista de listas
agregarUNOatodos :: Int -> Set [Int] -> Set [Int]
agregarUNOatodos n [] = []
agregarUNOatodos n (cs:css) = (n:cs) : (agregarUNOatodos n css)






{- Ejercicio 4: insertarEn :: [Int] -> Int -> Int -> [Int]
que dados una lista l, un número n y una posición i (contando desde 1)
devuelva una lista en donde se insertó n en la posición i de l
y los elementos siguientes corridos en una posición.
Ejemplo > insertarEn [1, 2, 3, 4, 5] 6 2  =  [1, 6, 2, 3, 4, 5]     -}

insertarEn :: [Int] -> Int -> Int -> [Int]
insertarEn l      n 1 = n : l
insertarEn (x:xs) n i = x : (insertarEn xs n (i-1)) 


{- Ejercicio 5: permutaciones :: Set Int -> Set [Int]
que dado un conjunto de enteros, genere todas las posibles permutaciones 
de los números del conjunto pasado por parámetro.
Ejemplo> permutaciones [1,2,3] = [[1,2,3], [1,3,2], [2,1,3],
[2,3,1], [3,1,2], [3,2,1]]

permutaciones [1]     = [1]
permutaciones [1,2]   = [1,2] [2,1]
permutaciones [1,2,3] = [[3,1,2], [1,3,2], [1,2,3], [3,2,1], [2,3,1], [2,1,3]]

insertarentodas 2 [1] = [[1,2], [2,1]]
insertarentodas 3 [1,2] = [[3,1,2], [1,3,2], [1,2,3]]
insertarentodas 3 [2,1] = [[3,2,1], [2,3,1], [2,1,3]]

-}

permutaciones :: Set Int -> Set [Int]
permutaciones []     = [[]]
permutaciones (c:cs) = insertartodaslasLISTAS c (permutaciones cs)

--inserta un int en todas las posiciones
insertartodaslasLISTAS :: Int -> Set [Int] -> Set [Int]
insertartodaslasLISTAS n []       = []
insertartodaslasLISTAS n (cs:css) = (insertartodaslasPOS n cs) `superunion` (insertartodaslasLISTAS n css)

insertartodaslasPOS :: Int -> Set Int -> Set [Int]
insertartodaslasPOS n l = insertartodaslasPOShasta n l (length l + 1)

insertartodaslasPOShasta :: Int -> Set Int -> Int -> Set [Int]
insertartodaslasPOShasta n l 0 = []
insertartodaslasPOShasta n l p = (insertarEn l n p) `superagregar` (insertartodaslasPOShasta n l (p-1)) 



{- Ejercicio 6:  bolitasEnCajas :: Int -> Int -> Set [Int]
Todas las formas de ubicar n bolitas numeradas en k cajas.
Ejemplo > bolitasEnCajas 2 3 [[1,1],[1,2],[1,3],[2,1],[2,2],
[2,3],[3,1],[3,2],[3,3]]
Notar que el elemento i de cada sublista representa el número de caja
donde fue a parar la bolita i                                    

bolitasEnCajas 2 1 = [1,1]
bolitasEnCajas 2 2 = [1,1], [1,2], [2,1], [2,2]        
bolitasEnCajas 2 3 = [1,1], [1,2], [2,1], [2,2], [2,3],[3,1],[3,2],[3,3]] 

variaciones [1] 2     = [1,1]
variaciones [1,2] 2   = [[1,2],[1,1],[2,2],[2,1]]
variaciones [1,2,3] 2 = [[1,3],[1,2],[1,1],[2,3],[2,2],[2,1],[3,3],[3,2],[3,1]]
-}

bolitasEnCajas :: Int -> Int -> Set [Int]
bolitasEnCajas n k = variaciones [1..k] n

{- Ejercicio 7: 
Todas las formas de ubicar n bolitas numeradas en k cajas tal que la
primera caja nunca esté vacía. 

En todas las listas tiene que haber un 1, es decir que hay aunque sea
una bolita en la caja uno.                                          -}



bolitasEnCajasPrimeraNoVacia :: Int -> Int -> Set [Int]
bolitasEnCajasPrimeraNoVacia n k = quitarListasSinUno (bolitasEnCajas n k)

quitarListasSinUno :: Set [Int] -> Set [Int]
quitarListasSinUno [] = []
quitarListasSinUno (cs:css)
 | pertenece 1 cs = cs:(quitarListasSinUno css)
 | otherwise      =     quitarListasSinUno css




{- Ejercicio 8: listasOrdenadas :: Set Int -> Int -> Set [Int]
Todas las listas ordenadas de k números distintos tomados del conjunto 
{1, ... , n}.

Ejemplo > listasOrdenadas 2 [1,2,3] = [[1,2],[1,3],[2,3]]       

variaciones [1,2,3] 2 = [[1,3],[1,2],[1,1],[2,3],[2,2],
[2,1],[3,3],[3,2],[3,1]] -}

listasOrdenadas :: Int -> Set Int -> Set [Int]
listasOrdenadas k ns = quitarDesordenadas (variaciones ns k)

quitarDesordenadas :: Set [Int] -> Set [Int]
quitarDesordenadas [] = []
quitarDesordenadas (x:xs)
 | estaOrdenada x = x : quitarDesordenadas xs
 | otherwise      =     quitarDesordenadas xs

estaOrdenada :: Set Int -> Bool
estaOrdenada (a:b:cs) = (a<b) && (estaOrdenada (b:cs))
estaOrdenada (a:cs)   = True




{- Ejercicio 9: 4 Todas las sucesiones de los caracteres 'a' y 'b' de
longitud n y m respectivamente.
Ejemplo > sucesionesAB 1 2 = ["abb", "bab", "bba"]}        

sucesionesAB 1 0 = ["a"]
sucesionesAB 1 1 = ["ba", "ab"]
sucesionesAB 1 2 = ["bba", "bba", "bab", "bab", "abb", "abb"]
                 = ["bba", "bab", "abb"]


sucesionesAB 0 0 = []
sucesionesAB 0 1 = ["b"]
sucesionesAB 1 0 = ["a"]
sucesionesAB 1 1 = ["ba", "ab"]
sucesionesAB 2 0 = ["aa"]
sucesionesAB 0 2 = ["bb"]
sucesionesAB 2 1 = ["aab", "aba", "baa"]
sucesionesAB 1 2 = ["bba", "bab", "abb"]
sucesionesAB 2 2 = ["aabb", "abab", "baab", "bbaa", "baba", "abba"]

sucesionesAB 2 2 = agregaralfinalLLB (sucesiones 2 1) `union` agregaralfinalLLA (sucesiones 1 2)
sucesionesAB 1 1 = agregaralfinalLLB (sucesiones 1 0) `union` agregaralfinalLLA (sucesiones 0 1)

-}

sucesionesAB :: Int -> Int -> Set [Char]
sucesionesAB n 0 = [formarstring 'a' n]
sucesionesAB 0 m = [formarstring 'b' m]
sucesionesAB n m = (agregaralfinalLL 'b' (sucesionesAB n (m-1)) ) `superunion` (agregaralfinalLL 'a' (sucesionesAB (n-1) m) )


--agrega un char al final de todos las las listas de una lista de listas de char
-- LL es por lista de listas
agregaralfinalLL :: Char -> Set [Char] -> Set [Char]
agregaralfinalLL a []       = []
agregaralfinalLL a (cs:css) = agregaralfinal a cs `superagregar` (agregaralfinalLL a css)


--agrega un char al final de una lista de char
agregaralfinal :: Char -> Set Char -> Set Char
agregaralfinal a []     = [a]
agregaralfinal a (c:cs) = c : (agregaralfinal a cs)


formarstring :: Char -> Int -> Set Char
formarstring a 0 = []
formarstring a n = a : (formarstring a (n-1))


{- Ejercicio 10:
Todas las sucesiones de 'a', 'b' y 'c' de longitud n, m y k respectivamente

sucesionesABC n m 0 = polisucesiones 'a' 'b' n m
sucesionesABC n 0 k = polisucesiones 'a' 'c' n k
sucesionesABC 0 m k = polisucesiones 'b' 'c' m k 

sucesionesABC 1 0 0 = ["a"]
sucesionesABC 0 0 1 = ["c"]
sucesionesABC 1 1 0 = ["ba", "ab"]
sucesionesABC 0 1 1 = ["bc", "cb"]
sucesionesABC 1 0 1 = ["ac", "ca"]
sucesionesABC 1 1 1 = ["bac", "abc", "bca", "cba", "acb", "cab"]

sucesionesABC n m k =               agregaralfinalLL 'c' (sucesionesABC n m (k-1)) 
                       `superunion` agregaralfinalLL 'a' (sucesionesABC (n-1) m k)
                       `superunion` agregaralfinalLL 'b' (sucesionesABC n (m-1) k)


-}


polisucesiones :: Char -> Char -> Int -> Int -> Set [Char]
polisucesiones a b n 0 = [formarstring a n]
polisucesiones a b 0 m = [formarstring b m]
polisucesiones a b n m = (agregaralfinalLL b (polisucesiones a b n (m-1)) ) `superunion` (agregaralfinalLL a (polisucesiones a b (n-1) m) )

sucesionesABC :: Int -> Int -> Int -> Set [Char]
sucesionesABC n m 0 = polisucesiones 'a' 'b' n m
sucesionesABC n 0 k = polisucesiones 'a' 'c' n k
sucesionesABC 0 m k = polisucesiones 'b' 'c' m k 
sucesionesABC n m k =               agregaralfinalLL 'c' (sucesionesABC n m (k-1)) 
                       `superunion` agregaralfinalLL 'a' (sucesionesABC (n-1) m k)
                       `superunion` agregaralfinalLL 'b' (sucesionesABC n (m-1) k)


{- Ejercicio 11: subconjuntos :: Set Int -> Int -> Set (Set Int)
que dados un conjunto de enteros y un entero k, genera todos los
subconjuntos de k elementos del conjunto pasado por parámetro.
Ejemplo > subjconjuntos [1,2,3] 2 = [[1, 2], [2, 3], [1, 3]]

listasOrdenadas 2 [1,2,3] = [[1, 2], [2, 3], [1, 3]]           -}

subconjuntos :: Set Int -> Int -> Set (Set Int)
subconjuntos cs n = listasOrdenadas n cs