type Complejo = (Float, Float)

-- Ejercicio 1: re :: Complejo -> Float

re :: Complejo -> Float
re (a, b) = a


-- Ejercicio 2: im :: Complejo -> Float

im :: Complejo -> Float
im (a, b) = b


-- Ejercicio 3: conjugado :: Complejo -> Complejo

conjugado :: Complejo -> Complejo
conjugado (a, b) = (a, -b)


-- Ejercicio 4: suma :: Complejo -> Complejo -> Complejo

suma :: Complejo -> Complejo -> Complejo
suma (a, b) (c, d) = (a+c, b+d)

-- Ejercicio 5: producto :: Complejo -> Complejo -> Complejo

producto :: Complejo -> Complejo -> Complejo 
producto (a, b) (c, d) = (a*c - b*d, a*d + b*c)


-- Ejercicio 6: inverso :: Complejo -> Complejo

inverso :: Complejo -> Complejo 
inverso (a, b) =  (a/c, -b/c)
 where c = (a**2 + b**2)

--para hacer potencias de floats hay que usar ** en vez de ^


-- Ejercicio 7: cociente :: Complejo -> Complejo -> Complejo

cociente :: Complejo -> Complejo -> Complejo
cociente a b = a `producto` (inverso b)


-- Ejercicio 8: potencia :: Complejo -> Int -> Complejo

potencia :: Complejo -> Int -> Complejo
potencia a 0 = (1, 0)
potencia a p = a `producto` (potencia a (p-1))


{- Ejericio 9 : solucionesCuadratica :: Float -> Float -> Float -> (Complejo, Complejo)
Dada una función cuadrática ax2 + bx + c con a, b, c ∈ R,
a /= 0, definir la función que tome los coeficientes a, b y c y
devuelve las dos raíces. En caso de haber una sola, devolverla dos veces. -}


solucionesCuadratica :: Float -> Float -> Float -> (Complejo, Complejo)
solucionesCuadratica a b c
 | det == 0  = ((-b/(2*a), 0), (-b/(2*a), 0))
 | det > 0   = (((-b+det')/(2*a), 0), ((-b-det')/(2*a), 0))
 | otherwise = ((-b/(2*a), det'/(2*a)), (-b/(2*a), -det'/(2*a)))
 where det  = (b**2 - 4*a*c)
       det' = sqrt (abs det)





-- Ejercicio 10: modulo :: Complejo -> Float 

modulo :: Complejo -> Float
modulo (a, b) = sqrt (a**2 + b**2)

-- Ejercicio 11: argumento :: Complejo -> Float

argumento :: Complejo -> Float
argumento (a, b)
 | (a<0)     = 1 + arg
 | (b<0)     = 2 + arg
 | otherwise = arg
 where arg = (atan (b/a))


{- Ejercicio 12: pasarACartesianas :: Float -> Float -> Complejo
que toma r ≥ 0 y θ ∈ [0, 2π) y devuelve el complejo z que tiene módulo r
y argumento θ -}

pasarACartesianas :: Float -> Float -> Complejo
pasarACartesianas r arg = (r*cos(arg), r*sin(arg))


{- Ejercicio 13: raizCuadrada :: Complejo -> (Complejo,Complejo)
Dado z ∈ C, devuelve los dos complejos w que satisfacen w2 = z -}
-- (mod w)^2 = mod z
-- (2*arg w) = arg z       

raizCuadrada :: Complejo -> (Complejo, Complejo)
raizCuadrada (a, b) = (pasarACartesianas r arg1, pasarACartesianas r arg2)
 where r = sqrt (modulo (a, b))
       arg1 = (argumento (a, b))/2
       arg2 = (argumento (a, b))/2 + pi

-- ¿ESTÁ BIEN HECHO?


{- Ejercicio 14: solucionesCuadraticaCoefComplejos ::
Complejo -> Complejo -> Complejo -> (Complejo,Complejo)
Dado un polinomio az^2 +  + bz + c, con a, b, c ∈ C, a 6= 0, implementar la función que
devuelve sus dos raices. -}

resta :: Complejo -> Complejo -> Complejo
resta (a, b) (c, d) = (a-c, b-d)

productoEscalar :: Float -> Complejo -> Complejo
productoEscalar n (a, b) = (n*a, n*b)

solucionesCuadraticaCoefComplejos :: Complejo -> Complejo -> Complejo -> (Complejo, Complejo)
solucionesCuadraticaCoefComplejos a b c = (caso1, caso2)
 where det            = ((b `potencia` 2) `suma`  ((-4) `productoEscalar` (a `producto` c)) )
       (raiz1, raiz2) = raizCuadrada (det)
       caso0          = ((-1) `productoEscalar` b) `cociente` (2 `productoEscalar` a)
       caso1          = (((-1) `productoEscalar` b) `suma`  raiz1) `cociente` (2 `productoEscalar` a)
       caso2          = (((-1) `productoEscalar` b) `resta` raiz1) `cociente` (2 `productoEscalar` a)

verificarCuadratica :: Complejo -> Complejo -> Complejo -> Complejo -> Complejo
verificarCuadratica x a b c = ((a `potencia` 2) `producto` x) `suma` (b `producto` x) `suma` c

--MEPA QUE NO ANDA

{- Ejercicio 15: raicesNesimas :: Int -> [Complejo]
que, dado n natural, devuelve la lista con las raíces n-ésimas de la
unidad. -}

-- argumentos son: 2kpi/n
-- g es una raiz primitiva

raicesNesimasConGenHasta :: Int -> Complejo -> Int -> [Complejo]
raicesNesimasConGenHasta n g 0 = [(1,0)]
raicesNesimasConGenHasta n g p = (potencia g p) : raicesNesimasConGenHasta n g (p-1)

raicesNesimas :: Int -> [Complejo]
raicesNesimas n = raicesNesimasConGenHasta n g n 
 where g = redondearComplejo (pasarACartesianas 1 (2*pi/nf))
       nf = fromIntegral n   --nf es n pero en float

--redondea a cero los números muy peques
redondear :: Float -> Float
redondear a 
 | abs a < (1e-6) = 0
 | otherwise      = a

redondearComplejo :: Complejo -> Complejo
redondearComplejo (a,b) = (redondear a, redondear b)


{- Ejercicio 16: potenciasRaizNesima :: Int -> Int -> [Complejo]
que, dados k y n enteros con 0 ≤ k < n, devuelve la lista con las
potencias 0, 1, ... , n − 1 de la raíz n-ésima asociada a k
siguiendo la fórmula de arriba. -}

--literalmente programé raizNesima con la misma idea, pero con k=1

potenciasRaizNesima :: Int -> Int -> [Complejo]
potenciasRaizNesima n k = raicesNesimasConGenHasta n g n 
 where g = redondearComplejo (pasarACartesianas 1 (2*pi*kf/nf))
       nf = fromIntegral n   --nf es n pero en float
       kf = fromIntegral k   --kf es k pero en float