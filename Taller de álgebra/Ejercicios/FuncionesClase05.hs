module FuncionesClase05
where
     {- | Documentación

    Las funciones que hay en éste módulo son:

     - 
     - 
     - 
     - 
     - 
     - 
     -
     -}


     indice3 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


     {- |Documentación 

     - Nombre: menordivisordesde
     - Declaración:
     - Descripción: 
     - Parámetros:
     - Retorna: 
     -}

     menordivisordesde :: Int -> Int -> Int
     menordivisordesde n m | n == m = m
                           | mod n m == 0 = m
                           | otherwise = menordivisordesde n (m + 1)

     {- |Documentación 

     - Nombre: menordivisor
     - Declaración: 
     - Descripción: 
     - Parámetros:
     - Retorna:
     -}
     
     menordivisor :: Int -> Int
     menordivisor n = menordivisordesde n 2 


     {- |Documentación 

     - Nombre: esprimo
     - Declaración: 
     - Descripción: 
     - Parámetros:
     - Retorna:
     -}
     
     esprimo :: Int -> Bool
     esprimo n | menordivisor n == n = True
               | otherwise = False
