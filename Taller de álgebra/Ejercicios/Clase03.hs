
fib :: Int -> Int
fib 0 = 0 
fib 1 = 1
fib n | n > 0 = (fib (n-1) + fib (n-2))
      | otherwise = undefined

fibPM :: Int -> Int
fibPM 0 = 0 
fibPM 1 = 1
fibPM n = (fib (n-1) + fib (n-2))

--intento hacer una funcion para averiguar si un numero es fib
esfib :: Int -> Int -> Bool
esfib n x | n < 0 || x < 0 = False
          | x > 100 = undefined
          | n == fib (x) = True
          | otherwise = esfib n (x-1)

parteenterat :: Float -> Int
parteenterat n = truncate n

parteentera :: Float -> Integer
parteentera n | n < 0 = undefined
              | n < 1 = 0
              | otherwise = 1 + parteentera (n-1)

factorial :: Int -> Int
factorial n | n == 0 = 1
            | n > 0 = n * factorial (n-1)



{-Si estuviese definida así, o como en factorial2 y le metés
un número negativo, entra en un loop infinito -}

factorial1 :: Int -> Int
factorial1 n 
  | n == 0 = 1
  | otherwise = n * factorial1 (n-1)

factorial2 :: Int -> Int
factorial2 0 = 1
factorial2 n = n* factorial2 (n-1)


{- si le ingresás un número impar, la resta va a pasar de largo el
cero, y va a seguir restando hasta menos infinito-}
esParMal :: Int -> Bool
esParMal n | n == 0 = True
           | otherwise = esParMal (n-2)

--Estas dos están bien
esPar1 :: Int -> Bool
esPar1 n | n == 0 = True
         | n == 1 = False
         | otherwise = esPar1 (n-2)

esPar2 :: Int -> Bool
esPar2 n | n == 0 = True
         | otherwise = not (esPar2 (n-1))


--Ejercicios
{- 1 Escribir una función para determinar si un número natural 
es múltiplo de 3. No está permitido utilizar mod ni div. -}

multiplodetres :: Int -> Bool
multiplodetres n | n == 0 = True
                 | n == 1 = False
                 | n == 2 = False
                 | n < 0 = multiplodetres (abs n)
                 | otherwise = multiplodetres (n - 3)


{- 2 Implementar la función sumaImpares :: Int -> Int que 
dado n e N sume los primeros n números impares. 
Ej: sumaImpares 3 -> 1+3+5 -> 9. -}

-- 1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31
-- 1 2 3 4 5  6  7  8  9 10 11 12 13 14 15 16
-- Para calcular el n numero impar hago (2*n -1)

sumaImpares :: Int -> Int
sumaImpares n | n == 0 = 0
              | n > 0 = (2*n - 1) + sumaImpares (n-1)
              | otherwise = undefined

{- Escribir una función medioFact que dado n 2 N 
calcula n!! = n (n − 2)(n − 4) · · · . Por ejemplo:
medioFact 10 10 ∗ 8 ∗ 6 ∗ 4 ∗ 2 3840.
medioFact 9 9 ∗ 7 ∗ 5 ∗ 3 ∗ 1 945. -}

medioFact :: Int -> Int
medioFact n | n == 0 = 1
            | n == 1 = 1
            | n > 0 = n*(medioFact (n-2))
            | otherwise = undefined


{- Escribir una función que determine la suma de dígitos 
de un número positivo. Para esta función pueden utilizar 
div y mod. -}
{- el último dígito de un número es (mod n 10), para sacarle
el último dígito a un número uso (div n 10) -}

sumadig :: Int -> Int
sumadig n | n < 10 = n
          | n < 0 = sumadig (abs n)
          | otherwise = (mod n 10) + sumadig (div n 10)

{- Implementar una función que determine si todos los dígitos
 de un número son iguales -}
--Funciones auxiliares: digitounidades y digitodecenas
soniguales :: Int -> Bool
soniguales n | n < 0 = soniguales (abs n) 
             | n < 10 = True
             | digitounidades n == digitodecenas n = soniguales (div n 10)
             | otherwise = False

digitounidades :: Int -> Int
digitounidades n | n < 0 = digitounidades (abs n)
                 | n < 10 = n
                 | otherwise = (mod n 10)

digitodecenas :: Int -> Int 
digitodecenas n | n < 0 = digitodecenas (abs n)
                | n < 10 = undefined
                | otherwise = mod (div n 10) 10