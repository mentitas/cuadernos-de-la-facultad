type Polinomio = [Float]
type Monomio = (Float, Int)

{- Ejercicio 1: limpiar :: [Float] -> Polinomio
que dada una lista cualquiera de números reales, le borra
los 0s iniciales si los hubiera (luego el resultado cumple
el invariante del tipo Polinomio). -}

limpiar :: [Float] -> Polinomio
limpiar (0:xs) = limpiar xs
limpiar p = p

{- Ejercicio 2: grado :: Polinomio -> Int
(en general se usa la convenci´on grado(0) = −∞, en la
implementación se puede dejar grado [] indefinido) -}

grado :: Polinomio -> Int
grado [] = undefined
grado [x] = 0
grado (x:xs) = 1 + (grado xs)

{- Ejercicio 3: evaluar :: Polinomio -> Float -> Float
que dados P ∈ R[X] y a ∈ R calcula P(a). -}

evaluar :: Polinomio -> Float -> Float
evaluar [p] _ = p
evaluar (p:ps) a = p*(a^n) + evaluar (limpiar ps) a
 where n = grado (p:ps)


{- Ejercicio 4: suma :: Polinomio -> Polinomio -> Polinomio.
Sugerencia: Tener en cuenta las funciones init y last del
Prelude, para hacer la recursión de atrás hacia adelante. -}

sumaSucia :: Polinomio -> Polinomio -> Polinomio
sumaSucia p [] = p
sumaSucia [] p = p
sumaSucia p q = (sumaSucia (init p) (init q)) ++ [(last p) + (last q)]

suma :: Polinomio -> Polinomio -> Polinomio
suma p q = limpiar (sumaSucia p q)


{- Ejercicio 5: productoPorEscalar :: Float -> Polinomio -> Polinomio
que calcula el producto de un escalar por un polinomio. -}

productoPorEscalar :: Float -> Polinomio -> Polinomio
productoPorEscalar n []     = []
productoPorEscalar n (p:ps) = (n*p) : (productoPorEscalar n ps)


{- Ejercicio 6: resta :: Polinomio -> Polinomio -> Polinomio,
que calcula la resta de polinomios. -}

resta :: Polinomio -> Polinomio -> Polinomio
resta p q = p `suma` ((-1) `productoPorEscalar` q)


{- Ejercicio 7: productoPorMonomio :: (Float, Int) -> Polinomio -> Polinomio
que calcula el producto de un monomio por un polinomio. -}

productoPorMonomio :: (Float, Int) -> Polinomio -> Polinomio
productoPorMonomio (a, 0) p = productoPorEscalar a p
productoPorMonomio (a, n) p = (productoPorMonomio (a, n-1) p) ++ [0]


{- Ejercicio 8: producto :: Polinomio -> Polinomio -> Polinomio
que calcula el producto de dos polinomios. -}

producto :: Polinomio -> Polinomio -> Polinomio
producto p [] = []
producto p (q:qs) = (productoPorMonomio (q, n) p) `suma` (producto p qs)
 where n = grado (q:qs)


{- Ejercicio 9: hacerPolinomio :: Monomio -> Polinomio
que “convierte” un monomio en un polinomio (considerando sus respectivas
codificaciones). -}

hacerPolinomio :: Monomio -> Polinomio
hacerPolinomio (0, _) = []
hacerPolinomio (a, 0) = [a]
hacerPolinomio (a, n) = hacerPolinomio (a, n-1) ++ [0]


{- Ejercicio 10: derivadaMonomio :: Monomio -> Monomio
que deriva un monomio. -}

derivadaMonomio :: Monomio -> Monomio
derivadaMonomio (a, n) = (a*nf, n-1)
 where nf = fromIntegral n



{- Ejercicio 11: derivada :: Polinomio -> Polinomio -}

derivada :: Polinomio -> Polinomio
derivada []     = []
derivada (p:ps) = hacerPolinomio (derivadaMonomio (p, n)) `suma` (derivada ps)
 where n = grado (p:ps)


{- Ejercicio 12: derivadaNEsima :: Polinomio -> Int -> Polinomio
que dados P ∈ R[X] y n ∈ N calcula la derivada n-ésima de P. -}

derivadaNEsima :: Polinomio -> Int -> Polinomio
derivadaNEsima [] _ = []
derivadaNEsima p 1  = derivada p
derivadaNEsima p n  = derivadaNEsima (derivada p) (n-1)


{- Ejercicio 13: primerCociente :: Polinomio -> Polinomio -> Monomio
que calcula el primer monomio del cociente de la división de dos polinomios. -}

primerCociente :: Polinomio -> Polinomio -> Monomio
primerCociente (p:ps) (q:qs) = (p/q, gp-gq)
 where gp = grado (p:ps)
       gq = grado (q:qs)


{- Ejercicio 14: primerResto :: Polinomio -> Polinomio -> Polinomio
que calcula la resta entre el dividendo y el producto del divisor por el
primer cociente. -}

primerResto :: Polinomio -> Polinomio -> Polinomio
primerResto p q = p `resta` (productoPorMonomio c q)
 where c = primerCociente p q


{- Ejercicio 15: division :: Polinomio -> Polinomio -> (Polinomio, Polinomio)
que dados polinomios P(X) y Q(X) calcule el cociente C(X) y el resto R(X) de 
la división (se debe
verificar P(X) = C(X)Q(X) + R(X) y deg R(X) < deg Q(X) o R(X) = 0). -}

division :: Polinomio -> Polinomio -> (Polinomio, Polinomio)
division p q 
 | (grado r) >= (grado q) = division r q 

 where primerDivision = (c, r)
       segundaDivision = (r, q)
       c = hacerPolinomio (primerCociente p q)
       r = primerResto p q


---------------(Funciones propias)---------------

potencia :: Polinomio -> Int -> Polinomio
potencia p 1 = p
potencia p n = p `producto` (potencia p (n-1))