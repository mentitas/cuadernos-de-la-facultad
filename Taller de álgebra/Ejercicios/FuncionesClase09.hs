module FuncionesClase09
where
     {- | Documentación

    Las funciones que hay en éste módulo son:

     - mcd
     - emcd

     -}


     indice9 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


     {- |Documentación 

     - Nombre:
     - Declaración: 
     - Descripción:
     - Parámetros: 
     - Retorna: 
     -}
     mcd :: Int -> Int -> Int
     mcd a 0 = a
     mcd a b = mcd b r
      where r = a `mod` b

     emcd :: Int -> Int -> (Int, Int, Int)
     emcd a 0 = (a, 1, 0)
     emcd a b = (f, s, t)
       where (f, s', t') = emcd b (a`mod`b)
             s = t'
             t = s' - t' * (a`div`b)
