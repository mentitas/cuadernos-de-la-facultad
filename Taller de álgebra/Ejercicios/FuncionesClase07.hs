module FuncionesClase07
where
     {- | Documentación

    Las funciones que hay en éste módulo son:

     - pertenece 
     - agregar
     - incluido
     - iguales
     - union
     - interseccion
     - diferencia
     - quitar
     - diferenciasimetrica
     - subpertenece
     - subagregar
     - subunion
     - agregaratodos
     - partes

     -}


     indice7 x = x
    -- esta función no hace nada, pero su doc devuelve el índice


     {- |Documentación 

     - Nombre:
     - Declaración: 
     - Descripción:
     - Parámetros: 
     - Retorna: 
     -}

     type Set a = [a]

     pertenece :: Int -> Set Int -> Bool
     pertenece n a | a == [] = False
                   | head a == n = True
                   | otherwise = pertenece n (tail a)

     agregar :: Int -> Set Int -> [Int]
     agregar n c | pertenece n c == True = c
                 | otherwise = (n:c)

     
     incluido :: Set Int -> Set Int -> Bool
     incluido [] c = True
     incluido (x:xs) c = pertenece x c && incluido xs c


     iguales :: Set Int -> Set Int -> Bool
     iguales c1 c2 = incluido c1 c2 && incluido c2 c1

     union :: Set Int -> Set Int -> Set Int
     union []     c = c
     union (x:xs) c = union xs (agregar x c)

     interseccion :: Set Int -> Set Int -> Set Int
     interseccion [] _ = []
     interseccion (x:xs) c
       | pertenece x c = x : (interseccion xs c)
       | otherwise     = interseccion xs c


     diferencia :: Set Int -> Set Int -> Set Int
     diferencia [] _ = []
     diferencia (x:xs) c 
      | pertenece x c = diferencia xs c
      | otherwise     = x : (diferencia xs c)

     quitar :: Int -> Set Int -> Set Int
     quitar n [] = []
     quitar n (x:xs) 
      | n == x = quitar n xs
      | otherwise = x : (quitar n xs)

     diferenciasimetrica :: Set Int -> Set Int -> Set Int
     diferenciasimetrica c1 c2 = diferencia (union c1 c2) (interseccion c1 c2)

     subpertenece :: Set Int -> Set (Set Int) -> Bool
     subpertenece n [] = False
     subpertenece n (x:xs)
      | iguales n x = True
      | otherwise   = subpertenece n xs

     subagregar :: Set Int -> Set (Set Int) -> Set (Set Int)
     subagregar c [] = [c]
     subagregar c cs 
      | subpertenece c cs = cs
      | otherwise         = c : cs 

     subunion :: Set (Set Int) -> Set (Set Int) -> Set (Set Int)
     subunion []     c = c
     subunion (x:xs) c = subunion xs (subagregar x c)

     agregaratodos :: Int -> Set (Set Int) -> Set (Set Int)
     agregaratodos n [] = [[]]
     agregaratodos n (cs:css) = (agregar n cs) : (agregaratodos n css)

     partes :: Set Int -> Set (Set Int)
     partes [] = [[]]
     partes (c:cs) = partescs `subunion` agregaratodos c partescs 
      where partescs = partes cs

