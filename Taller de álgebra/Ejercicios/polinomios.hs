import FuncionesClase07


--polinomio 3x^2 + 5x - 2 = [3, 4, -2]

type Polinomio = [Float]


darVuelta :: Polinomio -> Polinomio
darVuelta (x:xs) = agregarAlFinal x (darVuelta xs)

agregarAlFinal :: Float -> Polinomio -> Polinomio
agregarAlFinal a [] = [a]
agregarAlFinal a (x:xs) = x : (agregarAlFinal a xs)

grado :: Polinomio -> Int
grado [] = -1
grado p  = (length p) - 1

alargarPol_Adelante :: Int -> Polinomio -> Polinomio
alargarPol_Adelante n x = (formarListaConCeros m) `concatenar` x
 where m = n - (length x)

alargarPol_AlFinal :: Int -> Polinomio -> Polinomio
alargarPol_AlFinal n x = x `concatenar` (formarListaConCeros m)
 where m = n - (length x)

formarListaConCeros :: Int -> Polinomio
formarListaConCeros 0 = []
formarListaConCeros n = 0 : (formarListaConCeros (n-1))

concatenar :: Polinomio -> Polinomio -> Polinomio
concatenar [] bs = bs
concatenar (a:as) bs = a : (concatenar as bs)

--funciona si los polinomios son del mismo grado
sumaPol' :: Polinomio -> Polinomio -> Polinomio
sumaPol' [] [] = []
sumaPol' (a:as) (b:bs) = (a + b) : (sumaPol' as bs)

sumaPol :: Polinomio -> Polinomio -> Polinomio
sumaPol [] b = b
sumaPol a [] = a
sumaPol a b 
 | (grado a == grado b) = sumaPol' a b
 | (grado a >  grado b) = sumaPol' a b_alargado
 | (grado b >  grado a) = sumaPol' b a_alargado
 where a_alargado = alargarPol_Adelante (grado b) a
       b_alargado = alargarPol_Adelante (grado a) b

producto :: Float -> Polinomio -> Polinomio
producto n [] = []
producto n (x:xs) = (n*x) : producto n xs

restaPol :: Polinomio -> Polinomio -> Polinomio
restaPol a b = sumaPol a b_negativo
 where b_negativo = (-1) `producto` b

cuadradoBinomio :: Polinomio -> Polinomio
cuadradoBinomio [a, b] = [a**2, 2*a*b, b**2]
cuadradoBinomio x = undefined

cuboBinomio :: Polinomio -> Polinomio
cuboBinomio [a,b] = [a^3, 3*(a^2)*b, 3*a*(b^2), b^3]


productoMon_Pol :: Polinomio -> Polinomio -> Polinomio
productoMon_Pol m p
 | grado m == 0 = (head m) `producto` p
 | otherwise    = alargarPol_AlFinal grado_mp ((head m) `producto` p)
  where grado_mp = (grado m) + (grado p) + 1
 --ms=[] porque (m:ms) es un monomio


productoPol_Pol :: Polinomio -> Polinomio -> Polinomio
productoPol_Pol [] x = []
productoPol_Pol (p:ps) x = (monomioP `productoMon_Pol` x) `sumaPol` (productoPol_Pol ps x)
 where monomioP = alargarPol_AlFinal (length (p:ps)) [p]

{-
alargarPol' :: Int -> Polinomio -> Polinomio
alargarPol' 0 x = x
alargarPol' n x = 0 : (alargarPol' (n-1) x)

alargarPol :: Int -> Polinomio -> Polinomio
alargarPol n x = alargarPol' (n - length x) x

alargarPol_AlFinal' :: Int -> Polinomio -> Polinomio
alargarPol_AlFinal' 0 x = x
alargarPol_AlFinal' n x = agregarAlFinal 0 (alargarPol' (n-1) x)

alargarPol_AlFinal :: Int -> Polinomio -> Polinomio
alargarPol_AlFinal n x = alargarPol_AlFinal' (n - length x) x
-}